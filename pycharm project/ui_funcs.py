from xlrd import xlsx

__author__ = 'ali'

import xlrd
from numpy import *
import scipy
import csv

# function [inputData, errMsg] = loadData(filepath, handleMsg, dataType)
# global gHandles
# errMsg = gHandles.param.functNoError;
# try
#     fileNameElem = numel(filepath);
#     fileNameStop = max(regexp(filepath,'[.]'));
#     fileExt      = filepath(fileNameStop+1:fileNameElem);
#     switch fileExt
#         case 'xls'
#             [inputData, ~, ~] = xlsread(filepath);
#         case 'csv'
#             inputData = csvread(filepath);
#         otherwise
#             throw(MException('General:IOError',' file path is invalid or cannot be recognised.'));
#     end
# catch err
#     errMsg   = err.message;
#     inputData= -1;
#     if nargin == 3 % Show Error Dialog only if requested
#         if handleMsg
#             if strcmp(dataType,gHandles.param.dataTypeInput)
#                 resetPaths();
#             elseif strcmp(dataType,gHandles.param.dataTypeTraj)
#                 resetTrajPaths();
#             end
#             errordlg(['Error Occurred: ' dataType errMsg],'Load I/O Error');
#             uiwait(gcf); % Force to wait for confirmation
#         end
#     end
# end


def myxlsread(filepath):
    try:
        xlsfile=xlrd.open_workbook(filepath)
        return array(xlsfile.sheet_by_index(0)._cell_values)
    except Exception:
        return array([])

def mycsvread(filepath):
    try:
        with open(filepath,'rb') as csvfile:
            reader = csv.reader(csvfile)
            ret=[]
            for row in reader:
                row_data=[];
                for d in row:
                    row_data.append(float(d))
                ret.append(row_data)
            return array(ret,dtype='float64')
    except Exception:
        return array([])

def load_data(filepath,handleMsg,dataType):
    filext = str(filepath).split('.')[1];
    if(filext=='xls'):
        return myxlsread(filepath)
    elif(filext=='csv'):
        return mycsvread(filepath)
    else:
        pass #todo: error

#
# function [uMat, cholMat] = matrixFactorisation(mat)
# % Sparse matrix Operations: http://www.mathworks.com.au/help/techdoc/math/f6-8856.html
# % Postive Definite Matrix Test: https://ece.uwaterloo.ca/~ece204/howtos/positive.html
# % Lower-Upper Decomposition
# global gHandles
# try
#     [lMat,uMat,pMat] = lu(mat);
#     % If CholMod is selected, take lower decomposition matrix for cholesky factorisation
#     if     gHandles.param.cholMod == 1 % Lower decomposition matrix
#         mat = lMat * lMat';
#     elseif gHandles.param.cholMod == 2 % Permutation matrix
#         mat = pMat * pMat';
#     end
# catch err % Error in LU Factorisation
#     uMat = NaN;
# end
# % Cholesky Factorization
# try
#     cholMat = chol(mat);
# catch err % Fails Test for Positive Definiteness
#     cholMat = NaN;
# end

def matrixfactorisation(mat,cholMod):
    try:
        (pMat,lMat,uMat)=scipy.linalg.lu(mat)
        if(cholMod==1):
            mat= lMat*lMat
        else:
            mat = pMat*pMat
    except:
        uMat=nan
    try:
        cholMat = scipy.linalg.cholesky(mat)
    except:
        cholMat = nan
    return uMat,cholMat

def cleanupMatrix(mat):
    if isinstance(mat,array):
        mat[isnan(mat)]=0
        mat[isinf(mat)]=0
    return mat
