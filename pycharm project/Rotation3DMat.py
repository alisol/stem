from __future__ import division
from smop.core import *
from math import *
from numpy import full,nan,finfo
from numpy.linalg import norm

__author__ = 'ali'
def Rotation3DMat(r=None,axis=None,*args,**kwargs):
    nargout = kwargs["nargout"] if kwargs else None
    varargin = cellarray(args)
    nargin = 2-[r,axis].count(None)+len(args)

    if nargin == 1:
        raise Exception(char('Must have 2 input variables'))
    l=norm(axis)
    if (l < finfo(float).eps):
        raise Exception(char('Axis direction must be non-zero vector'))
    axis=axis / l
    u=axis[1]
    v=axis[2]
    w=axis[3]
    u2=u ** 2
    v2=v ** 2
    w2=w ** 2
    c=cos(r)
    s=sin(r)
    R=full([3,3],nan)
    R[1,1]=u2 + (v2 + w2) * c
    R[1,2]=u * v * (1 - c) - w * s
    R[1,3]=u * w * (1 - c) + v * s
    R[2,1]=u * v * (1 - c) + w * s
    R[2,2]=v2 + (u2 + w2) * c
    R[2,3]=v * w * (1 - c) - u * s
    R[3,1]=u * w * (1 - c) - v * s
    R[3,2]=v * w * (1 - c) + u * s
    R[3,3]=w2 + (u2 + v2) * c
    return R
