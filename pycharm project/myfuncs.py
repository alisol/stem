__author__ = 'ali'

from smop.core import char,strcmp
def isstr(a):
    return isinstance(a,str) or isinstance(a,char)

def lower(a):
    if isinstance(a,char):
        a.tostring().lower()
    a.lower()

def strncmp(a,b,n):
    return strcmp(a[:n],b[:n])

def rem(a,m):
    return a % m
