import sys
from PyQt4 import QtGui, QtCore
from Biometric import Ui_Dialog


class StartQT4(QtGui.QMainWindow):
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        # here we connect signals with our slots
        QtCore.QObject.connect(self.ui.inputOpen, QtCore.SIGNAL("clicked()"), self.inputOpen)
        QtCore.QObject.connect(self.ui.pathopen, QtCore.SIGNAL("clicked()"), self.pathopen)
        QtCore.QObject.connect(self.ui.reset, QtCore.SIGNAL("clicked()"), self.reset)
        QtCore.QObject.connect(self.ui.run, QtCore.SIGNAL("clicked()"), self.run)
        QtCore.QObject.connect(self.ui.stop, QtCore.SIGNAL("clicked()"), self.stop)
        QtCore.QObject.connect(self.ui.clear, QtCore.SIGNAL("clicked()"), self.clear)

    def inputOpen(self):
        dlg = QtGui.QFileDialog()
        dlg.setViewMode(QtGui.QFileDialog.Detail)
        dlg.setNameFilters([self.tr('Excel Files (*.xls)'), self.tr('CSV Files (*.csv)')])
        dlg.setDefaultSuffix('.xls')
        dlg.setFileMode(QtGui.QFileDialog.ExistingFiles)
        hjk = dlg.exec_()

    def pathopen(self):
        dlg = QtGui.QFileDialog()
        dlg.setViewMode(QtGui.QFileDialog.Detail)
        dlg.setNameFilters([self.tr('Excel Files (*.xls)'), self.tr('CSV Files (*.csv)')])
        dlg.setDefaultSuffix('.xls')
        dlg.setFileMode(QtGui.QFileDialog.ExistingFiles)
        hjk = dlg.exec_()

    def reset(self):
        dlg = QtGui.QFileDialog()
        dlg.setViewMode(QtGui.QFileDialog.Detail)
        dlg.setNameFilters([self.tr('Excel Files (*.xls)'), self.tr('CSV Files (*.csv)')])
        dlg.setDefaultSuffix('.xls')
        dlg.setFileMode(QtGui.QFileDialog.ExistingFiles)
        hjk = dlg.exec_()

    def run(self):
        dlg = QtGui.QFileDialog()
        dlg.setViewMode(QtGui.QFileDialog.Detail)
        dlg.setNameFilters([self.tr('Excel Files (*.xls)'), self.tr('CSV Files (*.csv)')])
        dlg.setDefaultSuffix('.xls')
        dlg.setFileMode(QtGui.QFileDialog.ExistingFiles)
        hjk = dlg.exec_()

    def stop(self):
        dlg = QtGui.QFileDialog()
        dlg.setViewMode(QtGui.QFileDialog.Detail)
        dlg.setNameFilters([self.tr('Excel Files (*.xls)'), self.tr('CSV Files (*.csv)')])
        dlg.setDefaultSuffix('.xls')
        dlg.setFileMode(QtGui.QFileDialog.ExistingFiles)
        hjk = dlg.exec_()

    def clear(self):
        dlg = QtGui.QFileDialog()
        dlg.setViewMode(QtGui.QFileDialog.Detail)
        dlg.setNameFilters([self.tr('Excel Files (*.xls)'), self.tr('CSV Files (*.csv)')])
        dlg.setDefaultSuffix('.xls')
        dlg.setFileMode(QtGui.QFileDialog.ExistingFiles)
        hjk = dlg.exec_()


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    myapp = StartQT4()
    myapp.show()
    sys.exit(app.exec_())

