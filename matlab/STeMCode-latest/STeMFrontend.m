%%% Biomimetic Algorithm Simulation System (BASS) %%%

% Note: Excel File must be Excel 2000/2003 (*.xls) version or
%       CSV Comma Delimited Text File only (*.csv).
%       Must be in array type (n:3 dimensions)
%       4+ columns will cause errors and/or undesired results
%       Ensure the Frontend is executed in the same directory the STeM
%       code is located (i.e. "%USERPROFILE%\Documents\MATLAB\STeM\")
%       for data import and export functions to run correctly.
%       View System Default Values in STeMFrontend_OpeningFcn
%       To immediately halt operations: Press Ctrl+C in Command Window
%
% Memory Issues: Running large datasets will result in Out of Memory errors
%       Tips to resolve memory constraints (Pack command is used in command window):
%       http://www.mathworks.com.au/support/tech-notes/1100/1107.html
%       http://www.mathworks.com.au/help/techdoc/matlab_prog/brh72ex-49.html
%       http://stackoverflow.com/questions/2496594/memory-not-freed-in-matlab
%       http://undocumentedmatlab.com/blog/matlab-java-memory-leaks-performance/
%       The recommended way to reconsolidate lost memory is to save all settings
%       and shutdown MATLAB, and restart again.

function varargout = STeMFrontend(varargin)
% STEMFRONTEND M-file for STeMFrontend.fig
% STEMFRONTEND, by itself, creates a new STEMFRONTEND or raises the existing
% singleton*.
% 
% H = STEMFRONTEND returns the handle to a new STEMFRONTEND or the handle to
% the existing singleton*.
% 
% STEMFRONTEND('CALLBACK',hObject,eventData,handles,...) calls the local
% function named CALLBACK in STEMFRONTEND.M with the given input arguments.
%     
% STEMFRONTEND('Property','Value',...) creates a new STEMFRONTEND or raises the
% existing singleton*.  Starting from the left, property value pairs are
% applied to the GUI before STeMFrontend_OpeningFcn gets called.  An
% unrecognized property name or invalid value makes property application
% stop.  All inputs are passed to STeMFrontend_OpeningFcn via varargin.
%     
% *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
% instance to run (singleton)".
% See also: GUIDE, GUIDATA, GUIHANDLES
% Edit the above text to modify the response to help STeMFrontend
% Last Modified by GUIDE v2.5 21-Apr-2012 18:30:00
% Begin initialization code - DO NOT EDIT

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @STeMFrontend_OpeningFcn, ...
                   'gui_OutputFcn',  @STeMFrontend_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

%%% FIGURE CALLBACK FUNCTIONS %%%
% --- Executes just before STeMFrontend is made visible.
function STeMFrontend_OpeningFcn(hObject, ~, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to STeMFrontend (see VARARGIN)

handles.output = hObject;              % Choose default command line output for STeMFrontend
guidata(hObject, handles);             % Update handles structure
% uiwait(handles.figure_STeMFrontend); % UIWAIT makes STeMFrontend wait for user response (see UIRESUME)

global gHandles
baseFilePath = GetFilePath();
path(path, baseFilePath);
gHandles.handles            = handles;
gHandles.param.maxPercent   = 100;
gHandles.param.dataTypeInput= 'Input Data';
gHandles.param.dataTypeTraj = 'Trajectory';
gHandles.param.dataExpSingle= 'Singular';
gHandles.param.dataExpDiff  = 'Difference';
gHandles.param.dataTypesStat= {'Hessian', 'Determinant', 'Determinant (Inverse)', 'Largest Eigenvalues'};
gHandles.param.dataTypes    = {'HV1', 'HV2', 'HV3', 'HV4', 'HVAdd', 'All'};
gHandles.param.dataTypesLeg = {'HV1', 'HV1-Inv', 'HV2', 'HV2-Inv', 'HV3', 'HV3-Inv', 'HV4', 'HV4-Inv', 'HVAdd', 'HVAdd-Inv'};
gHandles.param.functUnavail = 'N/A';
gHandles.param.functNoError = 'NoError';
gHandles.param.dateFmt      = 'dd-mm-yy, HH.MM.SS.FFF';
gHandles.param.figNames     = {'Source';'PCA';'Sparsity';'Determinant';'Plot';'Decomposition';'Evaluation'};
gHandles.fig.fig1           = figure('Name','Fig #1 Window (Auto-Close)','NumberTitle','off'); % Renderer: Painters, ZBuffer, OpenGL
gHandles.fig.fig2           = figure('Name','Fig #2 Window (Auto-Close)','NumberTitle','off');
gHandles.fig.fig3           = figure('Name','Fig #3 Window (Auto-Close)','NumberTitle','off');
gHandles.fig.fig4           = figure('Name','Fig #4 Window (Auto-Close)','NumberTitle','off');
gHandles.fig.fig5           = figure('Name','Fig #5 Window (Auto-Close)','NumberTitle','off');
gHandles.fig.fig6           = figure('Name','Fig #6 Window (Auto-Close)','NumberTitle','off');
gHandles.fig.fig1Close      = get(gHandles.fig.fig1,'CloseRequestFcn');
gHandles.fig.fig2Close      = get(gHandles.fig.fig2,'CloseRequestFcn');
gHandles.fig.fig3Close      = get(gHandles.fig.fig3,'CloseRequestFcn');
gHandles.fig.fig4Close      = get(gHandles.fig.fig4,'CloseRequestFcn');
gHandles.fig.fig5Close      = get(gHandles.fig.fig5,'CloseRequestFcn');
gHandles.fig.fig6Close      = get(gHandles.fig.fig6,'CloseRequestFcn');
set(gHandles.fig.fig1,'CloseRequestFcn','');
set(gHandles.fig.fig2,'CloseRequestFcn','');
set(gHandles.fig.fig3,'CloseRequestFcn','');
set(gHandles.fig.fig4,'CloseRequestFcn','');
set(gHandles.fig.fig5,'CloseRequestFcn','');
set(gHandles.fig.fig6,'CloseRequestFcn','');
outputDataStem(['Run Time: ' datestr(now, gHandles.param.dateFmt)]);
PSO();         % Configure Function Handles for PSO Algorithm
clearOutput(); % Clear Display of Visual Charting Elements
resetParams(); % Default Settings for Simulator

% --- Outputs from this function are returned to the command line.
function varargout = STeMFrontend_OutputFcn(~, ~, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes when user attempts to close figure_STeMFrontend.
function figure_STeMFrontend_CloseRequestFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to figure_STeMFrontend (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: delete(hObject) closes the figure
global gHandles
try
    set(gHandles.fig.fig1,'CloseRequestFcn',gHandles.fig.fig1Close);
    set(gHandles.fig.fig2,'CloseRequestFcn',gHandles.fig.fig2Close);
    set(gHandles.fig.fig3,'CloseRequestFcn',gHandles.fig.fig3Close);
    set(gHandles.fig.fig4,'CloseRequestFcn',gHandles.fig.fig4Close);
    set(gHandles.fig.fig5,'CloseRequestFcn',gHandles.fig.fig5Close);
    set(gHandles.fig.fig6,'CloseRequestFcn',gHandles.fig.fig6Close);
    close(gHandles.fig.fig1);
    close(gHandles.fig.fig2);
    close(gHandles.fig.fig3);
    close(gHandles.fig.fig4);
    close(gHandles.fig.fig5);
    close(gHandles.fig.fig6);
    set(gHandles.fig.fig1_1,'CloseRequestFcn',gHandles.fig.fig1_1Close);
    close(gHandles.fig.fig1_1);
catch err
end
delete(hObject);
clear all;
close all force;

%%% FIGURE CALLBACK FUNCTIONS: Control Button Panel %%%
% --- Executes on button press in pb_RunAlgorithm.
function pb_RunAlgorithm_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to pb_RunAlgorithm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
preInitUIBeforeRun();
preRunStemFunction();
postInitUIAfterRun();

% --- Executes on button press in pb_StopAlgorithm.
function pb_StopAlgorithm_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to pb_StopAlgorithm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.proc.stopProcess = 1;

% --- Executes on button press in pb_ClearOutput.
function pb_ClearOutput_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to pb_ClearOutput (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clearOutput();
clearTextField();

% --- Executes on button press in pb_ResetParams.
function pb_ResetParams_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to pb_ResetParams (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
resetParams();

%%% FIGURE CALLBACK FUNCTIONS: 1. Input Data Process %%%
% --- Executes during object creation, after setting all properties.
function input_FilePath_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_FilePath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function input_FilePath_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to input_FilePath (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_FilePath as text
%        str2double(get(hObject,'String')) returns contents of input_FilePath as a double
verifyFileEntered();

% --- Executes on button press in pb_OpenFile.
function pb_OpenFile_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to pb_OpenFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
verifyFileSelected();

function input_Iterations_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Iterations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_Iterations as text
%        str2double(get(hObject,'String')) returns contents of input_Iterations as a double
global gHandles
verifyParamsAbsRound(hObject);
gHandles.param.buffIteration = str2double(get(hObject,'String'));
verifyDataMapMode();

% --- Executes on key press with focus on input_Iterations and none of its controls.
function input_Iterations_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Iterations (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

% --- Executes during object creation, after setting all properties.
function input_Iterations_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Iterations (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function input_WaitTime_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_WaitTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_WaitTime as text
%        str2double(get(hObject,'String')) returns contents of input_WaitTime as a double
global gHandles
verifyParamsAbsAndZero(hObject);
gHandles.param.buffWaitTime = str2double(get(hObject,'String'));

% --- Executes during object creation, after setting all properties.
function input_WaitTime_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_WaitTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on key press with focus on input_WaitTime and none of its controls.
function input_WaitTime_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_WaitTime (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

% --- Executes on selection change in popup_DataMode.
function popup_DataMode_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_DataMode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns popup_DataMode contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_DataMode
dataProcessMode();

% --- Executes during object creation, after setting all properties.
function popup_DataMode_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_DataMode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popup_DataMap.
function popup_DataMap_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_DataMap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns popup_DataMap contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_DataMap
verifyDataMapMode();

% --- Executes during object creation, after setting all properties.
function popup_DataMap_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_DataMap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popup_ExportData.
function popup_ExportData_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_ExportData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns popup_ExportData contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_ExportData

% --- Executes during object creation, after setting all properties.
function popup_ExportData_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_ExportData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popup_ExportImage.
function popup_ExportImage_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_ExportImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns popup_ExportImage contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_ExportImage
noExport = 1;
if get(hObject,'Value') ~= noExport
    figureCaptureWarning('Image');
end

% --- Executes during object creation, after setting all properties.
function popup_ExportImage_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_ExportImage (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popup_ExportVideo.
function popup_ExportVideo_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_ExportVideo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns popup_ExportVideo contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_ExportVideo
noRecording = 1;
if get(hObject,'Value') ~= noRecording
    figureCaptureWarning('Video');
    dataFrameInput();
end

% --- Executes during object creation, after setting all properties.
function popup_ExportVideo_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_ExportVideo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%% FIGURE CALLBACK FUNCTIONS: 2. Custom Data Manipulation %%%
% --- Executes on button press in cb_EnableCustomData.
function cb_EnableCustomData_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to cb_EnableCustomData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of cb_EnableCustomData
enableManipulationMode();

% --- Executes on selection change in popup_ManipulationMode.
function popup_ManipulationMode_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_ManipulationMode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns popup_ManipulationMode contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_ManipulationMode
enableManipulationMode();

% --- Executes during object creation, after setting all properties.
function popup_ManipulationMode_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_ManipulationMode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popup_IncreaseMode.
function popup_IncreaseMode_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_IncreaseMode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns popup_IncreaseMode contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_IncreaseMode

% --- Executes during object creation, after setting all properties.
function popup_IncreaseMode_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_IncreaseMode (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function input_Interval_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Interval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_Interval as text
%        str2double(get(hObject,'String')) returns contents of input_Interval as a double
verifyParamsAbsRound(hObject);
verifyDataSize();

% --- Executes during object creation, after setting all properties.
function input_Interval_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Interval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on key press with focus on input_Interval and none of its controls.
function input_Interval_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Interval (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

function input_SplitInterval_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_SplitInterval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_SplitInterval as text
%        str2double(get(hObject,'String')) returns contents of input_SplitInterval as a double
verifyParamsAbsRound(hObject);
verifyDataSize();

% --- Executes on key press with focus on input_SplitInterval and none of its controls.
function input_SplitInterval_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_SplitInterval (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

% --- Executes during object creation, after setting all properties.
function input_SplitInterval_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_SplitInterval (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function input_RandVar_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_RandVar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_RandVar as text
%        str2double(get(hObject,'String')) returns contents of input_RandVar as a double
global gHandles
verifyParamsAbs(hObject);
if (str2double(get(hObject,'String')) > gHandles.param.maxPercent)
    set(hObject,'String',gHandles.param.maxPercent);
end

% --- Executes during object creation, after setting all properties.
function input_RandVar_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_RandVar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on key press with focus on input_RandVar and none of its controls.
function input_RandVar_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_RandVar (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

function input_RandPop_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_RandPop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_RandPop as text
%        str2double(get(hObject,'String')) returns contents of input_RandPop as a double
global gHandles
verifyParamsAbs(hObject);
if (str2double(get(hObject,'String')) > gHandles.param.maxPercent)
    set(hObject,'String',gHandles.param.maxPercent);
end

% --- Executes during object creation, after setting all properties.
function input_RandPop_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_RandPop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on key press with focus on input_RandPop and none of its controls.
function input_RandPop_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_RandPop (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

function input_Xshift_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Xshift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_Xshift as text
%        str2double(get(hObject,'String')) returns contents of input_Xshift as a double
verifyParams(hObject);

% --- Executes on key press with focus on input_Xshift and none of its controls.
function input_Xshift_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Xshift (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

% --- Executes during object creation, after setting all properties.
function input_Xshift_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Xshift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function input_Yshift_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Yshift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_Yshift as text
%        str2double(get(hObject,'String')) returns contents of input_Yshift as a double
verifyParams(hObject);

% --- Executes on key press with focus on input_Yshift and none of its controls.
function input_Yshift_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Yshift (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

% --- Executes during object creation, after setting all properties.
function input_Yshift_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Yshift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function input_Zshift_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Zshift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_Zshift as text
%        str2double(get(hObject,'String')) returns contents of input_Zshift as a double
verifyParams(hObject);

% --- Executes on key press with focus on input_Zshift and none of its controls.
function input_Zshift_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Zshift (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

% --- Executes during object creation, after setting all properties.
function input_Zshift_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Zshift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function input_Xrot_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Xrot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_Xrot as text
%        str2double(get(hObject,'String')) returns contents of input_Xrot as a double
verifyParams(hObject);

% --- Executes on key press with focus on input_Xrot and none of its controls.
function input_Xrot_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Xrot (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

% --- Executes during object creation, after setting all properties.
function input_Xrot_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Xrot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function input_Yrot_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Yrot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_Yrot as text
%        str2double(get(hObject,'String')) returns contents of input_Yrot as a double
verifyParams(hObject);

% --- Executes on key press with focus on input_Yrot and none of its controls.
function input_Yrot_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Yrot (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

% --- Executes during object creation, after setting all properties.
function input_Yrot_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Yrot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function input_Zrot_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Zrot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_Zrot as text
%        str2double(get(hObject,'String')) returns contents of input_Zrot as a double
verifyParams(hObject);

% --- Executes on key press with focus on input_Zrot and none of its controls.
function input_Zrot_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Zrot (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

% --- Executes during object creation, after setting all properties.
function input_Zrot_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Zrot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%% FIGURE CALLBACK FUNCTIONS: 3. Path Trajectory %%%
% --- Executes on button press in cb_EnablePathTraj.
function cb_EnablePathTraj_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to cb_EnablePathTraj (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of cb_EnablePathTraj
enableTrajectoryMode();

% --- Executes on button press in cb_ShiftRadialLength.
function cb_ShiftRadialLength_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to cb_ShiftRadialLength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of cb_ShiftRadialLength

% --- Executes on button press in pb_TrajOpenFile.
function pb_TrajOpenFile_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to pb_TrajOpenFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.buffPathWarn = 0;
verifyTrajFileSelected();

% --- Executes on selection change in popup_TrajManipulationType.
function popup_TrajManipulationType_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_TrajManipulationType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns popup_TrajManipulationType contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_TrajManipulationType
enableTrajectoryMode();

% --- Executes during object creation, after setting all properties.
function popup_TrajManipulationType_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_TrajManipulationType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function input_TrajFile_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_TrajFile as text
%        str2double(get(hObject,'String')) returns contents of input_TrajFile as a double
global gHandles
gHandles.param.buffPathWarn = 0;
verifyTrajFileEntered();

% --- Executes during object creation, after setting all properties.
function input_TrajFile_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function input_TrajMoveRad_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajMoveRad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_TrajMoveRad as text
%        str2double(get(hObject,'String')) returns contents of input_TrajMoveRad as a double
verifyParams(hObject);

% --- Executes during object creation, after setting all properties.
function input_TrajMoveRad_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajMoveRad (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on key press with focus on input_TrajMoveRad and none of its controls.
function input_TrajMoveRad_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajMoveRad (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

function input_TrajRandVar_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajRandVar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_TrajRandVar as text
%        str2double(get(hObject,'String')) returns contents of input_TrajRandVar as a double
global gHandles
verifyParamsAbs(hObject);
if (str2double(get(hObject,'String')) > gHandles.param.maxPercent)
    set(hObject,'String',gHandles.param.maxPercent);
end

% --- Executes during object creation, after setting all properties.
function input_TrajRandVar_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajRandVar (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on key press with focus on input_TrajRandVar and none of its controls.
function input_TrajRandVar_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajRandVar (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

function input_TrajRandPop_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajRandPop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_TrajRandPop as text
%        str2double(get(hObject,'String')) returns contents of input_TrajRandPop as a double
global gHandles
verifyParamsAbs(hObject);
if (str2double(get(hObject,'String')) > gHandles.param.maxPercent)
    set(hObject,'String',gHandles.param.maxPercent);
end

% --- Executes during object creation, after setting all properties.
function input_TrajRandPop_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajRandPop (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on key press with focus on input_TrajRandPop and none of its controls.
function input_TrajRandPop_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajRandPop (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

function input_TrajXShift_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajXShift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_TrajXShift as text
%        str2double(get(hObject,'String')) returns contents of input_TrajXShift as a double
verifyParams(hObject);

% --- Executes during object creation, after setting all properties.
function input_TrajXShift_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajXShift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on key press with focus on input_TrajXShift and none of its controls.
function input_TrajXShift_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajXShift (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

function input_TrajYShift_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajYShift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_TrajYShift as text
%        str2double(get(hObject,'String')) returns contents of input_TrajYShift as a double
verifyParams(hObject);

% --- Executes during object creation, after setting all properties.
function input_TrajYShift_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajYShift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on key press with focus on input_TrajYShift and none of its controls.
function input_TrajYShift_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajYShift (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

function input_TrajZShift_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajZShift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_TrajZShift as text
%        str2double(get(hObject,'String')) returns contents of input_TrajZShift as a double
verifyParams(hObject);

% --- Executes during object creation, after setting all properties.
function input_TrajZShift_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajZShift (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on key press with focus on input_TrajZShift and none of its controls.
function input_TrajZShift_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajZShift (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

function input_TrajXRot_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajXRot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_TrajXRot as text
%        str2double(get(hObject,'String')) returns contents of input_TrajXRot as a double
verifyParams(hObject);

% --- Executes during object creation, after setting all properties.
function input_TrajXRot_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajXRot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on key press with focus on input_TrajXRot and none of its controls.
function input_TrajXRot_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajXRot (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

function input_TrajYRot_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajYRot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_TrajYRot as text
%        str2double(get(hObject,'String')) returns contents of input_TrajYRot as a double
verifyParams(hObject);

% --- Executes during object creation, after setting all properties.
function input_TrajYRot_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajYRot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on key press with focus on input_TrajYRot and none of its controls.
function input_TrajYRot_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajYRot (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

function input_TrajZRot_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajZRot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_TrajZRot as text
%        str2double(get(hObject,'String')) returns contents of input_TrajZRot as a double
verifyParams(hObject);

% --- Executes during object creation, after setting all properties.
function input_TrajZRot_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajZRot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on key press with focus on input_TrajZRot and none of its controls.
function input_TrajZRot_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_TrajZRot (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

%%% FIGURE CALLBACK FUNCTIONS: 4. STEM Settings %%%
function input_Epsilon_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Epsilon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_Epsilon as text
%        str2double(get(hObject,'String')) returns contents of input_Epsilon as a double
verifyParams(hObject);
defaultStemParams();

% --- Executes on key press with focus on input_Epsilon and none of its controls.
function input_Epsilon_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Epsilon (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

% --- Executes during object creation, after setting all properties.
function input_Epsilon_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Epsilon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function input_Kr_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Kr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_Kr as text
%        str2double(get(hObject,'String')) returns contents of input_Kr as a double
verifyParams(hObject);

% --- Executes on key press with focus on input_Kr and none of its controls.
function input_Kr_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Kr (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

% --- Executes during object creation, after setting all properties.
function input_Kr_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Kr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function input_Ktheta_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Ktheta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_Ktheta as text
%        str2double(get(hObject,'String')) returns contents of input_Ktheta as a double
verifyParams(hObject);

% --- Executes on key press with focus on input_Ktheta and none of its controls.
function input_Ktheta_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Ktheta (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

% --- Executes during object creation, after setting all properties.
function input_Ktheta_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Ktheta (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function input_Kphi1_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Kphi1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_Kphi1 as text
%        str2double(get(hObject,'String')) returns contents of input_Kphi1 as a double
verifyParams(hObject);

% --- Executes on key press with focus on input_Kphi1 and none of its controls.
function input_Kphi1_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Kphi1 (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

% --- Executes during object creation, after setting all properties.
function input_Kphi1_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Kphi1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function input_Kphi3_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Kphi3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_Kphi3 as text
%        str2double(get(hObject,'String')) returns contents of input_Kphi3 as a double
verifyParams(hObject);

% --- Executes on key press with focus on input_Kphi3 and none of its controls.
function input_Kphi3_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Kphi3 (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

% --- Executes during object creation, after setting all properties.
function input_Kphi3_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_Kphi3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function input_LongTerm_Callback(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_LongTerm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of input_LongTerm as text
%        str2double(get(hObject,'String')) returns contents of input_LongTerm as a double
verifyParams(hObject);

% --- Executes during object creation, after setting all properties.
function input_LongTerm_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_LongTerm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on key press with focus on input_LongTerm and none of its controls.
function input_LongTerm_KeyPressFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to input_LongTerm (see GCBO)
% eventdata  structure with the following fields (see UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)
global gHandles
gHandles.param.origVal = get(hObject, 'String');

%%% FIGURE CALLBACK FUNCTIONS: 5. Algorithmic Output %%%
% --- Executes during object creation, after setting all properties.
function edit_OutputData_CreateFcn(~, ~, ~) %#ok<DEFNU>
% hObject    handle to edit_OutputData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

function edit_OutputData_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to edit_OutputData (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'String') returns contents of edit_OutputData as text
%        str2double(get(hObject,'String')) returns contents of edit_OutputData as a double

%%% FIGURE CALLBACK FUNCTIONS: 6. Show Figures %%%
% --- Executes on button press in cb_ShowAlgResults.
function cb_ShowAlgResults_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to cb_ShowAlgResults (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of cb_ShowAlgResults

% --- Executes on button press in cb_ShowFig1.
function cb_ShowFig1_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to cb_ShowFig1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of cb_ShowFig1

% --- Executes on button press in cb_ShowFig2.
function cb_ShowFig2_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to cb_ShowFig2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of cb_ShowFig2

% --- Executes on button press in cb_ShowFig3.
function cb_ShowFig3_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to cb_ShowFig3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of cb_ShowFig3

% --- Executes on button press in cb_ShowFig4.
function cb_ShowFig4_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to cb_ShowFig4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of cb_ShowFig4

% --- Executes on button press in cb_ShowFig5.
function cb_ShowFig5_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to cb_ShowFig5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of cb_ShowFig5

% --- Executes on button press in cb_ShowFig6.
function cb_ShowFig6_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to cb_ShowFig6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hint: get(hObject,'Value') returns toggle state of cb_ShowFig6

% --- Executes on selection change in popup_HessColour.
function popup_HessColour_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_HessColour (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns popup_HessColour contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_HessColour

% --- Executes during object creation, after setting all properties.
function popup_HessColour_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_HessColour (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popup_TrajColour.
function popup_TrajColour_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_TrajColour (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns popup_TrajColour contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_TrajColour

% --- Executes during object creation, after setting all properties.
function popup_TrajColour_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_TrajColour (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popup_PointColour.
function popup_PointColour_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_PointColour (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns popup_PointColour contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_PointColour

% --- Executes during object creation, after setting all properties.
function popup_PointColour_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_PointColour (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popup_ColourMap.
function popup_ColourMap_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_ColourMap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns popup_ColourMap contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_ColourMap

% --- Executes during object creation, after setting all properties.
function popup_ColourMap_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_ColourMap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in popup_MarkColour.
function popup_MarkColour_Callback(~, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_MarkColour (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: contents = cellstr(get(hObject,'String')) returns popup_MarkColour contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_MarkColour

% --- Executes during object creation, after setting all properties.
function popup_MarkColour_CreateFcn(hObject, ~, ~) %#ok<DEFNU>
% hObject    handle to popup_MarkColour (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%%% UI INTERFACE FUNCTIONS %%%
function updateWaitbar(value)
% Update Waitbar within axes (with text): http://www.mathworks.com/matlabcentral/newsreader/view_thread/148109
global gHandles
h = gHandles.handles.axes_ProgressBar;
strVal = [num2str(round(value*gHandles.param.maxPercent)) '% Done'];
if value>=0.5
    colorArray = [0.8 0.8 0.8];
else
    colorArray = [0.2 0.2 0.2];
end 
set(h,'xtick',[],'ytick',[]);
axes(h); %#ok<MAXES>
cla;
h = patch([0,value,value,0],[0,0,1,1],[0.3 0.3 0.8]);
axis([0,1,0,1]);
if value<0.1;      xPos = 0.37;
elseif value>=0.1; xPos = 0.33;
elseif value==1;   xPos = 0.3;
end
text(xPos,0.45,strVal,'color',colorArray);
axis off
drawnow;

function verifyDataSize(inputData, intervalOnly)
% Split Interval and Interval Management
global gHandles
handles = gHandles.handles;
fileOpen= 0;
if nargin == 0
    [inputData, fileOpen] = loadInitData();
    if ~fileOpen; return; end % Exit as file not validated
end
if nargin == 1 || fileOpen == 1
    % Test against Interval and Split Interval Data
    errMsg = '';
    inputArraySize = size(inputData,1);
    if str2double(get(handles.input_Interval,'String')) > inputArraySize
        set(handles.input_Interval,'String','1');
        errMsg = 'Input Warning: Interval size must be less than/equal to Input Array Size. ';
    end
    if str2double(get(handles.input_SplitInterval,'String')) > inputArraySize
        set(handles.input_SplitInterval,'String',num2str(inputArraySize));
        errMsg = [errMsg 'Input Warning: Split Interval must be less than/equal to Input Array Size.'];
    end
    if ~strcmp(errMsg,'')
        warndlg(errMsg,'Input Warning');
        uiwait(gcf); % Force to wait for confirmation
    end
elseif nargin == 2
    % Test against Interval Data Only
    inputArraySize = size(inputData,1);
    if (intervalOnly == 1) && (str2double(get(handles.input_Interval,'String')) > inputArraySize)
        set(handles.input_Interval,'String','1');
        warndlg('Input Warning: Interval size must be less than/equal to Input Array Size.','Input Warning');
        uiwait(gcf); % Force to wait for confirmation
    end
end

function isOk = verifyExpTrajSize(randSelect, manualSet, inputData, inputType)
global gHandles
handles = gHandles.handles;
isOk    = 1;
fileOpen= 0;
if nargin == 0
    % Verify All Settings Automatically
    [inputData, fileOpen] = loadInitData();
    if ~fileOpen; return; end % Exit as file not validated
    regionRandSel = gHandles.param.regionRandSel;
    regionManual  = gHandles.param.regionManual;
    radialRandSel = gHandles.param.radialRandSel;
    radialManual  = gHandles.param.radialManual;
    verifyExpTrajSize(regionRandSel, regionManual, inputData, gHandles.param.regionExpType); % Verify Region Exp. Settings
    verifyExpTrajSize(radialRandSel, radialManual, inputData, gHandles.param.radialExpType); % Verify Radial Exp. Settings
else
    % Load File Prior to Verify Array Size
    if nargin == 2
        [inputData, fileOpen] = loadInitData();
    end
    % Verify Array Size with Input Data
    if nargin == 4 || fileOpen == 1
        errMsg = '';
        inputArraySize = size(inputData,1);
        if randSelect > inputArraySize
            errMsg = 'Random Amount input must not be greater than input data array size. ';
        end
        for i=1:size(manualSet,2)
            if manualSet(i) > inputArraySize
                errMsg = [errMsg 'Manual Set Value input must not be greater than data array size. '];
                break
            end
        end
        % Show Message
        if ~strcmp(errMsg,'')
            isOk = 0;
            if nargin == 4
                warndlg([errMsg 'Please Note: Defaults used; Use expansion data dialog to modify settings.'],'Input Warning');
                uiwait(gcf); % Force to wait for confirmation
                % Set Default Settings
                switch inputType
                    case gHandles.param.regionExpType
                        gHandles.param.regionRandSel= 1;
                        gHandles.param.regionManual = 0;
                    case gHandles.param.radialExpType
                        gHandles.param.radialRandSel= 1;
                        gHandles.param.radialManual = 0;
                end
            end
        end
    end
end

function [expMode] = verifyExpansionMode(manEntrySize, expMode)
% Validity Check for Expansion Mode and Manual Entry
if manEntrySize > 1 && expMode ~= 1
    expMode = 1;
    warndlg('Notice: Setting Expansion Mode to Forward Only as manual entry of data manipulation is being used.', 'Notice', 'replace');
    uiwait(gcf); % Force to wait for confirmation
end

function verifyPathSize()
% Iteration Management
global gHandles
handles  = gHandles.handles;
filepath = gHandles.proc.trajPath;
[inputData, errMsg]= loadData(filepath);
if strcmp(errMsg, gHandles.param.functNoError)
    noWarning      = gHandles.param.buffPathWarn == 0;
    inputArraySize = size(inputData,1);
    iterationActive= get(handles.input_Iterations,'Value');
    arraySizeExceed= str2double(get(handles.input_Iterations,'String')) > inputArraySize;
    isTrajEnabled  = get(handles.cb_EnablePathTraj,'Value');
    % Verify path size for single file & random data (not multi-file as iteration is fixed)
    if noWarning && iterationActive && arraySizeExceed && isTrajEnabled
        choice = questdlg('Warning: The Number of Iterations exceeds the Input Path Trajectory Size. This query will reopen with a change in the path trajectory.', ...
                          'Path Trajectory Parameter Warning', ...
                          'Change Iterations to Path Size','Leave Iterations Currently Set','Change Iterations to Path Size');
        gHandles.param.buffPathWarn = 1; % Warning has been made, user has acknowledged
        % Handle response
        switch choice
            case 'Change Iterations to Path Size'
                set(handles.input_Iterations,'String',num2str(inputArraySize));
                saveIterWaitTimeLastState(); % Persist Last State of Iteration & Wait-time
                verifyDataMapMode();
            case 'Leave Iterations Currently Set'
                % No action
        end
    end
else
    warndlg('Notice: Input verification pending as alternate data source is used or file not specified.','System Notice');
    uiwait(gcf); % Force to wait for confirmation
end

function verifyDataMapMode()
global gHandles
handles     = gHandles.handles;
iteration   = str2double(get(handles.input_Iterations,'String'));
dataMapMode = get(handles.popup_DataMap,'Value');
singMode    = 1;
diffMode    = 2;
if iteration <= 1 && dataMapMode == diffMode
    set(handles.popup_DataMap,'Value',singMode);
    warndlg('Warning: Difference Data-Mode can only run with more than 1 iteration.','Data-Mode Warning');
end

function [isFunctionOk, evalFunction] = verifyEvalFunction(evalFunction, isFunctionOk)
% Evaluation Function Management
global gHandles
if nargin == 1
    % Evaluate Function
    try
        x = 1; xGoal = 2;
        y = 2; yGoal = 4;
        z = 3; zGoal = 6;
        val = eval(evalFunction); % Evaluate function to test its validity
        if isnan(val) || isinf(val)
            isFunctionOk = 0;
        else
            isFunctionOk = 1;
        end
    catch err
        isFunctionOk = 0;
    end
else
    % Perform Default Parameter Query
    if isFunctionOk == 0
        choice = questdlg(['Warning: The evaluated objective function entered cannot be executed. '     ...
                           'Ensure that x, y, z and xGoal, yGoal, zGoal variables are only specified, ' ...
                           'and the function is in a valid mathematical form with correct operators.'],    ...
                           'Evaluation Function Warning', ...
                           'Change Evaluation Function','Set to Default Function','Change Evaluation Function');
        % Handle response
        switch choice
            case 'Set to Default Function'
                gHandles.PSO.evalFunction = gHandles.PSO.defEvalFunction;
        end
    end
end

function verifyParams(hObject)
global gHandles
newVal = str2double(get(hObject, 'String'));
if (isnan(newVal) || isinf(newVal))
    setVal = gHandles.param.origVal;
    errordlg('Input Error: Please re-enter as a real, non-infinite value.','Input Error');
    uiwait(gcf); % Force to wait for confirmation
else
    setVal = newVal;
end
set(hObject, 'String', setVal);

function verifyParamsAbs(hObject)
global gHandles
newVal = abs(str2double(get(hObject, 'String')));
if (isnan(newVal) || isinf(newVal))
    setVal = gHandles.param.origVal;
    errordlg('Input Error: Please re-enter as an absolute, non-infinite value.','Input Error');
    uiwait(gcf); % Force to wait for confirmation
elseif newVal == 0
    setVal = 1;
else
    setVal = newVal;
end
set(hObject, 'String', setVal);

function verifyParamsAbsAndZero(hObject)
global gHandles
newVal = str2double(get(hObject, 'String'));
if (isnan(newVal) || isinf(newVal))
    setVal = gHandles.param.origVal;
    errordlg('Input Error: Please re-enter as an absolute, non-infinite value.','Input Error');
    uiwait(gcf); % Force to wait for confirmation
elseif newVal < 0
    setVal = 0;
else
    setVal = newVal;
end
set(hObject, 'String', setVal);

function verifyParamsAbsRound(hObject)
global gHandles
newVal = round(abs(str2double(get(hObject, 'String'))));
if (isnan(newVal) || isinf(newVal))
    setVal = gHandles.param.origVal;
    errordlg('Input Error: Please re-enter as a positive, absolute, non-infinite value.','Input Error');
    uiwait(gcf); % Force to wait for confirmation
elseif newVal == 0
    setVal = 1;
else
    setVal = newVal;
end
set(hObject, 'String', setVal);

function filePath = GetFilePath()
filePath   = [pwd '/'];
if ispc && ~isunix && ~ismac
    slashIndex = regexp(filePath,'\');
    for i = 1:size(slashIndex,2)
        filePath(slashIndex(i)) = '/';
    end
end

function verifyTrajFileEntered()
global gHandles
handles                = gHandles.handles;
gHandles.proc.trajPath = get(handles.input_TrajFile, 'String');
[~, errMsg]            = loadData(gHandles.proc.trajPath);
if strcmp(errMsg, gHandles.param.functNoError)
    verifyPathSize();
else
    resetTrajPaths();
end

function verifyTrajFileSelected()
global gHandles
handles = gHandles.handles;
[filename, pathname] = uigetfile( ...
   {'*.xls','Excel File (*.xls)';
    '*.csv','Comma Delimited File (*.csv)';
    '*.*',  'All Files (*.*)'}, ...
    'Load Data: Select Path Trajectory File', ...
    [pwd '\import']);
% Check if file input is entered
try
    if filename == 0
        resetTrajPaths();
        return
    end
catch err %#ok<*NASGU>
end
% Run file path processing
gHandles.proc.trajPath = [pathname, filename];
set(handles.input_TrajFile,'String',gHandles.proc.trajPath);
verifyPathSize();

function resetTrajPaths()
global gHandles
handles                = gHandles.handles;
gHandles.proc.trajPath = '';
set(handles.input_TrajFile,'String','Open/Type Path');

function verifyFileEntered()
global gHandles
handles = gHandles.handles;
gHandles.proc.inputPaths = get(handles.input_FilePath, 'String');
[~, errMsg]          = loadData(gHandles.proc.inputPaths);
if strcmp(errMsg, gHandles.param.functNoError)
    filesLoaded = 1;
    verifyFileParams(filesLoaded);
else
    resetPaths();
end

function verifyFileSelected()
global gHandles
handles = gHandles.handles;
[filename, pathname] = uigetfile( ...
   {'*.xls','Excel File (*.xls)';
    '*.csv','Comma Delimited File (*.csv)';
    '*.*',  'All Files (*.*)'},                  ...
   ['Load Data: '  gHandles.proc.multiFileName], ...
    'MultiSelect', gHandles.proc.multiFileSet,   ...
   [pwd '\import']);
% Check if file input is entered
try
    if filename == 0
        resetPaths();
        return
    end
catch err %#ok<*NASGU>
end
% Run file path processing
strPaths = '';
gHandles.proc.inputPaths = [pathname, filename];
filesLoad = 0;
if (iscell(gHandles.proc.inputPaths))
    % Multiple Files Selected
    filesLoad = size(gHandles.proc.inputPaths,2)-1;
    for i=1:filesLoad
        filepath = sprintf('%c',strcat(gHandles.proc.inputPaths{1}, gHandles.proc.inputPaths{i+1}));
        strPaths = [strPaths filepath '; '];
    end
else
    % Single File Selected
    filesLoad = 1;
    singleFile= 1;
    multiFile = 2;
    strPaths  = gHandles.proc.inputPaths;
    dataMode  = get(handles.popup_DataMode,'Value');
    % If a single file is selected in multi-file mode, provide suggestion
    if dataMode == multiFile
        choice = questdlg('Recommendation: You have selected a single file only in Multi-File mode. To change iterations, re-open this file using Single-File Data mode.', ...
                          'Multi-File Data Input Query', ...
                          'Change Data Mode to Single-File','Remain in Current Data Mode','Change Data Mode to Single-File');
        % Handle response
        switch choice
            case 'Change Data Mode to Single-File'
                % Change to Single-File Mode and execute settings
                set(handles.popup_DataMode,'Value',singleFile);
                dataProcessMode();
                gHandles.proc.inputPaths = strPaths; % Path renewed due to fileProcessMode
            case 'Remain in Current Data Mode'
                % Leave Settings as is
        end
    end
end
set(handles.input_FilePath,'String',strPaths);
verifyFileParams(filesLoad);

function verifyFileParams(filesLoad)
global gHandles
handles   = gHandles.handles;
dataMode  = get(handles.popup_DataMode,'Value');
multiFile = 2;
% Input-box Management For Multi-File Support
if dataMode == multiFile
    saveIterWaitTimeLastState(); % Persist Last State of Iteration & Wait-time
    set(handles.input_Iterations,'String',num2str(filesLoad));
    verifyDataMapMode();
    if filesLoad > 1
        set(handles.input_WaitTime,'Enable','on');
        set(handles.input_WaitTime,'String',gHandles.param.buffWaitTime);
        set(handles.input_WaitTime,'Value',1);
    else
        set(handles.input_WaitTime,'Enable','off');
        set(handles.input_WaitTime,'String','0');
        set(handles.input_WaitTime,'Value',0);
    end
end
% Split Interval and Interval Management & Region and Radial Management
verifyDataSize();
verifyExpTrajSize();

function dataProcessMode()
global gHandles
handles = gHandles.handles;
dataMode= fileProcessPreliminaries();
singleFile = 1;
multiFile  = 2;
randomData = 3;
psoData    = 4;
switch dataMode
    case singleFile
        gHandles.proc.multiFileSet = 'off';
        gHandles.proc.multiFileName= 'Select Single File';
        set(handles.input_FilePath,'Enable','on');
        set(handles.pb_OpenFile,'Enable','on');
        set(handles.pb_OpenFile,'String','Open File');
    case multiFile
        gHandles.proc.multiFileSet  = 'on';
        gHandles.proc.multiFileName = 'Select Multiple Files by Control Key';
        set(handles.input_FilePath,'Enable','on');
        set(handles.pb_OpenFile,'Enable','on');
        set(handles.pb_OpenFile,'String','Open Files');
    case randomData
        gHandles.proc.multiFileSet = gHandles.param.functUnavail;
        gHandles.proc.multiFileName= gHandles.param.functUnavail;
        set(handles.input_FilePath,'Enable','off');
        set(handles.pb_OpenFile,'Enable','off');
        set(handles.pb_OpenFile,'String',gHandles.param.functUnavail);
        dataRandomParamInput();
    case psoData
        gHandles.proc.multiFileSet = 'off';
        gHandles.proc.multiFileName= 'Select Single File';
        set(handles.input_FilePath,'Enable','on');
        set(handles.pb_OpenFile,'Enable','on');
        set(handles.pb_OpenFile,'String','Open File');
        psoAlgorithmParams();
end
gHandles.param.buffPrevMode = dataMode; % Set Current Mode as Previous Mode

function dataMode = fileProcessPreliminaries()
global gHandles
handles  = gHandles.handles;
dataMode = get(handles.popup_DataMode,'Value');
% Reset Custom and Path Trajectory Modes if Data Mode has been switched
if dataMode ~= gHandles.param.buffPrevMode
    dlgVisible = 0;
    gHandles.param.buffPathWarn = 0; % Re-verify changes are applicable
    set(handles.cb_EnableCustomData,'Value',1);
    set(handles.cb_EnablePathTraj,'Value',1);
    enableTrajectoryMode();
    enableManipulationMode(dlgVisible);
    resetPaths(); % Ensure this is last function called
end

function resetPaths()
global gHandles
gHandles.proc.inputPaths = '';
saveIterWaitTimeLastState(); % Persist Last State of Iteration & Wait-time
handles    = gHandles.handles;
dataMode   = get(handles.popup_DataMode,'Value');
singleFile = 1;
multiFile  = 2;
randomData = 3;
psoData    = 4;
switch dataMode
    case singleFile
        set(handles.input_FilePath,'String','Open/Type Path');
        set(handles.input_Iterations,'Value',1);
        set(handles.input_WaitTime,'Value',1);
    case multiFile
        set(handles.input_FilePath,'String','Open/Type Paths');
        set(handles.input_Iterations,'Enable','off');
        set(handles.input_Iterations,'String','0');
        set(handles.input_Iterations,'Value',0);
        set(handles.input_WaitTime,'Enable','off');
        set(handles.input_WaitTime,'String','0');
        set(handles.input_WaitTime,'Value',0);
    case randomData
        set(handles.input_FilePath,'String','Not Applicable');
        set(handles.input_Iterations,'Value',1);
        set(handles.input_WaitTime,'Value',1);
    case psoData
        set(handles.input_FilePath,'String','Open Path Option');
        set(handles.input_Iterations,'Enable','off');
        set(handles.input_Iterations,'String','0');
        set(handles.input_Iterations,'Value',0);
        set(handles.input_WaitTime,'Enable','on')
        set(handles.input_WaitTime,'String',gHandles.param.buffWaitTime);
        set(handles.input_WaitTime,'Value',1);
end

function preInitUIBeforeRun()
global gHandles
handles = gHandles.handles;
scrollTextField(handles.edit_OutputData);
enableButtonUI('off','Please Wait');
disableManipulationMode();
disableTrajectoryMode();
enableInputDataUI('off');
enableStemUI('off');

function postInitUIAfterRun()
global gHandles
handles   = gHandles.handles;
dlgVisible= 0;
scrollTextField(handles.edit_OutputData);
enableButtonUI('on','Run Algorithm');
enableManipulationMode(dlgVisible);
enableTrajectoryMode();
enableInputDataUI('on');
enableStemUI('on');

function enableButtonUI(isEnabled, strValue)
global gHandles
handles = gHandles.handles;
set(handles.input_FilePath,   'Enable',isEnabled);
set(handles.pb_OpenFile,      'Enable',isEnabled);
set(handles.pb_ResetParams,   'Enable',isEnabled);
set(handles.pb_ClearOutput,   'Enable',isEnabled);
set(handles.pb_RunAlgorithm,  'Enable',isEnabled);
set(handles.pb_RunAlgorithm,  'String',strValue);
if strcmp(isEnabled,'on')
    set(handles.pb_StopAlgorithm, 'Enable','off');
    if strcmp(get(handles.pb_OpenFile,'String'),gHandles.param.functUnavail)
        set(handles.pb_OpenFile,   'Enable','off');
        set(handles.input_FilePath,'Enable','off');
    end
else
    set(handles.pb_StopAlgorithm,'Enable','on');
end

function enableInputDataUI(isEnabled)
global gHandles
handles = gHandles.handles;
set(handles.popup_DataMode,   'Enable',isEnabled);
set(handles.popup_DataMap,    'Enable',isEnabled);
set(handles.popup_ExportData, 'Enable',isEnabled);
set(handles.popup_ExportImage,'Enable',isEnabled);
set(handles.popup_ExportVideo,'Enable',isEnabled);
% Custom Support for Iterations and Wait-time inputbox
if get(handles.input_Iterations,'Value') == 1
    set(handles.input_Iterations,'Enable',isEnabled);
end
if get(handles.input_WaitTime,'Value') == 1
    set(handles.input_WaitTime,'Enable',isEnabled);
end

function enableStemUI(isEnabled)
global gHandles
handles = gHandles.handles;
set(handles.input_Epsilon,    'Enable',isEnabled);
set(handles.input_Kr,         'Enable',isEnabled);
set(handles.input_Ktheta,     'Enable',isEnabled);
set(handles.input_Kphi1,      'Enable',isEnabled);
set(handles.input_Kphi3,      'Enable',isEnabled);
set(handles.input_LongTerm,   'Enable',isEnabled);

function clearOutput()
global gHandles
handles = gHandles.handles;
updateWaitbar(0);
% To ensure free memory (hessResult, fData, buffTrajData, PSO data)
gHandles.proc.stopProcess   = 0;
gHandles.proc.hessResult    = {};
gHandles.fData              = [];
gHandles.param.buffTrajData = [];
gHandles.param.buffTrajIndex= 0;
gHandles.param.runHessBuff  = {};
gHandles.PSO.axisLims       = [];
gHandles.PSO.swarmData      = [];
clf(gHandles.fig.fig1);
clf(gHandles.fig.fig2); 
clf(gHandles.fig.fig3); 
clf(gHandles.fig.fig4); 
clf(gHandles.fig.fig5);
clf(gHandles.fig.fig6);
set(gHandles.fig.fig1,'Name','Fig #1 Window (Auto-Close)');
set(gHandles.fig.fig2,'Name','Fig #2 Window (Auto-Close)');
set(gHandles.fig.fig3,'Name','Fig #3 Window (Auto-Close)');
set(gHandles.fig.fig4,'Name','Fig #4 Window (Auto-Close)');
set(gHandles.fig.fig5,'Name','Fig #5 Window (Auto-Close)');
set(gHandles.fig.fig6,'Name','Fig #6 Window (Auto-Close)');
set(handles.uitable_StemOutput1,'ColumnWidth',{'Auto'});
set(handles.uitable_StemOutput2,'ColumnWidth',{'Auto'});
set(handles.uitable_StemOutput3,'ColumnWidth',{'Auto'});
set(handles.uitable_StemOutput4,'ColumnWidth',{'Auto'});
set(handles.uitable_StemOutput1,'Data', []);
set(handles.uitable_StemOutput2,'Data', []);
set(handles.uitable_StemOutput3,'Data', []);
set(handles.uitable_StemOutput4,'Data', []);
try %#ok<ALIGN>
    gHandles.fig.fig1_1_initialize = 0;
    set(gHandles.fig.fig1_1,'CloseRequestFcn',gHandles.fig.fig1_1Close);
    close(gHandles.fig.fig1_1);
catch err; end

function resetParams()
global gHandles
% Set Default Parameters for Functions
gHandles.PSO.axisLims       = [];                               % General PSO Parameters (Refer to PSO.m)
gHandles.PSO.swarmData      = [];
gHandles.PSO.iterations     = 30;
gHandles.PSO.inertia        = 1.0;
gHandles.PSO.correctFactor  = 2.0;
gHandles.PSO.bestValue      = 1000;
gHandles.PSO.xGoal          = 3;
gHandles.PSO.yGoal          = 3;
gHandles.PSO.zGoal          = 3;
gHandles.PSO.swarmSize      = 125;
gHandles.PSO.percentAdjust  = gHandles.param.maxPercent;
gHandles.PSO.defEvalFunction= '(x-xGoal)^2 + (y-yGoal)^2 + (z-zGoal)^2';
gHandles.PSO.evalFunction   = gHandles.PSO.defEvalFunction;
gHandles.param.epsilonVal   = 0.36;                             % Basic STEM Parameters
gHandles.param.longTermVal  = 120;                              % Basic STEM Parameters
gHandles.param.waitTime     = 0.05;                             % Default wait time between intervals
gHandles.param.iterations   = 10;                               % Default Iterations for Processing
gHandles.param.frameRate    = 10;                               % In Frames per Second (FPS)
gHandles.param.buffWaitTime = gHandles.param.waitTime;          % Set Buffer Wait Time Values to Defaults
gHandles.param.buffIteration= gHandles.param.iterations;        % Set Buffer Iteration Values to Defaults
gHandles.param.buffPathWarn = 0;                                % Re-verify changes are applicable
gHandles.param.buffTrajData = [];
gHandles.param.buffTrajIndex= 0;
gHandles.param.buffPrevMode = 0;                                % Reset Previous Data Mode for Re-initialization
gHandles.param.trajDefRadian= pi/4;                             % Default value for shiftRadial operation
gHandles.param.figWriterObj = cell(1,1);                        % For VideoWriter class
gHandles.param.randElements = 50;                               % For functional utility parameters
gHandles.param.randUpBound  = 10;
gHandles.param.randLowBound = -10;
gHandles.param.regionExpType= 'Region-Expand';
gHandles.param.regionExpValX= 5;
gHandles.param.regionExpValY= 5;
gHandles.param.regionExpValZ= 5;
gHandles.param.regionExpMode= 1;
gHandles.param.regionRandSel= 1;
gHandles.param.regionManual = 0;
gHandles.param.radialExpType= 'Radial-Expand';
gHandles.param.radialExpVal = 5;
gHandles.param.radialExpMode= 1;
gHandles.param.radialRandSel= 1;
gHandles.param.radialManual = 0;
gHandles.param.radialTheta  = pi/4;
gHandles.param.radialPhi    = pi/4;
gHandles.param.showType     = gHandles.param.dataTypes{4};      % Plot Display Type (HV4)
gHandles.param.showDetType  = gHandles.param.dataTypes{4};      % Plot Determinant Type (HV4, Also consider All)
gHandles.param.showAzim     = 0;                                % 3D Plot Azimuth
gHandles.param.showElev     = 90;                               % 3D Plot Elevation
gHandles.param.dataAzim     = -37.5;                            % Data Plot Azimuth
gHandles.param.dataElev     = 30;                               % Data Plot Elevation
gHandles.param.contLayers   = 25;                               % Number of layers for Contour plot
gHandles.param.chartType    = 1;                                % Plot Type: 1-Surface,2-Mesh,3-Contour
gHandles.param.cholMod      = 1;                                % Cholesky Modification: 0-Use Matrix, 1-LU Decomposition (Lower), 2-Use LU Decomposition (Permutation)
gHandles.param.showHess     = 1;                                % Hessian Direction Arrows (Uses gHandles.param.showType for Hessian value)
gHandles.param.showHessShade= 90;                               % Hessian Shading Type: -2: Disable, -1: AutoShade, 0-100: Show Threshold Percentage
gHandles.param.showHessGrade= 20;                               % Hessian Shading Gradient
gHandles.param.runHessDvFact= 2;                                % Auto-Scale Normalize Factor Default value
gHandles.param.runHessNormal= [1 gHandles.param.runHessDvFact]; % Normalize Values: -3:Disable, -2:Log, 1:Log2, 0:Log10, 1:Auto-Scale & Auto-Scale Factor, >1:Division by Value
gHandles.param.runHessDiff  = 1;                                % Hessian Diff Values: 0:Disable, 1:Enable
gHandles.param.runHessBuff  = {};
% Set UI Parameters to Defaults
dlgVisible= 0;
handles   = gHandles.handles;
epsilon   = gHandles.param.epsilonVal;
longTerm  = gHandles.param.longTermVal;
waitTime  = gHandles.param.waitTime;
iteration = gHandles.param.iterations;
set(handles.input_WaitTime,            'String',waitTime);% Set System & STEM Defaults
set(handles.input_Epsilon,             'String',epsilon);
set(handles.input_LongTerm,            'String',longTerm);
set(handles.input_Iterations,          'String',iteration);
set(handles.pb_StopAlgorithm,          'Enable','off');
set(handles.popup_DataMode,            'Value',1);
set(handles.popup_DataMap,             'Value',1);
set(handles.popup_ExportData,          'Value',1);
set(handles.popup_ExportImage,         'Value',1);
set(handles.popup_ExportVideo,         'Value',1);
defaultStemParams();
dataProcessMode();
set(handles.input_SplitInterval,       'String',3);       % Custom Manipulation Settings
set(handles.input_Interval,            'String',1);
set(handles.input_RandVar,             'String',5);
set(handles.input_RandPop,             'String',5);
set(handles.input_Xshift,              'String',5);
set(handles.input_Yshift,              'String',0);
set(handles.input_Zshift,              'String',0);
set(handles.input_Xrot,                'String',5);
set(handles.input_Yrot,                'String',0);
set(handles.input_Zrot,                'String',0);
set(handles.cb_EnableCustomData,       'Value',1);
set(handles.popup_ManipulationMode,    'Value',1);
set(handles.popup_IncreaseMode,        'Value',1);
enableManipulationMode(dlgVisible);
set(handles.input_TrajMoveRad,         'String',1);       % Custom Path Trajectory Settings
set(handles.input_TrajRandVar,         'String',5);
set(handles.input_TrajRandPop,         'String',5);
set(handles.input_TrajXShift,          'String',5);
set(handles.input_TrajYShift,          'String',0);
set(handles.input_TrajZShift,          'String',0);
set(handles.input_TrajXRot,            'String',5);
set(handles.input_TrajYRot,            'String',0);
set(handles.input_TrajZRot,            'String',0);
set(handles.cb_EnablePathTraj,         'Value',1);
set(handles.cb_ShiftRadialLength,      'Value',1);
set(handles.popup_TrajManipulationType,'Value',1);
enableTrajectoryMode();
resetTrajPaths();
set(handles.cb_ShowAlgResults,         'Value',1);        % Custom Figure Settings
set(handles.cb_ShowFig1,               'Value',1);
set(handles.cb_ShowFig2,               'Value',1);
set(handles.cb_ShowFig3,               'Value',1);
set(handles.cb_ShowFig4,               'Value',1);
set(handles.cb_ShowFig5,               'Value',1);
set(handles.cb_ShowFig6,               'Value',1);
set(handles.popup_HessColour,          'Value',4);        % Green-Lime
set(handles.popup_TrajColour,          'Value',3);        % Red-Pink
set(handles.popup_PointColour,         'Value',1);        % Blue-BlueGray
set(handles.popup_MarkColour,          'Value',1);        % Blue
set(handles.popup_ColourMap,           'Value',10);       % Jet

function defaultStemParams()
global gHandles
handles = gHandles.handles;
epsilon = str2double(get(handles.input_Epsilon,'String'));
k_r     = 100*epsilon;
k_theta = 20 *epsilon;
k_phi1  = 1  *epsilon;
k_phi3  = 0.5*epsilon;
set(handles.input_Kr,      'String',k_r);
set(handles.input_Ktheta,  'String',k_theta);
set(handles.input_Kphi1,   'String',k_phi1);
set(handles.input_Kphi3,   'String',k_phi3);

function figureCaptureWarning(persistType)
global gHandles
handles = gHandles.handles;
showFlag = get(handles.cb_ShowFig1,'Value');
showFlag = get(handles.cb_ShowFig2,'Value') + showFlag;
showFlag = get(handles.cb_ShowFig3,'Value') + showFlag;
showFlag = get(handles.cb_ShowFig4,'Value') + showFlag;
showFlag = get(handles.cb_ShowFig5,'Value') + showFlag;
showFlag = get(handles.cb_ShowFig6,'Value') + showFlag;
if showFlag == 0
    warndlg(['Save Warning: For ' persistType ' capture, select at least one figure checkbox.'], 'Capture Warning');
    uiwait(gcf); % Force to wait for confirmation
end

function psoAlgorithmParams()
global gHandles
result = cell(1);
while cellfun(@isempty,result)
    options.Interpreter='tex';
    result = inputdlg({'PSO Maximum Iterations: (Overrides Iteration Value set in main panel.)',         ...
                       'PSO Inertia Factor:          (Suggested: 1.0)',                                  ...
                       'PSO Correction Factor:   (Suggested: 2.0)',                                      ...
                       'PSO Initial Best Value:    (Suggested: 1000)',                                   ...
                       'Goal to Reach:                (X-Coordinate)',                                   ...
                       'Goal to Reach:                (Y-Coordinate)',                                   ...
                       'Goal to Reach:                (Z-Coordinate)',                                   ...
                       'Percentage Adjustment:  (Specify % Value from 0 to 100)',                      ...
                       'Swarm Size Generation: (If File Input is used, this parameter will be ignored.)',...
                       'Evaluation Function:        (Default: (x-xGoal)^2+(y-yGoal)^2+(z-zGoal)^2)'},    ...
                       'PSO Algorithm Input Parameters',    ...
                       1,                                   ... % Input Numlines
                       {num2str(gHandles.PSO.iterations),   ...
                        num2str(gHandles.PSO.inertia),      ...
                        num2str(gHandles.PSO.correctFactor),...
                        num2str(gHandles.PSO.bestValue),    ...
                        num2str(gHandles.PSO.xGoal),        ...
                        num2str(gHandles.PSO.yGoal),        ...
                        num2str(gHandles.PSO.zGoal),        ...
                        num2str(gHandles.PSO.percentAdjust),...
                        num2str(gHandles.PSO.swarmSize),    ...
                        gHandles.PSO.evalFunction},         ...
                        options);
    try
        errMsg         = '';
        iterations     = str2double(result{1});
        inertia        = str2double(result{2});
        correctFactor  = str2double(result{3});
        bestValue      = str2double(result{4});
        xGoal          = str2double(result{5});
        yGoal          = str2double(result{6});
        zGoal          = str2double(result{7});
        percentAdjust  = str2double(result{8});
        swarmSize      = str2double(result{9});
        [isFunctionOk, evalFunction] = verifyEvalFunction(result{10});
        
        if (isnan(iterations) || isinf(iterations) || iterations < 1)
            errMsg = [errMsg 'Iterations value must be a valid, non-infinite integer greater than 0. '];
        end
        if (isnan(inertia) || isinf(inertia))
            errMsg = [errMsg 'Inertia value input must be a numerically valid, non-infinite integer. '];
        end
        if (isnan(correctFactor) || isinf(correctFactor))
            errMsg = [errMsg 'Correction Factor value must be numerically valid, non-infinite integer. '];
        end
        if (isnan(bestValue) || isinf(bestValue))
            errMsg = [errMsg 'Best input value must be a numerically valid, non-infinite integer. '];
        end
        if (isnan(xGoal) || isinf(xGoal))
            errMsg = [errMsg 'X-Coordinate value must be a numerically valid, non-infinite integer. '];
        end
        if (isnan(yGoal) || isinf(yGoal))
            errMsg = [errMsg 'Y-Coordinate value must be a numerically valid, non-infinite integer. '];
        end
        if (isnan(zGoal) || isinf(zGoal))
            errMsg = [errMsg 'Z-Coordinate value must be a numerically valid, non-infinite integer. '];
        end
        if ~(percentAdjust >= 0 && percentAdjust <= gHandles.param.maxPercent)
            errMsg = [errMsg 'Percentage Adjustment must be between 0 and 100 percentage points only. '];
        end
        if (isnan(swarmSize) || isinf(swarmSize) || swarmSize < 1)
            errMsg = [errMsg 'Swarm Size value must be a valid, non-infinite integer greater than zero.'];
        end
        if ~isFunctionOk
            errMsg = [errMsg 'Objective function cannot be evaluated as a valid mathematical form. '];
        end
        % Throw Error Message if failed verification
        if ~strcmp(errMsg,'');
            throw(MException('General:Input',errMsg));
        end
    catch err
        if ~strcmp(err.identifier,'MATLAB:badsubscript')
            errordlg(err.message, 'Input Error', 'replace');
            uiwait(gcf); % Force to wait for confirmation
            verifyEvalFunction(evalFunction, isFunctionOk);
        else
            warndlg('User Warning: Please try again and enter parameters by clicking OK.', 'User Warning', 'replace');
            uiwait(gcf); % Force to wait for confirmation
        end
        result = cell(1);
    end
end
% Return Settings & and Set Default Parameters
gHandles.PSO.iterations    = round(abs(iterations));
gHandles.PSO.inertia       = inertia;
gHandles.PSO.correctFactor = correctFactor;
gHandles.PSO.bestValue     = bestValue;
gHandles.PSO.xGoal         = xGoal;
gHandles.PSO.yGoal         = yGoal;
gHandles.PSO.zGoal         = zGoal;
gHandles.PSO.swarmSize     = round(abs(swarmSize));
gHandles.PSO.percentAdjust = percentAdjust;
gHandles.PSO.evalFunction  = evalFunction;
% Set as PSO values as default (Plus one for original source)
set(gHandles.handles.input_Iterations, 'String', num2str(gHandles.PSO.iterations+1));

function strVal = parseAsString(inputSet, rad2Deg)
strVal = '';
for i=1:size(inputSet,2)
    if nargin == 1
        if i == size(inputSet,2); strVal = [strVal num2str(inputSet(i))]; %#ok<*AGROW>
        else                      strVal = [strVal num2str(inputSet(i)) ',']; %#ok<*AGROW>
        end
    elseif rad2Deg == 1;
        if i == size(inputSet,2); strVal = [strVal num2str(radtodeg(inputSet(i)))]; %#ok<*AGROW>
        else                      strVal = [strVal num2str(radtodeg(inputSet(i))) ',']; %#ok<*AGROW>
        end
    end
end

function regionExpansionParams()
global gHandles
errMsg = '';
result = cell(1);
while cellfun(@isempty,result)
    result = inputdlg({'Region Expansion Amount X:        (Use '','' as delimiter & X,Y,Z is equal pair)', ...
                       'Region Expansion Amount Y:        (Use '','' as delimiter & X,Y,Z is equal pair)', ...
                       'Region Expansion Amount Z:        (Use '','' as delimiter & X,Y,Z is equal pair)', ...
                       'Expand Mode for Single Amount:  (1-Fwd, 2-Fwd&Rev, 3-Fwd&FullRev, 4-FullCycle)',   ...
                       'No of Values to Select Randomly: (Set 0-Disable, Rand or Manual only choices)',    ...
                       'Values Manually Specified:           (Use '','' as delimiter, or Set 0-Disable)'}, ...
                       'Region Expansion Parameters',...
                       1, ... % Numlines
                       {parseAsString(gHandles.param.regionExpValX),...
                        parseAsString(gHandles.param.regionExpValY),...
                        parseAsString(gHandles.param.regionExpValZ),...
                        num2str(gHandles.param.regionExpMode),      ...
                        num2str(gHandles.param.regionRandSel),      ...
                        parseAsString(gHandles.param.regionManual)});
    try
        errMsg       = '';
        regionExpValX= str2double(regexp(result{1},',','split'));
        regionExpValY= str2double(regexp(result{2},',','split'));
        regionExpValZ= str2double(regexp(result{3},',','split'));
        regionExpMode= round(str2double(result{4}));
        regionRandSel= round(str2double(result{5}));
        regionManual = round(str2double(regexp(result{6},',','split')));
        regionExpXLen= size(regionExpValX,2);
        regionExpYLen= size(regionExpValY,2);
        regionExpZLen= size(regionExpValZ,2);
        
        if ~(regionExpMode == 1 || regionExpMode == 2 || regionExpMode == 3 || regionExpMode == 4)
            errMsg = [errMsg 'Region Expansion Mode must be set as value of 1, 2, 3 or 4 Only. ']; %#ok<AGROW>
        end
        if isnan(regionRandSel) || isinf(regionRandSel)
            errMsg = [errMsg 'Region Random data input must be a valid, non-infinte numeral. ']; %#ok<AGROW>
        end
        if ~(regionExpXLen == regionExpYLen && regionExpXLen == regionExpZLen)
            errMsg = [errMsg 'Region Expansion Value Pairs (X,Y & Z) must be of equal size. ']; %#ok<AGROW>
        else
            for i=1:regionExpXLen
                if (isnan(regionExpValX(i)) || isnan(regionExpValY(i)) || isnan(regionExpValZ(i)) || ...
                    isinf(regionExpValX(i)) || isinf(regionExpValY(i)) || isinf(regionExpValZ(i)))
                    errMsg = 'Ensure Region data input for X,Y & Z is a valid, non-infinite numeral. ';
                    break
                end
                if (regionExpValX(i) < 0) || (regionExpValY(i) < 0) || (regionExpValZ(i) < 0)
                    errMsg = [errMsg 'Region Expansion Value must be greater than or equal to zero. ']; %#ok<AGROW>
                    break
                end
            end
        end
        for i=1:size(regionManual,2)
            if isnan(regionManual(i)) || isinf(regionManual(i))
                errMsg = [errMsg 'Ensure Manual Value data input is a valid, non-infinite numeral. ']; %#ok<AGROW>
                break
            end
            if regionManual(i) < 1 && size(regionManual,2) > 1
                errMsg = [errMsg 'Manual Value ID data must be greater than zero (To disable, just type 0). ']; %#ok<AGROW>
                break
            end
            if regionManual(i) < 0 || regionRandSel < 0
                errMsg = [errMsg 'Manual Value ID data or Random data set cannot be less than zero. ']; %#ok<AGROW>
                break
            end
            if regionManual(i) == 0 && regionRandSel == 0
                errMsg = [errMsg 'Manual Value ID data and Random data set cannot be disabled simulaneously. ']; %#ok<AGROW>
                break
            end
            if regionManual(i) >= 1 && regionRandSel >= 1
                errMsg = [errMsg 'Manual Value ID data and Random data set cannot be enabled simulaneously. ']; %#ok<AGROW>
                break
            end
        end
        if ~verifyExpTrajSize(regionRandSel, regionManual)
            errMsg = [errMsg 'Manual Value ID or Random Data must not be greater than data array size.']; %#ok<AGROW>
        end
        % Throw Error Message if failed verification
        if ~strcmp(errMsg,'');
            throw(MException('General:Input',errMsg));
        end
    catch err
        if ~strcmp(err.identifier,'MATLAB:badsubscript')
            errordlg(['Input Error: ' err.message], 'Input Error', 'replace');
        else
            warndlg('User Warning: Please try again and enter parameters by clicking OK.', 'User Warning', 'replace');
        end
        uiwait(gcf); % Force to wait for confirmation
        result = cell(1);
    end
end
regionExpMode = verifyExpansionMode(regionExpXLen, regionExpMode);
gHandles.param.regionExpValX= regionExpValX;
gHandles.param.regionExpValY= regionExpValY;
gHandles.param.regionExpValZ= regionExpValZ;
gHandles.param.regionExpMode= regionExpMode;
gHandles.param.regionRandSel= regionRandSel;
gHandles.param.regionManual = regionManual;

function radialExpansionParams()
global gHandles
errMsg  = '';
result  = cell(1);
rad2Deg = 1;
while cellfun(@isempty,result)
    options.Interpreter='tex';
    result = inputdlg({'Radius Expansion Amount:           (Delimiter: '',''; Pair r,\theta,\phi)',       ...
                       'Inclination/Polar Angle:                  (Delimiter: '',''; Pair r,\theta,\phi)',...
                       'Azimuth/Azimuthal Angle:             (Delimiter: '',''; Pair r,\theta,\phi)',     ...
                       'Expand Mode for Single Amount:  (1-Fwd, 2-Fwd&Rev, 3-Fwd&FullRev, 4-FullCycle)',  ...
                       'No of Values to Select Randomly: (Set 0-Disable, Rand or Manual only choices)',   ...
                       'Values Manually Specified:           (Use '','' as delimiter, or Set 0-Disable)'},...
                       'Radial Expansion Parameters', ...
                       1, ... % Numlines
                       {parseAsString(gHandles.param.radialExpVal),       ...
                        parseAsString(gHandles.param.radialTheta,rad2Deg),...
                        parseAsString(gHandles.param.radialPhi,rad2Deg),  ...
                        num2str(gHandles.param.radialExpMode),            ...
                        num2str(gHandles.param.radialRandSel),            ...
                        parseAsString(gHandles.param.radialManual)},      ...
                        options);
    try
        errMsg       = '';
        radialExpVal = str2double(regexp(result{1},',','split'));
        radialTheta  = degtorad(str2double(regexp(result{2},',','split')));
        radialPhi    = degtorad(str2double(regexp(result{3},',','split')));
        radialExpMode= round(str2double(result{4}));
        radialRandSel= round(str2double(result{5}));
        radialManual = round(str2double(regexp(result{6},',','split')));
        radialExpVLen= size(radialExpVal,2);
        radialThetLen= size(radialTheta,2);
        radialPhiLen = size(radialPhi,2);
        
        if ~(radialExpMode == 1 || radialExpMode == 2 || radialExpMode == 3 || radialExpMode == 4)
            errMsg = [errMsg 'Radial Expansion Mode must be set as value of 1, 2, 3 or 4 Only. ']; %#ok<AGROW>
        end
        if (isnan(radialRandSel) || isinf(radialRandSel))
            errMsg = [errMsg 'Radial Random data input must be a valid, non-infinte numeral. ']; %#ok<AGROW>
        end
        if ~(radialExpVLen == radialThetLen && radialExpVLen == radialPhiLen)
            errMsg = [errMsg 'Radial Value Pairs (Radius, Theta and Phi) must be same size. ']; %#ok<AGROW>
        else
            for i=1:radialExpVLen
                if (isnan(radialExpVal(i)) || isinf(radialExpVal(i)))
                    errMsg = [errMsg 'Ensure Radial data input value is a valid, non-infinite numeral. ']; %#ok<AGROW>
                    break
                end
                if (radialExpVal(i) <= 0)
                    errMsg = [errMsg 'Radial Expansion Value must be greater than zero to be validated. ']; %#ok<AGROW>
                    break
                end
                if (isnan(radialTheta(i)) || isnan(radialPhi(i)) || isinf(radialTheta(i)) || isinf(radialPhi(i)))
                    errMsg = [errMsg 'Ensure Theta or Phi degree input values are valid and non-infinite. ']; %#ok<AGROW>
                    break
                end
            end
        end
        for i=1:size(radialManual,2)
            if isnan(radialManual(i)) || isinf(radialManual(i))
                errMsg = [errMsg 'Ensure Manual Value data input is a valid, non-infinite numeral. ']; %#ok<AGROW>
                break
            end
            if radialManual(i) < 1 && size(radialManual,2) > 1
                errMsg = [errMsg 'Manual Value ID data must be greater than zero (To disable, just type 0). ']; %#ok<AGROW>
                break
            end
            if radialManual(i) < 0 || radialRandSel < 0
                errMsg = [errMsg 'Manual Value ID data or Random data set cannot be less than zero. ']; %#ok<AGROW>
                break
            end
            if radialManual(i) == 0 && radialRandSel == 0
                errMsg = [errMsg 'Manual Value ID data and Random data set cannot be disabled simulaneously. ']; %#ok<AGROW>
                break
            end
            if radialManual(i) >= 1 && radialRandSel >= 1
                errMsg = [errMsg 'Manual Value ID data and Random data set cannot be enabled simulaneously. ']; %#ok<AGROW>
                break
            end
        end
        if ~verifyExpTrajSize(radialRandSel, radialManual)
            errMsg = [errMsg 'Manual Value ID or Random Data must not be greater than data array size.']; %#ok<AGROW>
        end
        % Throw Error Message if failed verification
        if ~strcmp(errMsg,'');
            throw(MException('General:Input',errMsg));
        end
    catch err
        if ~strcmp(err.identifier,'MATLAB:badsubscript')
            errordlg(['Input Error: ' err.message], 'Input Error', 'replace');
        else
            warndlg('User Warning: Please try again and enter parameters by clicking OK.', 'User Warning', 'replace');
        end
        uiwait(gcf); % Force to wait for confirmation
        result = cell(1);
    end
end
radialExpMode = verifyExpansionMode(radialExpVLen, radialExpMode);
gHandles.param.radialExpVal = radialExpVal;
gHandles.param.radialTheta  = radialTheta;
gHandles.param.radialPhi    = radialPhi;
gHandles.param.radialExpMode= radialExpMode;
gHandles.param.radialRandSel= radialRandSel;
gHandles.param.radialManual = radialManual;

function dataRandomParamInput()
global gHandles
result = cell(1);
while cellfun(@isempty,result)
    result = inputdlg({'Number of Data Elements to Randomly Generate:','Maximum Upper Bound of Data:','Minimum Lower Bound of Data:'}, ...
                       'Random Generator', ...
                       1, ... % Numlines
                       {num2str(gHandles.param.randElements), ...
                        num2str(gHandles.param.randUpBound) , ...
                        num2str(gHandles.param.randLowBound)});
    try
        randElements = str2double(result{1});
        randUpBound  = str2double(result{2});
        randLowBound = str2double(result{3});
        
        if (isnan(randElements) || isnan(randUpBound) || isnan(randLowBound) || isinf(randElements) || isinf(randUpBound) || isinf(randLowBound))
            throw(MException('General:Input','Ensure data input is a valid, non-infinite numeral.'));
        elseif (randElements <= 0)
            throw(MException('General:Input','Number of Elements must be greater than 0.'));
        elseif (randUpBound == randLowBound)
            throw(MException('General:Input','Upper Bound must not be equal to Lower Bound.'));
        elseif (randUpBound <= randLowBound)
            throw(MException('General:Input','Upper Bound must be greater than Lower Bound.'));
        end
    catch err
        if ~strcmp(err.identifier,'MATLAB:badsubscript')
            errordlg(['Input Error: ' err.message], 'Input Error', 'replace');
        else
            warndlg('User Warning: Please try again and enter parameters by clicking OK.', 'User Warning', 'replace');
        end
        uiwait(gcf); % Force to wait for confirmation
        result = cell(1);
    end
end
gHandles.param.randElements = round(abs(randElements));
gHandles.param.randUpBound  = randUpBound;
gHandles.param.randLowBound = randLowBound;

function dataFrameInput()
global gHandles
result = cell(1);
while cellfun(@isempty,result)
    result = inputdlg({'Enter Frames per Second for Video Recording (FPS):'}, ...
                       'Recording Parameters', ...
                       1, ... % Numlines
                       {num2str(gHandles.param.frameRate)});
    try
        frameRateInput = str2double(result{1});
        
        if (frameRateInput <= 0)
            throw(MException('General:Input','Frames per Second must be greater than 0.'));
        elseif (isnan(frameRateInput) || isinf(frameRateInput))
            throw(MException('General:Input','Frames per Second must be a valid, non-infinite integer.'));
        end
    catch err
        if ~strcmp(err.identifier,'MATLAB:badsubscript')
            errordlg(['Input Error: ' err.message], 'Input Error', 'replace');
        else
            warndlg('User Warning: Please try again and enter parameters by clicking OK.', 'User Warning', 'replace');
        end
        uiwait(gcf); % Force to wait for confirmation
        result = cell(1);
    end
end
gHandles.param.frameRate = round(abs(frameRateInput));

function preProcessParamInput()
global gHandles
result = cell(1);
while cellfun(@isempty,result)
    result = inputdlg({'Show Hessian Plotting Type:      (Fig 1,5,6: HV1, HV2, HV3, HV4, HVAdd)',          ...
                       'Show Hessian Determinant:       (Fig 2: HV1, HV2, HV3, HV4, HVAdd, All)',          ...
                       'Show Plot Type ID:                      (Fig 5,6: 1:Surface, 2:Mesh, 3:Contour)',  ...
                       'Set 3D Plot Azimuth Value Pair:  (Fig 5,6: Pair Input   0,    0, -50, -37.5, 0)',  ...
                       'Set 3D Plot Elevation Value Pair: (Fig 5,6: Pair Input 90, -90,  30,     30, 0)',  ...
                       'Set Contour Plot Layer:                (Fig 5,6: Contour Plot Parameter 25)',      ...
                       'Set Cholesky Modification:           (Fig 6: 0:Matrix, 1:Low Decomp, 2:Permut)',   ...
                       'Show Hessian Directional Plot:    (Fig 1: Choice of 0-Disable, 1-Enable)',         ...
                       'Show Hessian Shading Thresh.: (Fig 1: -2:None, -1:Auto, 0-100:Max Only to All)',   ...
                       'Show Hessian Gradient Value:   (Fig 1: 1:Lightest, >1:Darker Larger Gradient)',    ...
                       'Set 3D Data Azimuth Value Pair: (Fig 1: Pair Input   0,    0, -50, -37.5, 0)',     ...
                       'Set 3D Data Elevation Value Pair:(Fig 1: Pair Input 90, -90,  30,     30, 0)',     ...
                       'Show Hessian Value Difference:(All Figures & Data Values: 0:Disable, 1:Enable)',   ...
                       ['Show Hessian Normalization:     (Fig 1: -3:None, -2,-1,0:Logarithm (e, 2, 10), .' ...
                        '                                                   1:Auto-Scale, >1:Divide By)']},...
                       'Algorithm Preprocessing Parameters',        ...
                       1,                                           ... % Input Numlines
                       {gHandles.param.showType,                    ...
                        gHandles.param.showDetType,                 ...
                        num2str(gHandles.param.chartType),          ...
                        num2str(gHandles.param.showAzim),           ...
                        num2str(gHandles.param.showElev),           ...
                        num2str(gHandles.param.contLayers),         ...
                        num2str(gHandles.param.cholMod),            ...
                        num2str(gHandles.param.showHess),           ...
                        num2str(gHandles.param.showHessShade),      ...
                        num2str(gHandles.param.showHessGrade),      ...
                        num2str(gHandles.param.dataAzim),           ...
                        num2str(gHandles.param.dataElev),           ...
                        num2str(gHandles.param.runHessDiff),        ...
                        parseAsString(gHandles.param.runHessNormal)});
    try
        errMsg          = '';
        showType        = result{1};
        showDetType     = result{2};
        chartType       = str2double(result{3});
        showAzim        = str2double(result{4});
        showElev        = str2double(result{5});
        contLayers      = str2double(result{6});
        cholMod         = str2double(result{7});
        showHess        = round(str2double(result{8}));
        showHessShade   = str2double(result{9});
        showHessGrade   = str2double(result{10});
        dataAzim        = str2double(result{11});
        dataElev        = str2double(result{12});
        runHessDiff     = str2double(result{13});
        runHessNormal   = str2double(regexp(result{14},',','split'));
        
        showTypeTest    = ~(strcmpi(showType,gHandles.param.dataTypes{1}) || ...
                            strcmpi(showType,gHandles.param.dataTypes{2}) || ...
                            strcmpi(showType,gHandles.param.dataTypes{3}) || ...
                            strcmpi(showType,gHandles.param.dataTypes{4}) || ...
                            strcmpi(showType,gHandles.param.dataTypes{5}));
        showTypeDetTest = ~(strcmpi(showDetType,gHandles.param.dataTypes{1}) || ...
                            strcmpi(showDetType,gHandles.param.dataTypes{2}) || ...
                            strcmpi(showDetType,gHandles.param.dataTypes{3}) || ...
                            strcmpi(showDetType,gHandles.param.dataTypes{4}) || ...
                            strcmpi(showDetType,gHandles.param.dataTypes{5}) || ...
                            strcmpi(showDetType,gHandles.param.dataTypes{6}));
        
        if showTypeTest && ~showTypeDetTest
            errMsg = 'Valid Plot values must be only: HV1, HV2, HV3, HV4, HVAdd. ';
        elseif ~showTypeTest && showTypeDetTest
            errMsg = 'Valid Det values must be only: HV1, HV2, HV3, HV4, HVAdd, All. ';
        elseif showTypeTest && showTypeDetTest
            errMsg = 'Valid Plot Types must be values: HV1, HV2, HV3, HV4, HVAdd; Valid Plot Determinant must be values: HV1, HV2, HV3, HV4, HVAdd, All. ';
        end
        if     (chartType~=1 && chartType~=2 && chartType~=3)  && ~(isnan(contLayers) || isinf(contLayers) || contLayers<=0)
            errMsg = [errMsg 'Plot choices must be 1, 2 or 3 (Type: Surface, Mesh or Contour). ']; %#ok<AGROW>
        elseif ~(chartType~=1 && chartType~=2 && chartType~=3) && (isnan(contLayers) || isinf(contLayers) || contLayers<=0)
            errMsg = [errMsg 'Contour layers must be a valid, non-infinite integer above 0. ']; %#ok<AGROW>
        elseif (chartType~=1 && chartType~=2 && chartType~=3)  && (isnan(contLayers) || isinf(contLayers) || contLayers<=0)
            errMsg = [errMsg 'Plot choices must be 1, 2 or 3; Contour layers must be an integer. ']; %#ok<AGROW>
        end
        if     (isnan(showAzim) || isinf(showAzim)) && ~(isnan(showElev)|| isinf(showElev))
            errMsg = [errMsg 'Azimuth input value must be a numerically valid, non-infinite integer. ']; %#ok<AGROW>
        elseif ~(isnan(showAzim)|| isinf(showAzim)) && (isnan(showElev) || isinf(showElev))
            errMsg = [errMsg 'Elevation input value must be a numerically valid, non-infinite integer. ']; %#ok<AGROW>
        elseif (isnan(showAzim) || isinf(showAzim)) && (isnan(showElev) || isinf(showElev))
            errMsg = [errMsg 'Azimuth and Elevation input values must be a valid, non-infinite integer. ']; %#ok<AGROW>
        end
        if     (isnan(dataAzim) || isinf(dataAzim)) && ~(isnan(dataElev)|| isinf(dataElev))
            errMsg = [errMsg 'Azimuth data value must be a numerically valid, non-infinite integer. ']; %#ok<AGROW>
        elseif ~(isnan(dataAzim)|| isinf(dataAzim)) && (isnan(dataElev) || isinf(dataElev))
            errMsg = [errMsg 'Elevation data value must be a numerically valid, non-infinite integer. ']; %#ok<AGROW>
        elseif (isnan(dataAzim) || isinf(dataAzim)) && (isnan(dataElev) || isinf(dataElev))
            errMsg = [errMsg 'Azimuth and Elevation data values must be a valid, non-infinite integer. ']; %#ok<AGROW>
        end
        if (cholMod~=0 && cholMod~=1 && cholMod~=2) 
            errMsg = [errMsg 'Cholesky Modifier must be 0, 1 or 2 (Matrix, Lower Decomp, Permutation). ']; %#ok<AGROW>
        end
        if ~(showHess==0 || showHess==1)
            errMsg = [errMsg 'Show Hessian Plot Value must be either 0 to Disable, 1 to Enable. ']; %#ok<AGROW>
        end
        if ~(showHessShade==-2 ||showHessShade==-1 || (showHessShade>=0 && showHessShade<=gHandles.param.maxPercent))
            errMsg = [errMsg 'Show Hessian Shade: -2:Disable, -1:Enable, 0:Thresh (0:Max, 100:All). ']; %#ok<AGROW>
        end
        if ~(showHessGrade>=1) || isinf(showHessGrade) || isnan(showHessGrade)
            errMsg = [errMsg 'Show Hessian Grade must be valid and greater than or equal to one. ']; %#ok<AGROW>
        end
        if ~(runHessDiff==0 || runHessDiff==1)
            errMsg = [errMsg 'Hessian Value Difference Mode must be only: 0 to Disable or 1 to Enable. ']; %#ok<AGROW>
        end
        if ~(runHessNormal(1)==-3 || runHessNormal(1)==-2 || runHessNormal(1)==-1 || runHessNormal(1)==0 || runHessNormal(1)>=1) || isinf(runHessNormal(1))|| isnan(runHessNormal(1))
            errMsg = [errMsg 'Hessian Normalize: -3:None, -2 to 0:Log(e, 2 and 10), 1:Auto-Scale (Use comma for factor), >1:Divide by value.']; %#ok<AGROW>
        elseif runHessNormal(1)~=1 && size(runHessNormal,2) > 1
            errMsg = [errMsg 'Hessian Normalize Auto-Scale Factor can be used for Auto-Scale mode only. Use one input for all other methods.']; %#ok<AGROW>
        elseif runHessNormal(1)==1 && ~(size(runHessNormal,2) == 1 || size(runHessNormal,2) == 2)
            errMsg = [errMsg 'Hessian Normalize Auto-Scale Factor can be entered in conjuction with Auto-Scale mode only. Use 1 or 2 inputs.']; %#ok<AGROW>
        elseif runHessNormal(1)==1 && size(runHessNormal,2) == 1
            runHessNormal(2) = gHandles.param.runHessDvFact; % Use default factor value
            warndlg(['User Warning: Normalize Auto-Scale Factor is set to the default factor. ' ...
                     'Use comma (i.e. 1,10) to enter numeric factor other than default of '     ...
                     num2str(gHandles.param.runHessDvFact) '.'], 'User Warning', 'replace');
            uiwait(gcf); % Force to wait for confirmation
        elseif runHessNormal(1)==1 && (isinf(runHessNormal(2)) || isnan(runHessNormal(2)) || runHessNormal(2) == 0)
            errMsg = [errMsg 'Hessian Normalize Auto-Scale Factor must be valid and not equal zero. Use any ordinary numeral other than 0.']; %#ok<AGROW>
        end
        % Throw Error Message if failed verification
        if ~strcmp(errMsg,'');
            throw(MException('General:Input',errMsg));
        end
    catch err
        if ~strcmp(err.identifier,'MATLAB:badsubscript')
            errordlg(['Input Error: ' err.message], 'Input Error', 'replace');
        else
            warndlg('User Warning: Please try again and enter parameters by clicking OK.', 'User Warning', 'replace');
        end
        uiwait(gcf); % Force to wait for confirmation
        result = cell(1);
    end
end
% Return Settings & and Set Default Parameters
if     strcmpi(showType,gHandles.param.dataTypes{1}); showType = gHandles.param.dataTypes{1};
elseif strcmpi(showType,gHandles.param.dataTypes{2}); showType = gHandles.param.dataTypes{2};
elseif strcmpi(showType,gHandles.param.dataTypes{3}); showType = gHandles.param.dataTypes{3};
elseif strcmpi(showType,gHandles.param.dataTypes{4}); showType = gHandles.param.dataTypes{4};
elseif strcmpi(showType,gHandles.param.dataTypes{5}); showType = gHandles.param.dataTypes{5};
end
if     strcmpi(showDetType,gHandles.param.dataTypes{1}); showDetType = gHandles.param.dataTypes{1};
elseif strcmpi(showDetType,gHandles.param.dataTypes{2}); showDetType = gHandles.param.dataTypes{2};
elseif strcmpi(showDetType,gHandles.param.dataTypes{3}); showDetType = gHandles.param.dataTypes{3};
elseif strcmpi(showDetType,gHandles.param.dataTypes{4}); showDetType = gHandles.param.dataTypes{4};
elseif strcmpi(showDetType,gHandles.param.dataTypes{5}); showDetType = gHandles.param.dataTypes{5};
elseif strcmpi(showDetType,gHandles.param.dataTypes{6}); showDetType = gHandles.param.dataTypes{6};
end
gHandles.param.showType     = showType;
gHandles.param.showDetType  = showDetType;
gHandles.param.chartType    = chartType;
gHandles.param.showAzim     = showAzim;
gHandles.param.showElev     = showElev;
gHandles.param.dataAzim     = dataAzim;
gHandles.param.dataElev     = dataElev;
gHandles.param.contLayers   = contLayers;
gHandles.param.cholMod      = cholMod;
gHandles.param.showHess     = showHess;
gHandles.param.showHessShade= showHessShade;
gHandles.param.showHessGrade= showHessGrade;
gHandles.param.runHessNormal= runHessNormal;
gHandles.param.runHessDiff  = runHessDiff;

function saveIterWaitTimeLastState()
global gHandles
handles = gHandles.handles;
% Only if Current State of Inputbox is Active and in Single File or Random Mode state
if strcmp(get(handles.input_Iterations,'Enable'),'on') && get(handles.input_Iterations,'Value') == 1
    gHandles.param.buffIteration = str2double(get(handles.input_Iterations,'String'));
end
if strcmp(get(handles.input_WaitTime,'Enable'),'on') && get(handles.input_WaitTime,'Value') == 1
    gHandles.param.buffWaitTime = str2double(get(handles.input_WaitTime,'String'));
end

function verifyManipulationOrPathSet()
global gHandles
handles   = gHandles.handles;
dataMode  = get(handles.popup_DataMode,'Value');
singleFile= 1;
randomData= 3;
saveIterWaitTimeLastState(); % Persist Last State of Iteration & Wait-time
% If Custom Data Manipulation and Path Trajectory is set
if dataMode == singleFile || dataMode == randomData
    isManipSet = get(handles.cb_EnableCustomData, 'Value');
    isTrajSet  = get(handles.cb_EnablePathTraj,   'Value');
    if ~isManipSet && ~isTrajSet
        set(handles.input_Iterations,'Value',0);
        set(handles.input_Iterations,'Enable','off');
        set(handles.input_Iterations,'String','1');
        set(handles.input_WaitTime,  'Value',0);
        set(handles.input_WaitTime,  'Enable','off');
        set(handles.input_WaitTime,  'String','0');
    else
        set(handles.input_Iterations,'Value',1);
        set(handles.input_Iterations,'Enable','on');
        set(handles.input_Iterations,'String',num2str(gHandles.param.buffIteration));
        set(handles.input_WaitTime,  'Value',1);
        set(handles.input_WaitTime,  'Enable','on');
        set(handles.input_WaitTime,  'String',num2str(gHandles.param.buffWaitTime));
    end
    verifyDataMapMode();
end

function disableTrajectoryMode()
global gHandles
handles = gHandles.handles;
set(handles.cb_EnablePathTraj,         'Enable','off'); % Common
set(handles.cb_ShiftRadialLength,      'Enable','off');
set(handles.pb_TrajOpenFile,           'Enable','off');
set(handles.input_TrajFile,            'Enable','off');
set(handles.popup_TrajManipulationType,'Enable','off');
set(handles.txt_TrajManipulationType,  'Enable','off');
set(handles.input_TrajMoveRad,         'Enable','off');
set(handles.txt_TrajMoveRad,           'Enable','off');
set(handles.input_TrajRandVar,         'Enable','off'); % Rand
set(handles.input_TrajRandPop,         'Enable','off');
set(handles.txt_TrajRandVar,           'Enable','off');
set(handles.txt_TrajRandPop,           'Enable','off');
set(handles.input_TrajXShift,          'Enable','off'); % Shift
set(handles.input_TrajYShift,          'Enable','off');
set(handles.input_TrajZShift,          'Enable','off');
set(handles.txt_TrajXShift,            'Enable','off');
set(handles.txt_TrajYShift,            'Enable','off');
set(handles.txt_TrajZShift,            'Enable','off');
set(handles.input_TrajXRot,            'Enable','off'); % Rotate
set(handles.input_TrajYRot,            'Enable','off');
set(handles.input_TrajZRot,            'Enable','off');
set(handles.txt_TrajXRot,              'Enable','off');
set(handles.txt_TrajYRot,              'Enable','off');
set(handles.txt_TrajZRot,              'Enable','off');

function enableTrajectoryMode()
global gHandles
handles = gHandles.handles;
isVisible = get(handles.cb_EnablePathTraj,'Value');
disableTrajectoryMode(); % Reset to clear last state
verifyManipulationOrPathSet(); % Check to see if Data Manipulation or Path Trajectory set
set(handles.cb_EnablePathTraj,'Enable','on');
% If Manipulation Mode is Enabled for Input type
if isVisible
    set(handles.cb_EnablePathTraj,         'Enable','on'); % Common
    set(handles.cb_ShiftRadialLength,      'Enable','on');
    set(handles.pb_TrajOpenFile,           'Enable','on');
    set(handles.input_TrajFile,            'Enable','on');
    set(handles.popup_TrajManipulationType,'Enable','on');
    set(handles.txt_TrajManipulationType,  'Enable','on');
    set(handles.input_TrajMoveRad,         'Enable','on');
    set(handles.txt_TrajMoveRad,           'Enable','on');
    dataManMode    = get(handles.popup_TrajManipulationType,'Value');
    dataRadial     = 1;
    dataShift      = 2;
    dataRotate     = 3;
    dataRand       = 4;
    dataShifRot    = 5;
    dataShifRand   = 6;
    dataRotRand    = 7;
    dataShifRotRand= 8;
    switch dataManMode
        case dataRadial
            set(handles.txt_TrajMoveRad,'String','Min Radial');
            set(handles.cb_ShiftRadialLength,'Enable','off'); % Automatic by default
        case dataShift
            set(handles.txt_TrajMoveRad,  'String','Min Radius');
            set(handles.input_TrajXShift, 'Enable','on'); % Shift
            set(handles.input_TrajYShift, 'Enable','on');
            set(handles.input_TrajZShift, 'Enable','on');
            set(handles.txt_TrajXShift,   'Enable','on');
            set(handles.txt_TrajYShift,   'Enable','on');
            set(handles.txt_TrajZShift,   'Enable','on');
        case dataRotate
            set(handles.txt_TrajMoveRad,  'String','Min Radius');
            set(handles.input_TrajXRot,   'Enable','on'); % Rotate
            set(handles.input_TrajYRot,   'Enable','on');
            set(handles.input_TrajZRot,   'Enable','on');
            set(handles.txt_TrajXRot,     'Enable','on');
            set(handles.txt_TrajYRot,     'Enable','on');
            set(handles.txt_TrajZRot,     'Enable','on');
        case dataRand
            set(handles.txt_TrajMoveRad,  'String','Min Radius');
            set(handles.input_TrajRandVar,'Enable','on'); % Rand
            set(handles.input_TrajRandPop,'Enable','on');
            set(handles.txt_TrajRandVar,  'Enable','on');
            set(handles.txt_TrajRandPop,  'Enable','on');
        case dataShifRot
            set(handles.txt_TrajMoveRad,  'String','Min Radius');
            set(handles.input_TrajXShift, 'Enable','on'); % Shift
            set(handles.input_TrajYShift, 'Enable','on');
            set(handles.input_TrajZShift, 'Enable','on');
            set(handles.txt_TrajXShift,   'Enable','on');
            set(handles.txt_TrajYShift,   'Enable','on');
            set(handles.txt_TrajZShift,   'Enable','on');
            set(handles.input_TrajXRot,   'Enable','on'); % Rotate
            set(handles.input_TrajYRot,   'Enable','on');
            set(handles.input_TrajZRot,   'Enable','on');
            set(handles.txt_TrajXRot,     'Enable','on');
            set(handles.txt_TrajYRot,     'Enable','on');
            set(handles.txt_TrajZRot,     'Enable','on');
        case dataShifRand
            set(handles.txt_TrajMoveRad,  'String','Min Radius');
            set(handles.input_TrajXShift, 'Enable','on'); % Shift
            set(handles.input_TrajYShift, 'Enable','on');
            set(handles.input_TrajZShift, 'Enable','on');
            set(handles.txt_TrajXShift,   'Enable','on');
            set(handles.txt_TrajYShift,   'Enable','on');
            set(handles.txt_TrajZShift,   'Enable','on');
            set(handles.input_TrajRandVar,'Enable','on'); % Rand
            set(handles.input_TrajRandPop,'Enable','on');
            set(handles.txt_TrajRandVar,  'Enable','on');
            set(handles.txt_TrajRandPop,  'Enable','on');
        case dataRotRand
            set(handles.txt_TrajMoveRad,  'String','Min Radius');
            set(handles.input_TrajXRot,   'Enable','on'); % Rotate
            set(handles.input_TrajYRot,   'Enable','on');
            set(handles.input_TrajZRot,   'Enable','on');
            set(handles.txt_TrajXRot,     'Enable','on');
            set(handles.txt_TrajYRot,     'Enable','on');
            set(handles.txt_TrajZRot,     'Enable','on');
            set(handles.input_TrajRandVar,'Enable','on'); % Rand
            set(handles.input_TrajRandPop,'Enable','on');
            set(handles.txt_TrajRandVar,  'Enable','on');
            set(handles.txt_TrajRandPop,  'Enable','on');
        case dataShifRotRand
            set(handles.txt_TrajMoveRad,  'String','Min Radius');
            set(handles.input_TrajXShift, 'Enable','on'); % Shift
            set(handles.input_TrajYShift, 'Enable','on');
            set(handles.input_TrajZShift, 'Enable','on');
            set(handles.txt_TrajXShift,   'Enable','on');
            set(handles.txt_TrajYShift,   'Enable','on');
            set(handles.txt_TrajZShift,   'Enable','on');
            set(handles.input_TrajXRot,   'Enable','on'); % Rotate
            set(handles.input_TrajYRot,   'Enable','on');
            set(handles.input_TrajZRot,   'Enable','on');
            set(handles.txt_TrajXRot,     'Enable','on');
            set(handles.txt_TrajYRot,     'Enable','on');
            set(handles.txt_TrajZRot,     'Enable','on');
            set(handles.input_TrajRandVar,'Enable','on'); % Rand
            set(handles.input_TrajRandPop,'Enable','on');
            set(handles.txt_TrajRandVar,  'Enable','on');
            set(handles.txt_TrajRandPop,  'Enable','on');
    end
end

function disableManipulationMode()
global gHandles
handles = gHandles.handles;
set(handles.cb_EnableCustomData,   'Enable','off'); % Common
set(handles.popup_ManipulationMode,'Enable','off');
set(handles.popup_IncreaseMode,    'Enable','off');
set(handles.txt_Manipulation,      'Enable','off');
set(handles.input_Interval,        'Enable','off');
set(handles.txt_Interval,          'Enable','off');
set(handles.txt_Xshift,            'Enable','off'); % Shift
set(handles.txt_Yshift,            'Enable','off');
set(handles.txt_Zshift,            'Enable','off');
set(handles.input_Xshift,          'Enable','off');
set(handles.input_Yshift,          'Enable','off');
set(handles.input_Zshift,          'Enable','off');
set(handles.txt_Xrot,              'Enable','off'); % Rotate
set(handles.txt_Yrot,              'Enable','off');
set(handles.txt_Zrot,              'Enable','off');
set(handles.input_Xrot,            'Enable','off');
set(handles.input_Yrot,            'Enable','off');
set(handles.input_Zrot,            'Enable','off');
set(handles.txt_SplitInterval,     'Enable','off'); % Split
set(handles.input_SplitInterval,   'Enable','off');
set(handles.txt_RandVar,           'Enable','off'); % Random
set(handles.txt_RandPop,           'Enable','off');
set(handles.input_RandVar,         'Enable','off');
set(handles.input_RandPop,         'Enable','off');

function enableManipulationMode(dlgVisible)
global gHandles
handles = gHandles.handles;
isVisible = get(handles.cb_EnableCustomData,'Value');
disableManipulationMode(); % Reset to clear last state
verifyManipulationOrPathSet(); % Check to see if Data Manipulation or Path Trajectory set
set(handles.cb_EnableCustomData,'Enable','on');
% Check if external dialog boxes should appear (Default: True)
if nargin == 0
    dlgVisible = 1;
end
% If Manipulation Mode is Enabled for Input type
if isVisible
    set(handles.cb_EnableCustomData,   'Enable','on'); % Common
    set(handles.popup_ManipulationMode,'Enable','on');
    set(handles.popup_IncreaseMode,    'Enable','on');
    set(handles.txt_Manipulation,      'Enable','on');
    set(handles.input_Interval,        'Enable','on');
    set(handles.txt_Interval,          'Enable','on');
    dataManMode      = get(handles.popup_ManipulationMode,'Value');
    dataShift        = 1;
    dataRotate       = 2;
    dataSplit        = 3;
    dataRand         = 4;
    dataRotShift     = 5;
    dataRotSplit     = 6;
    dataRandShift    = 7;
    dataRandRot      = 8;
    dataRandSplit    = 9;
    dataRandRotShift = 10;
    dataRandRotSplit = 11;
    dataRegionExpand = 12;
    dataRadialExpand = 13;
    switch dataManMode
        case dataShift
            set(handles.txt_Xshift,          'Enable','on'); % Shift
            set(handles.txt_Yshift,          'Enable','on');
            set(handles.txt_Zshift,          'Enable','on');
            set(handles.input_Xshift,        'Enable','on');
            set(handles.input_Yshift,        'Enable','on');
            set(handles.input_Zshift,        'Enable','on');
        case dataRotate
            set(handles.txt_Xrot,            'Enable','on'); % Rotate
            set(handles.txt_Yrot,            'Enable','on');
            set(handles.txt_Zrot,            'Enable','on');
            set(handles.input_Xrot,          'Enable','on');
            set(handles.input_Yrot,          'Enable','on');
            set(handles.input_Zrot,          'Enable','on');
        case dataSplit
            set(handles.txt_Xshift,          'Enable','on'); % Shift
            set(handles.txt_Yshift,          'Enable','on');
            set(handles.txt_Zshift,          'Enable','on');
            set(handles.input_Xshift,        'Enable','on');
            set(handles.input_Yshift,        'Enable','on');
            set(handles.input_Zshift,        'Enable','on');
            set(handles.txt_SplitInterval,   'Enable','on'); % Split
            set(handles.input_SplitInterval, 'Enable','on');
        case dataRand
            set(handles.txt_RandVar,         'Enable','on'); % Random
            set(handles.txt_RandPop,         'Enable','on');
            set(handles.input_RandVar,       'Enable','on');
            set(handles.input_RandPop,       'Enable','on');
        case dataRotShift
            set(handles.txt_Xshift,          'Enable','on'); % Shift
            set(handles.txt_Yshift,          'Enable','on');
            set(handles.txt_Zshift,          'Enable','on');
            set(handles.input_Xshift,        'Enable','on');
            set(handles.input_Yshift,        'Enable','on');
            set(handles.input_Zshift,        'Enable','on');
            set(handles.txt_Xrot,            'Enable','on'); % Rotate
            set(handles.txt_Yrot,            'Enable','on');
            set(handles.txt_Zrot,            'Enable','on');
            set(handles.input_Xrot,          'Enable','on');
            set(handles.input_Yrot,          'Enable','on');
            set(handles.input_Zrot,          'Enable','on');
        case dataRotSplit
            set(handles.txt_Xshift,          'Enable','on'); % Shift
            set(handles.txt_Yshift,          'Enable','on');
            set(handles.txt_Zshift,          'Enable','on');
            set(handles.input_Xshift,        'Enable','on');
            set(handles.input_Yshift,        'Enable','on');
            set(handles.input_Zshift,        'Enable','on');
            set(handles.txt_Xrot,            'Enable','on'); % Rotate
            set(handles.txt_Yrot,            'Enable','on');
            set(handles.txt_Zrot,            'Enable','on');
            set(handles.input_Xrot,          'Enable','on');
            set(handles.input_Yrot,          'Enable','on');
            set(handles.input_Zrot,          'Enable','on');
            set(handles.txt_SplitInterval,   'Enable','on'); % Split
            set(handles.input_SplitInterval, 'Enable','on');
        case dataRandShift
            set(handles.txt_Xshift,          'Enable','on'); % Shift
            set(handles.txt_Yshift,          'Enable','on');
            set(handles.txt_Zshift,          'Enable','on');
            set(handles.input_Xshift,        'Enable','on');
            set(handles.input_Yshift,        'Enable','on');
            set(handles.input_Zshift,        'Enable','on');
            set(handles.txt_RandVar,         'Enable','on'); % Random
            set(handles.txt_RandPop,         'Enable','on');
            set(handles.input_RandVar,       'Enable','on');
            set(handles.input_RandPop,       'Enable','on');
        case dataRandRot
            set(handles.txt_Xrot,            'Enable','on'); % Rotate
            set(handles.txt_Yrot,            'Enable','on');
            set(handles.txt_Zrot,            'Enable','on');
            set(handles.input_Xrot,          'Enable','on');
            set(handles.input_Yrot,          'Enable','on');
            set(handles.input_Zrot,          'Enable','on');
            set(handles.txt_RandVar,         'Enable','on'); % Random
            set(handles.txt_RandPop,         'Enable','on');
            set(handles.input_RandVar,       'Enable','on');
            set(handles.input_RandPop,       'Enable','on');
        case dataRandSplit
            set(handles.txt_Xshift,          'Enable','on'); % Shift
            set(handles.txt_Yshift,          'Enable','on');
            set(handles.txt_Zshift,          'Enable','on');
            set(handles.input_Xshift,        'Enable','on');
            set(handles.input_Yshift,        'Enable','on');
            set(handles.input_Zshift,        'Enable','on');
            set(handles.txt_SplitInterval,   'Enable','on'); % Split
            set(handles.input_SplitInterval, 'Enable','on');
            set(handles.txt_RandVar,         'Enable','on'); % Random
            set(handles.txt_RandPop,         'Enable','on');
            set(handles.input_RandVar,       'Enable','on');
            set(handles.input_RandPop,       'Enable','on');
        case dataRandRotShift
            set(handles.txt_Xshift,          'Enable','on'); % Shift
            set(handles.txt_Yshift,          'Enable','on');
            set(handles.txt_Zshift,          'Enable','on');
            set(handles.input_Xshift,        'Enable','on');
            set(handles.input_Yshift,        'Enable','on');
            set(handles.input_Zshift,        'Enable','on');
            set(handles.txt_Xrot,            'Enable','on'); % Rotate
            set(handles.txt_Yrot,            'Enable','on');
            set(handles.txt_Zrot,            'Enable','on');
            set(handles.input_Xrot,          'Enable','on');
            set(handles.input_Yrot,          'Enable','on');
            set(handles.input_Zrot,          'Enable','on');
            set(handles.txt_RandVar,         'Enable','on'); % Random
            set(handles.txt_RandPop,         'Enable','on');
            set(handles.input_RandVar,       'Enable','on');
            set(handles.input_RandPop,       'Enable','on');
        case dataRandRotSplit
            set(handles.txt_Xshift,          'Enable','on'); % Shift
            set(handles.txt_Yshift,          'Enable','on');
            set(handles.txt_Zshift,          'Enable','on');
            set(handles.input_Xshift,        'Enable','on');
            set(handles.input_Yshift,        'Enable','on');
            set(handles.input_Zshift,        'Enable','on');
            set(handles.txt_Xrot,            'Enable','on'); % Rotate
            set(handles.txt_Yrot,            'Enable','on');
            set(handles.txt_Zrot,            'Enable','on');
            set(handles.input_Xrot,          'Enable','on');
            set(handles.input_Yrot,          'Enable','on');
            set(handles.input_Zrot,          'Enable','on');
            set(handles.txt_SplitInterval,   'Enable','on'); % Split
            set(handles.input_SplitInterval, 'Enable','on');
            set(handles.txt_RandVar,         'Enable','on'); % Random
            set(handles.txt_RandPop,         'Enable','on');
            set(handles.input_RandVar,       'Enable','on');
            set(handles.input_RandPop,       'Enable','on');
        case dataRegionExpand
            set(handles.input_Interval,      'Enable','off');% Custom
            set(handles.txt_Interval,        'Enable','off');
            if dlgVisible
                regionExpansionParams();
            end
        case dataRadialExpand
            set(handles.input_Interval,      'Enable','off');% Custom
            set(handles.txt_Interval,        'Enable','off');
            if dlgVisible
                radialExpansionParams();
            end
    end
end

%%% ALGORITHMIC SUPPORT FUNCTIONS %%%
function outputDataStem(dataOutput)
% Equivalent to disp(dataOutput) for edit_OutputData outputbox
global gHandles
handleObj = gHandles.handles.edit_OutputData;
existOutput = get(handleObj,'String');
if strcmp(existOutput,'')
    set(handleObj,'String',dataOutput);
elseif ~iscell(existOutput)
    newOutput = {existOutput dataOutput};
    set(handleObj,'String',newOutput); 
else
    existOutput{size(existOutput,1)+1} = dataOutput;
    set(handleObj,'String',existOutput);    
end

function clearTextField()
global gHandles
set(gHandles.handles.edit_OutputData,'String','');

function scrollTextField(handleObj)
% Scrollbox Caret Function
% http://undocumentedmatlab.com/blog/setting-line-position-in-edit-box-uicontrol/
% http://www.mathworks.com/matlabcentral/fileexchange/14317
try %#ok<ALIGN>
    jhEdit= FindJObj(handleObj);
    jEdit = jhEdit.getComponent(0).getComponent(0);
    jEdit.setCaretPosition(jEdit.getDocument.getLength);
catch err; end

function [inputData, fileOpen] = loadInitData()
global gHandles
% Test for a File
fileOpen = 0;
if (iscell(gHandles.proc.inputPaths)) % For multi-file mode, take first file
    filepath = sprintf('%c',strcat(gHandles.proc.inputPaths{1}, gHandles.proc.inputPaths{2}));
else
    filepath = gHandles.proc.inputPaths;
end
[inputData, errMsg] = loadData(filepath);
if strcmp(errMsg, gHandles.param.functNoError)
    fileOpen = 1;
else
    warndlg('Notice: Input verification pending as alternate data source is used or file not specified.','System Notice');
    uiwait(gcf); % Force to wait for confirmation
end

function [inputData, errMsg] = loadData(filepath, handleMsg, dataType)
global gHandles
errMsg = gHandles.param.functNoError;
try
    fileNameElem = numel(filepath);
    fileNameStop = max(regexp(filepath,'[.]'));
    fileExt      = filepath(fileNameStop+1:fileNameElem);
    switch fileExt
        case 'xls'
            [inputData, ~, ~] = xlsread(filepath);
        case 'csv'
            inputData = csvread(filepath);
        otherwise
            throw(MException('General:IOError',' file path is invalid or cannot be recognised.'));
    end
catch err
    errMsg   = err.message;
    inputData= -1;
    if nargin == 3 % Show Error Dialog only if requested
        if handleMsg
            if strcmp(dataType,gHandles.param.dataTypeInput)
                resetPaths();
            elseif strcmp(dataType,gHandles.param.dataTypeTraj)
                resetTrajPaths();
            end
            errordlg(['Error Occurred: ' dataType errMsg],'Load I/O Error');
            uiwait(gcf); % Force to wait for confirmation
        end
    end
end

function [uMat, cholMat] = matrixFactorisation(mat)
% Sparse matrix Operations: http://www.mathworks.com.au/help/techdoc/math/f6-8856.html
% Postive Definite Matrix Test: https://ece.uwaterloo.ca/~ece204/howtos/positive.html
% Lower-Upper Decomposition
global gHandles
try
    [lMat,uMat,pMat] = lu(mat);
    % If CholMod is selected, take lower decomposition matrix for cholesky factorisation
    if     gHandles.param.cholMod == 1 % Lower decomposition matrix
        mat = lMat * lMat';
    elseif gHandles.param.cholMod == 2 % Permutation matrix
        mat = pMat * pMat';
    end
catch err % Error in LU Factorisation
    uMat = NaN;
end
% Cholesky Factorization
try
    cholMat = chol(mat);
catch err % Fails Test for Positive Definiteness
    cholMat = NaN;
end

function [detMat, detMatInv] = detCalculation(mat)
matInv    = 1./mat;                     % Same as mat.\1; Do not use inv(mat) function.
detMat    = det(cleanupMatrix(mat));    % Clean-up matrix if it contains inf/NaN values
detMatInv = det(cleanupMatrix(matInv)); % Clean-up matrix if it contains inf/NaN values

function [matEigen] = eigenCalculation(mat)
warning('off', 'MATLAB:eigs:TooManyRequestedEigsForRealSym');
warning('off', 'MATLAB:eigs:TooManyRequestedEigsForComplexNonsym');
try      matEigen = eigs(mat); % Determine highest eigenvalues
catch e; matEigen = 0; end

function [hvEigenData] = eigenValueToString(hvEigen)
hvEigenData = '';
for i=1:size(hvEigen,1)
    if i==1; hvEigenData = num2str(hvEigen(i));
    else     hvEigenData = [hvEigenData '; ' num2str(hvEigen(i))]; %#ok<AGROW>
    end
end

function matrix = getHessianMatrixType(showType)
global gHandles
if     strcmpi(showType, gHandles.param.dataTypes{1}); matrix = gHandles.proc.hessResult{1};
elseif strcmpi(showType, gHandles.param.dataTypes{2}); matrix = gHandles.proc.hessResult{2};
elseif strcmpi(showType, gHandles.param.dataTypes{3}); matrix = gHandles.proc.hessResult{3};
elseif strcmpi(showType, gHandles.param.dataTypes{4}); matrix = gHandles.proc.hessResult{4};
elseif strcmpi(showType, gHandles.param.dataTypes{5}); matrix = gHandles.proc.hessResult{5};
end

function exportDataStem(procTime, inputData)
global gHandles
handles     = gHandles.handles;
dataExpMode = get(handles.popup_ExportData,'Value');
dataExpNone = 1;
dataExpXLS  = 2;
dataExpCSV  = 3;
dataMapMode = get(handles.popup_DataMap,'Value');
singMode    = 1;
diffMode    = 2;
diffHess    = gHandles.param.runHessDiff;
% Add to file name for Data Map Mode
if diffHess
    % Difference Mode for Hessians
    dMapStr = [' (' gHandles.param.dataExpDiff   ')'];
else
    % Difference Mode for Data Source
    switch dataMapMode
        case singMode; dMapStr = [' (' gHandles.param.dataExpSingle ')'];
        case diffMode; dMapStr = [' (' gHandles.param.dataExpDiff   ')'];
    end
end
% End export process if no export mode selected
if dataExpMode == dataExpNone 
    return
end
% Pre-operations: Matrix eigenvalues
hvStats(:,1)= gHandles.param.dataTypesStat;
hvTypes(:,1)= gHandles.param.dataTypes(1:5); % Include HV1-HVAdd Only
for i=1:size(hvTypes,1)
    [detMat, detMatInv] = detCalculation(getHessianMatrixType(hvTypes{i}));
    [matEigen]          = eigenCalculation(getHessianMatrixType(hvTypes{i}));
    [eigenStr]          = eigenValueToString(matEigen);
    hvStats(:,i+1)      = {hvTypes{i}, num2str(detMat), num2str(detMatInv), eigenStr};
end
% Pre-operations: Matrix decomposition
mat          = getHessianMatrixType(gHandles.param.showType);
[uMat, cMat] = matrixFactorisation(mat);
switch dataExpMode
    case dataExpXLS
        try
            excelWriter(procTime, inputData, hvStats, uMat, cMat, dMapStr);
        catch err
            outputDataStem('   Can''t export as Excel, Saving CSV');
            csvWriter(procTime, inputData, hvStats, uMat, cMat, dMapStr);
        end
    case dataExpCSV
        csvWriter(procTime, inputData, hvStats, uMat, cMat, dMapStr);
end

function excelWriter(procTime, inputData, hvStats, uMat, cMat, dMapStr)
global gHandles
warning('off', 'MATLAB:xlswrite:AddSheet'); % Warning Suppression of msg
xlswrite([pwd '\export\' procTime ' Data HV.xls'], inputData, 'Source');
xlswrite([pwd '\export\' procTime ' Data HV.xls'], getHessianMatrixType(gHandles.param.dataTypes{1}),[gHandles.param.dataTypes{1} dMapStr]);
xlswrite([pwd '\export\' procTime ' Data HV.xls'], getHessianMatrixType(gHandles.param.dataTypes{2}),[gHandles.param.dataTypes{2} dMapStr]);
xlswrite([pwd '\export\' procTime ' Data HV.xls'], getHessianMatrixType(gHandles.param.dataTypes{3}),[gHandles.param.dataTypes{3} dMapStr]);
xlswrite([pwd '\export\' procTime ' Data HV.xls'], getHessianMatrixType(gHandles.param.dataTypes{4}),[gHandles.param.dataTypes{4} dMapStr]);
xlswrite([pwd '\export\' procTime ' Data HV.xls'], getHessianMatrixType(gHandles.param.dataTypes{5}),[gHandles.param.dataTypes{5} dMapStr]);
xlswrite([pwd '\export\' procTime ' Data HV.xls'], hvStats,   'HV-Statistics');
xlswrite([pwd '\export\' procTime ' Data HV.xls'], uMat,      'Decomposition');
if ~isnan(cMat)
    xlswrite([pwd '\export\' procTime ' Data HV.xls'], cMat, 'Cholesky');
end
if gHandles.fig.fig1_1_initialize
    evalData(:,1) = gHandles.fData.fig1_1.hessIters;
    evalData(:,2) = gHandles.fData.fig1_1.hessCount;
    evalData(:,3) = gHandles.fData.fig1_1.hessPercent;
    xlswrite([pwd '\export\' procTime ' Data HV.xls'], evalData, 'HV-Evaluation');
end

function csvWriter(procTime, inputData, hvStats, uMat, cMat, dMapStr)
global gHandles
csvwrite([pwd '\export\' procTime ' Data Source.csv' ],inputData);
csvwrite([pwd '\export\' procTime ' Data ' gHandles.param.dataTypes{1} dMapStr '.csv'],getHessianMatrixType(gHandles.param.dataTypes{1}));
csvwrite([pwd '\export\' procTime ' Data ' gHandles.param.dataTypes{2} dMapStr '.csv'],getHessianMatrixType(gHandles.param.dataTypes{2}));
csvwrite([pwd '\export\' procTime ' Data ' gHandles.param.dataTypes{3} dMapStr '.csv'],getHessianMatrixType(gHandles.param.dataTypes{3}));
csvwrite([pwd '\export\' procTime ' Data ' gHandles.param.dataTypes{4} dMapStr '.csv'],getHessianMatrixType(gHandles.param.dataTypes{4}));
csvwrite([pwd '\export\' procTime ' Data ' gHandles.param.dataTypes{5} dMapStr '.csv'],getHessianMatrixType(gHandles.param.dataTypes{5}));
csvcells([pwd '\export\' procTime ' Data HV-Statistics.csv'],hvStats); % For cell CSV writing
csvwrite([pwd '\export\' procTime ' Data Decomposition.csv'],uMat);
if ~isnan(cMat)
    csvwrite([pwd '\export\' procTime ' Data Cholesky.csv'],cMat);
end
if gHandles.fig.fig1_1_initialize
    evalData(:,1) = gHandles.fData.fig1_1.hessIters;
    evalData(:,2) = gHandles.fData.fig1_1.hessCount;
    evalData(:,3) = gHandles.fData.fig1_1.hessPercent;
    csvwrite([pwd '\export\' procTime ' Data HV-Evaluation.csv'], evalData);
end

function csvcells(path, dataInput)
fid = fopen(path,'w');
for i = 1:size(dataInput,1)
   for j = 1:size(dataInput,2)
      fprintf(fid,'%s',dataInput{i,j});
      if(j~=size(dataInput,2))
         fprintf(fid,',',dataInput{i,j}); %#ok<CTPCT>
      else
         fprintf(fid,'\n');
      end
   end
end
fclose(fid);

function exportFigure(printFigure, procTime, figName)
% Screen Capture (and external source links):
% http://www.mathworks.com.au/help/techdoc/ref/imwrite.html
% http://www.mathworks.com/matlabcentral/fileexchange/24323
global gHandles
handles    = gHandles.handles;
imgFormatId= get(handles.popup_ExportImage,'Value');
figExpNone = 1;
figExpPNG  = 2;
figExpJPG  = 3;
switch imgFormatId
    case figExpNone
        return
    case {figExpPNG, figExpJPG}
        % Capture Figure to Print
        set(printFigure,'InvertHardcopy','Off');
        set(printFigure,'PaperPositionMode','Auto');
        renderType = lower(['-' get(printFigure,'Renderer')]);
        % Save Figure to File
        imgFormats   = {'-',  '-dpng', '-djpeg'};
        imgExtension = {'-', '.png',  '.jpg'};
        print(printFigure, imgFormats{imgFormatId}, renderType, '-r96', [pwd '\export\' procTime ' Image ' figName imgExtension{imgFormatId}]);
end

function initVideo(figNames, procTime)
% Video Capture:
% http://www.mathworks.com.au/help/techdoc/ref/getframe.html
% http://www.mathworks.com.au/help/techdoc/ref/movie.html
% http://www.mathworks.com.au/help/techdoc/ref/videowriterclass.html
% Create the animation/video objects
global gHandles
figRecNone   = 1;
recordModeId = get(gHandles.handles.popup_ExportVideo,'Value');
if recordModeId ~= figRecNone
    gHandles.param.figWriterObj = cell(size(figNames,1),1); % Allocate memory
    videoFormats                = {'-', 'Motion JPEG AVI', 'Uncompressed AVI'};
    for i = 1:size(figNames,1)
        gHandles.param.figWriterObj{i} = VideoWriter([pwd '\export\' procTime ' Video ' figNames{i} '.avi'], videoFormats{recordModeId}); %#ok<*TNMLP>
        gHandles.param.figWriterObj{i}.FrameRate = gHandles.param.frameRate;
    end
end

function saveVideo(figure, figObjId)
% Write the animation/video frame
global gHandles
figRecNone = 1;
if get(gHandles.handles.popup_ExportVideo,'Value') ~= figRecNone
    open(gHandles.param.figWriterObj{figObjId});
    frame = getframe(figure);
    writeVideo(gHandles.param.figWriterObj{figObjId}, frame);
end

function exportVideo()
% Close the animation/video objects
global gHandles
figRecNone = 1;
if get(gHandles.handles.popup_ExportVideo,'Value') ~= figRecNone
    for i = 1:size(gHandles.param.figWriterObj,1)
        close(gHandles.param.figWriterObj{i});
    end
    gHandles.param.figWriterObj = cell(1,1); % Deallocate memory
end

function [inputData, errMsg] = getInputData(inputData, i)
global gHandles
handles    = gHandles.handles;
dataType   = gHandles.param.dataTypeInput;
fileWarn   = 1;
dataMode   = get(handles.popup_DataMode,'Value');
singleFile = 1;
multiFile  = 2;
randomData = 3;
psoData    = 4;
switch dataMode
    case singleFile
        % Single-File Mode
        if i == 1
            % Run 1st iteration using input data
            filepath = gHandles.proc.inputPaths;
            [inputData, errMsg] = loadData(filepath, fileWarn, dataType);
        else
            % Subsequent data as what is modified
            errMsg   = gHandles.param.functNoError;
        end
    case multiFile
        % Multi-File Mode
        if (iscell(gHandles.proc.inputPaths))
            % Run multiple input files
            filepath = sprintf('%c',strcat(gHandles.proc.inputPaths{1}, gHandles.proc.inputPaths{i+1}));
            [inputData, errMsg] = loadData(filepath, fileWarn, dataType);
        else
            % Run single input file only
            filepath = gHandles.proc.inputPaths;
            [inputData, errMsg] = loadData(filepath, fileWarn, dataType);
        end
    case randomData
        % Random Data Input Mode
        errMsg    = gHandles.param.functNoError;
        inputData = generateRandomData(inputData, i);
    case psoData
        % Particle-Swarm Algorithm Mode
        errMsg    = gHandles.param.functNoError;
        inputData = performPSOAlgorithm(i);
end

function [inputData] = performPSOAlgorithm(iteration)
% Particle-Swarm Algorithm Integration (Using function handles to access PSO.m)
global gHandles
iterations   = gHandles.PSO.iterations;
inertia      = gHandles.PSO.inertia;
correctFactor= gHandles.PSO.correctFactor;
bestValue    = gHandles.PSO.bestValue;
xGoal        = gHandles.PSO.xGoal;
yGoal        = gHandles.PSO.yGoal;
zGoal        = gHandles.PSO.zGoal;
swarmSize    = gHandles.PSO.swarmSize;
percentAdjust= gHandles.PSO.percentAdjust;
evalFunction = gHandles.PSO.evalFunction;
swarmData    = gHandles.PSO.swarmData;
if iteration == 1
    % Run 1st iteration using input data
    filepath = gHandles.proc.inputPaths;
    [inputData, errMsg]= loadData(filepath);
    if strcmp(errMsg, gHandles.param.functNoError)
        % As Input file is entered, we use this as source
        [swarmData, axisLims] = gHandles.PSO.PSO_ManualInput(inputData, bestValue);
    else
        % As Input file is not entered, we use generated data as source
        [swarmData, axisLims] = gHandles.PSO.PSO_Generate(swarmSize, bestValue);
    end
    gHandles.PSO.axisLims = gHandles.PSO.PSO_AdjustLims(axisLims, xGoal, yGoal, zGoal);
else
    % Perform PSO algorithm
    swarmData = gHandles.PSO.PSO_Algorithm(swarmData, inertia, correctFactor, xGoal, yGoal, zGoal, percentAdjust, evalFunction);
end
% Persist Data for future PSO iterations
gHandles.PSO.swarmData = swarmData;
inputData = [swarmData(:,1,1) swarmData(:,1,2) swarmData(:,1,3)];

function [inputData] = generateRandomData(inputData, iteration)
% Generate Random Data Population (using Mersenne Twister Method)
global gHandles
if iteration == 1
    % Run 1st iteration to generate data only
    randElems = gHandles.param.randElements;
    minBound  = gHandles.param.randUpBound;
    maxBound  = gHandles.param.randLowBound;
    inputData = minBound + (maxBound-minBound).*rand(randElems,3);
end

function inputData = manipulateData(inputData, iteration, maxIterations)
% Run Data Transformations/Manipulations
global gHandles
handles  = gHandles.handles;
isEnabled= get(handles.cb_EnableCustomData,'Value');
% Determine rate of increase/decrease (excludes random function)
increaseMode = get(handles.popup_IncreaseMode,'Value');
fractIncrem  = 1;
fractDecrem  = 2;
exponIncrem  = 3;
exponDecrem  = 4;
switch increaseMode
    case fractIncrem; iterationVal = 1;
    case fractDecrem; iterationVal = -1;
    case exponIncrem; iterationVal = iteration;
    case exponDecrem; iterationVal = -iteration;
end
if isEnabled
    % If Enable Data Transformations is selected, run routine, else skip routine
    dataManMode      = get(handles.popup_ManipulationMode,'Value');
    dataShift        = 1;
    dataRotate       = 2;
    dataSplit        = 3;
    dataRand         = 4;
    dataRotShift     = 5;
    dataRotSplit     = 6;
    dataRandShift    = 7;
    dataRandRot      = 8;
    dataRandSplit    = 9;
    dataRandRotShift = 10;
    dataRandRotSplit = 11;
    dataRegionExpand = 12;
    dataRadialExpand = 13;
    switch dataManMode
        case dataShift
            inputData = shiftDataInput (inputData, iterationVal, maxIterations);
        case dataRotate
            inputData = rotDataInput   (inputData, iterationVal, maxIterations);
        case dataSplit
            inputData = splitDataInput (inputData, iterationVal, maxIterations);
        case dataRand
            inputData = randDataInput  (inputData);
        case dataRotShift
            inputData = rotDataInput   (inputData, iterationVal, maxIterations);
            inputData = shiftDataInput (inputData, iterationVal, maxIterations);
        case dataRotSplit
            inputData = rotDataInput   (inputData, iterationVal, maxIterations);
            inputData = splitDataInput (inputData, iterationVal, maxIterations);
        case dataRandShift
            inputData = randDataInput  (inputData);
            inputData = shiftDataInput (inputData, iterationVal, maxIterations);
        case dataRandRot
            inputData = randDataInput  (inputData);
            inputData = rotDataInput   (inputData, iterationVal, maxIterations);
        case dataRandSplit
            inputData = randDataInput  (inputData);
            inputData = splitDataInput (inputData, iterationVal, maxIterations);
        case dataRandRotShift
            inputData = randDataInput  (inputData);
            inputData = rotDataInput   (inputData, iterationVal, maxIterations);
            inputData = shiftDataInput (inputData, iterationVal, maxIterations);
        case dataRandRotSplit
            inputData = randDataInput  (inputData);
            inputData = rotDataInput   (inputData, iterationVal, maxIterations);
            inputData = splitDataInput (inputData, iterationVal, maxIterations);
        case dataRegionExpand
            inputData = regionExpansion(inputData, iterationVal, maxIterations, iteration);
        case dataRadialExpand
            inputData = radialExpansion(inputData, iterationVal, maxIterations, iteration);
    end
end

function errMsg = getTrajData(iteration)
global gHandles
if get(gHandles.handles.cb_EnablePathTraj,'Value') && iteration == 1
    % Obtain Path Trajectory Data if first iteration run
    fileWarn = 1;
    filepath = gHandles.proc.trajPath;
    dataType = gHandles.param.dataTypeTraj;
    [trajData, errMsg] = loadData(filepath, fileWarn, dataType);
    gHandles.param.buffTrajData = trajData;
else
    % Otherwise no Path Trajectory Data is obtained
    errMsg = gHandles.param.functNoError;
end

function inputData = trajectoryEvent(inputData, iteration)
% Run Path Trajectory Data Event
global gHandles
handles  = gHandles.handles;
trajData = gHandles.param.buffTrajData; % From getTrajectoryData function
isEnabled= get(handles.cb_EnablePathTraj,'Value');
withinLim= size(trajData,1) >= iteration;
if isEnabled && withinLim
    % If Enable Trajectory Data is selected & iteration within min trajectory data size, else skip routine
    for i=1:size(inputData,1)
        currTrajectory = trajData(iteration,:);
        currInputPoint = inputData(i,:);
        gHandles.param.buffTrajIndex = iteration; % For Charting
        if isInsideRadius(currTrajectory, currInputPoint)
            dataTrajMode   = get(handles.popup_TrajManipulationType,'Value');
            isRadialShift  = get(handles.cb_ShiftRadialLength,'Value');
            dataRadial     = 1;
            dataShift      = 2;
            dataRotate     = 3;
            dataRand       = 4;
            dataShifRot    = 5;
            dataShifRand   = 6;
            dataRotRand    = 7;
            dataShifRotRand= 8;
            % If Radial Shift is Set and Except Move Radial Mode (as dataRadial runs Radial Shift)
            if isRadialShift && dataTrajMode ~= dataRadial
                currInputPoint = shiftRadial(currInputPoint, currTrajectory);
            end
            switch dataTrajMode
                case dataRadial
                    currInputPoint = shiftRadial  (currInputPoint, currTrajectory);
                case dataShift
                    currInputPoint = shiftDataTraj(currInputPoint);
                case dataRotate
                    currInputPoint = rotDataTraj  (currInputPoint);
                case dataRand
                    currInputPoint = randDataTraj (currInputPoint);
                case dataShifRot
                    currInputPoint = shiftDataTraj(currInputPoint);
                    currInputPoint = rotDataTraj  (currInputPoint);
                case dataShifRand
                    currInputPoint = shiftDataTraj(currInputPoint);
                    currInputPoint = randDataTraj (currInputPoint);
                case dataRotRand
                    currInputPoint = rotDataTraj  (currInputPoint);
                    currInputPoint = randDataTraj (currInputPoint);
                case dataShifRotRand
                    currInputPoint = shiftDataTraj(currInputPoint);
                    currInputPoint = rotDataTraj  (currInputPoint);
                    currInputPoint = randDataTraj (currInputPoint);
            end
            inputData(i,:) = currInputPoint; % Update changed input point into inputData
        end
    end
end

function preRunStemFunction()
global gHandles
clearOutput();
preProcessParamInput();
handles      = gHandles.handles;
dataMapMode  = get(handles.popup_DataMap,'Value');
maxIteration = str2double(get(handles.input_Iterations,'String'));
maxIteration = setMaxIteration(dataMapMode, maxIteration);
waitTime     = str2double(get(handles.input_WaitTime,'String'));
singMode     = 1;
diffMode     = 2;
inputData    = zeros(1,1);
for i=1:maxIteration
    switch dataMapMode
        case singMode
            [inputData, errInput] = getInputData(inputData, i);
            errTraj = getTrajData(i);
            if isDataInvalid(errInput, errTraj); return; end             % Input or Trajectory Data Invalid
            inputData = manipulateData(inputData, i, maxIteration);      % 1. Manipulate Data
            inputData = trajectoryEvent(inputData, i);                   % 2. Add Trajectory Event
            runStemFunction(inputData, i, maxIteration);                 % Run Heuristics
        case diffMode
            diff = cell(1,2); k = 1;
            for j=i:i+1
                [inputData, errInput] = getInputData(inputData, j);
                errTraj = getTrajData(j);
                if isDataInvalid(errInput, errTraj); return; end         % Input or Trajectory Data Invalid
                inputData = manipulateData(inputData, j, maxIteration+1);% 1. Manipulate Data
                inputData = trajectoryEvent(inputData, j);               % 2. Add Trajectory Event
                diff{k}   = inputData; k = k + 1;
            end
            runStemFunction(diff{2}-diff{1}, i, maxIteration, diff{2});  % Run Heuristics, Difference of Matrices
    end
    updateWaitbar(i/maxIteration);
    if gHandles.proc.stopProcess == 1; return; end % If Stop Button Triggered
    pause(waitTime);
end

function result = isDataInvalid(errInput, errTraj)
global gHandles
result = ~strcmp(errInput, gHandles.param.functNoError) || ...
         ~strcmp(errTraj,  gHandles.param.functNoError);

function maxIteration = setMaxIteration(dataMapMode, maxIteration)
diffMode = 2;
if dataMapMode == diffMode
    verifyDataMapMode(); % Verify Max Iteration is valid
    if maxIteration == 1
        maxIteration = 0;
    else
        maxIteration = maxIteration - 1;
    end
end

%%% ALGORITHMIC FUNCTIONS: Radial Shift Data %%%
function insideRadius = isInsideRadius(arrayPointA, arrayPointB)
global gHandles
handles      = gHandles.handles;
minRadius    = str2double(get(handles.input_TrajMoveRad,'String'));
insideRadius = radiusValue(arrayPointA, arrayPointB) <= minRadius;

function radius = radiusValue(arrayPointA, arrayPointB)
diffX  = arrayPointA(1,1) - arrayPointB(1,1);
diffY  = arrayPointA(1,2) - arrayPointB(1,2);
diffZ  = arrayPointA(1,3) - arrayPointB(1,3);
radius = sqrt((diffX)^2 + (diffY)^2 + (diffZ)^2);

function [inputData] = shiftRadial(inputData, trajData)
% http://en.wikipedia.org/wiki/Spherical_coordinates
global gHandles
handles  = gHandles.handles;
radius   = str2double(get(handles.input_TrajMoveRad,'String'));
inputPt  = inputData - trajData;
zeroPoint= [0 0 0];
if isequal(inputPt, zeroPoint)
    % If the difference between inputData and trajData is cancelled out,
    % thus it makes inputData the centre-point of the virtual sphere.
    % Hence, no determination of theta or phi can be obtained.
    theta    = gHandles.param.trajDefRadian; % Use defaults specified in globals
    phi      = gHandles.param.trajDefRadian;
else
    xVal     = inputPt(1,1);
    yVal     = inputPt(1,2);
    zVal     = inputPt(1,3);
    theta    = acos(zVal / radius); % returns value in radians
    phi      = atan(yVal / xVal);   % returns value in radians
end
xValNew  = (radius * cos(phi) * sin(theta)) + trajData(1,1);
yValNew  = (radius * sin(phi) * sin(theta)) + trajData(1,2);
zValNew  = (radius * cos(theta))            + trajData(1,3);
inputData= [xValNew yValNew zValNew];

%%% ALGORITHMIC FUNCTIONS: Randomise Data %%%
function [inputData] = randDataTraj(inputData)
% Expressed as Percentage to be converted to decimal
global gHandles
handles        = gHandles.handles;
randVariance   = str2double(get(handles.input_TrajRandVar,'String'))/gHandles.param.maxPercent;
randPopulation = str2double(get(handles.input_TrajRandPop,'String'))/gHandles.param.maxPercent;
interval       = 1;
inputData      = randomiseData(inputData, randVariance, randPopulation, interval);

function [inputData] = randDataInput(inputData)
% Expressed as Percentage to be converted to decimal
global gHandles
handles        = gHandles.handles;
randVariance   = str2double(get(handles.input_RandVar,'String'))/gHandles.param.maxPercent;
randPopulation = str2double(get(handles.input_RandPop,'String'))/gHandles.param.maxPercent;
interval       = str2double(get(handles.input_Interval,'String'));
inputData      = randomiseData(inputData, randVariance, randPopulation, interval);

function [dataPoints] = randomiseData(dataPoints, randVariance, randPopulation, interval)
minRange       = -1;
maxRange       = 1;
maxElements    = size(dataPoints,1);
maxDimensions  = size(dataPoints,2);
maxPopModified = ceil(randPopulation*maxElements);
if maxPopModified > maxElements
    maxPopModified = maxElements; % Check against logical size overflow
end
for i=1:maxPopModified
    if mod(i,interval) == 0
        randomIdSelected = ceil(rand(1,1)*maxElements);
        if randomIdSelected>maxElements
            randomIdSelected = maxElements; % Check against illogical ID generated
        end
        randValues = (minRange + (maxRange-minRange).*rand(1,maxDimensions)).*randVariance;
        dataPoints(randomIdSelected,:) = dataPoints(randomIdSelected,:) + (dataPoints(randomIdSelected,:)*randValues(:));
    end
end

%%% ALGORITHMIC FUNCTIONS: Shift Data %%%
function [inputData] = shiftDataTraj(inputData)
global gHandles
handles = gHandles.handles;
intervalOnly  = 1;
verifyDataSize(inputData, intervalOnly); % Verify Data Size against Interval
splitInterval = 1;
interval      = 1;
xshift        = str2double(get(handles.input_TrajXShift,'String'));
yshift        = str2double(get(handles.input_TrajYShift,'String'));
zshift        = str2double(get(handles.input_TrajZShift,'String'));
inputData     = splitData(inputData, splitInterval, interval, xshift, yshift, zshift);

function [inputData] = shiftDataInput(inputData, currIteration, maxIterations)
global gHandles
handles = gHandles.handles;
intervalOnly  = 1;
verifyDataSize(inputData, intervalOnly); % Verify Data Size against Interval
splitInterval = size(inputData,1);
interval      = str2double(get(handles.input_Interval,'String'));
xshift        = str2double(get(handles.input_Xshift,'String'))*(currIteration/maxIterations);
yshift        = str2double(get(handles.input_Yshift,'String'))*(currIteration/maxIterations);
zshift        = str2double(get(handles.input_Zshift,'String'))*(currIteration/maxIterations);
inputData     = splitData(inputData, splitInterval, interval, xshift, yshift, zshift);

%%% ALGORITHMIC FUNCTIONS: Split Data %%%
function [inputData] = splitDataInput(inputData, currIteration, maxIterations)
global gHandles
handles = gHandles.handles;
verifyDataSize(inputData); % Verify Data Size and Obtain Split Iteration Value
splitInterval = str2double(get(handles.input_SplitInterval,'String'));
interval      = str2double(get(handles.input_Interval,'String'));
xshift        = str2double(get(handles.input_Xshift,'String'))*(currIteration/maxIterations);
yshift        = str2double(get(handles.input_Yshift,'String'))*(currIteration/maxIterations);
zshift        = str2double(get(handles.input_Zshift,'String'))*(currIteration/maxIterations);
inputData     = splitData(inputData, splitInterval, interval, xshift, yshift, zshift);

function [dataPoints] = splitData(dataPoints, splitInterval, interval, xshift, yshift, zshift)
for i=1:splitInterval
    if mod(i,interval) == 0
        dataPoints(i,1) = dataPoints(i,1) + xshift;
        dataPoints(i,2) = dataPoints(i,2) + yshift;
        dataPoints(i,3) = dataPoints(i,3) + zshift;
    end
end

%%% ALGORITHMIC FUNCTIONS: Rotate Data %%%
function [inputData] = rotDataTraj(inputData)
global gHandles
handles  = gHandles.handles;
interval = 1;
xRad     = degtorad(str2double(get(handles.input_TrajXRot,'String')));
yRad     = degtorad(str2double(get(handles.input_TrajYRot,'String')));
zRad     = degtorad(str2double(get(handles.input_TrajZRot,'String')));
inputData= rotateData(inputData, interval, xRad, yRad, zRad);

function [inputData] = rotDataInput(inputData, currIteration, maxIterations)
global gHandles
handles  = gHandles.handles;
interval = str2double(get(handles.input_Interval,'String'));
xRad     = degtorad(str2double(get(handles.input_Xrot,'String'))*(currIteration/maxIterations));
yRad     = degtorad(str2double(get(handles.input_Yrot,'String'))*(currIteration/maxIterations));
zRad     = degtorad(str2double(get(handles.input_Zrot,'String'))*(currIteration/maxIterations));
inputData= rotateData(inputData, interval, xRad, yRad, zRad);

function [dataPoints] = rotateData(dataPoints, interval, xRad, yRad, zRad)
% http://answers.yahoo.com/question/index?qid=20070824104734AAzLO3Q
% http://www.mathworks.com/matlabcentral/fileexchange/23417-compute-3d-rotation-matrix
xRotMat  = Rotation3DMat(xRad, [1 0 0]);
yRotMat  = Rotation3DMat(yRad, [0 1 0]);
zRotMat  = Rotation3DMat(zRad, [1 0 1]);
if interval == 1
    dataPoints = dataPoints*xRotMat;
    dataPoints = dataPoints*yRotMat;
    dataPoints = dataPoints*zRotMat;
else
    rotatData = genRotateIntervData(dataPoints, interval);
    rotatData = rotatData*xRotMat;
    rotatData = rotatData*yRotMat;
    rotatData = rotatData*zRotMat;
    dataPoints= dataPoints+rotatData;
end

function [intervalData] = genRotateIntervData(inputData, interval)
length       = size(inputData,1);
dims         = size(inputData,2);
intervalData = zeros(length,dims);
for i=1:size(inputData,1)
    if mod(i,interval)
        intervalData(i,:) = inputData(i,:);
    end
end

%%% ALGORITHMIC FUNCTIONS: Expansion of Data %%%
function [inputData] = regionExpansion(inputData, currIteration, maxIterations, realIteration)
global gHandles
% First Verify Region Expansion Settings
verifyExpTrajSize(gHandles.param.regionRandSel, gHandles.param.regionManual, inputData, gHandles.param.regionExpType);
regionExpValX = gHandles.param.regionExpValX;
regionExpValY = gHandles.param.regionExpValY;
regionExpValZ = gHandles.param.regionExpValZ;
regionExpMode = gHandles.param.regionExpMode;
regionRandSel = gHandles.param.regionRandSel;
regionManual  = gHandles.param.regionManual;
maxElements   = size(inputData,1);
iterRate      = realIteration/maxIterations;
% Determine shift is in single or multi-input mode
if size(regionExpValX,2) == 1
    xshift = regionExpValX *(currIteration/maxIterations);
    yshift = regionExpValY *(currIteration/maxIterations);
    zshift = regionExpValZ *(currIteration/maxIterations);
else
    if realIteration <= size(regionExpValX,2) % Shift according to manual entry
        xshift = regionExpValX(realIteration) *(currIteration/maxIterations);
        yshift = regionExpValY(realIteration) *(currIteration/maxIterations);
        zshift = regionExpValZ(realIteration) *(currIteration/maxIterations);
    else
        return % Multi-input exceeds dims, leave routine
    end
end
if regionRandSel > 0
    % Run Region Random Selection if enabled
    for i=1:regionRandSel
        id = ceil(rand(1,1)*maxElements);
        if id > maxElements
            id = maxElements; % Check against illogical ID generated
        end
        inputData = expansionMode(inputData, regionExpMode, iterRate, id, xshift, yshift, zshift);
    end
else
    % Run Manual ID Selection if enabled
    for i=1:size(regionManual,2)
        id = regionManual(i);
        inputData = expansionMode(inputData, regionExpMode, iterRate, id, xshift, yshift, zshift);
    end
end

function [inputData] = radialExpansion(inputData, currIteration, maxIterations, realIteration)
global gHandles
% First Verify Radial Expansion Settings
verifyExpTrajSize(gHandles.param.radialRandSel, gHandles.param.radialManual, inputData, gHandles.param.radialExpType);
radius       = gHandles.param.radialExpVal;
theta        = gHandles.param.radialTheta;
phi          = gHandles.param.radialPhi;
radialExpMode= gHandles.param.radialExpMode;
radialRandSel= gHandles.param.radialRandSel;
radialManual = gHandles.param.radialManual;
maxElements  = size(inputData,1);
iterRate     = realIteration/maxIterations;
% Determine shift is in single or multi-input mode
if size(radius,2) == 1
    xshift = ((radius*(currIteration/maxIterations)) * cos(phi) * sin(theta));
    yshift = ((radius*(currIteration/maxIterations)) * sin(phi) * sin(theta));
    zshift = ((radius*(currIteration/maxIterations)) * cos(theta));
else
    if realIteration <= size(radius,2) % Shift according to manual entry
        xshift = ((radius(realIteration)*(currIteration/maxIterations)) * cos(phi(realIteration)) * sin(theta(realIteration)));
        yshift = ((radius(realIteration)*(currIteration/maxIterations)) * sin(phi(realIteration)) * sin(theta(realIteration)));
        zshift = ((radius(realIteration)*(currIteration/maxIterations)) * cos(theta(realIteration)));
    else
        return % Multi-input exceeds dims, leave routine
    end
end
if radialRandSel > 0
    % Run Region Random Selection if enabled
    for i=1:radialRandSel
        id = ceil(rand(1,1)*maxElements);
        if id > maxElements
            id = maxElements; % Check against illogical ID generated
        end
        inputData = expansionMode(inputData, radialExpMode, iterRate, id, xshift, yshift, zshift);
    end
else
    % Run Manual ID Selection if enabled
    for i=1:size(radialManual,2)
        id = radialManual(i);
        inputData = expansionMode(inputData, radialExpMode, iterRate, id, xshift, yshift, zshift);
    end
end

function [inputData] = expansionMode(inputData, regionExpMode, iterRate, id, xshift, yshift, zshift)
forward    = 1;
forwardRev = 2;
fwdFullRev = 3;
fullCycle  = 4;
switch regionExpMode
    case forward
        inputData(id,1) = inputData(id,1) + xshift;
        inputData(id,2) = inputData(id,2) + yshift;
        inputData(id,3) = inputData(id,3) + zshift;
    case forwardRev
        if iterRate <= 0.5
            inputData(id,1) = inputData(id,1) + (xshift*2);
            inputData(id,2) = inputData(id,2) + (yshift*2);
            inputData(id,3) = inputData(id,3) + (zshift*2);
        else
            inputData(id,1) = inputData(id,1) - (xshift*2);
            inputData(id,2) = inputData(id,2) - (yshift*2);
            inputData(id,3) = inputData(id,3) - (zshift*2);
        end
    case fwdFullRev
        if iterRate <= 0.33333
            inputData(id,1) = inputData(id,1) + (xshift*3);
            inputData(id,2) = inputData(id,2) + (yshift*3);
            inputData(id,3) = inputData(id,3) + (zshift*3);
        else
            inputData(id,1) = inputData(id,1) - (xshift*3);
            inputData(id,2) = inputData(id,2) - (yshift*3);
            inputData(id,3) = inputData(id,3) - (zshift*3);
        end
    case fullCycle
        if iterRate <= 0.25 || iterRate >= 0.75
            inputData(id,1) = inputData(id,1) + (xshift*4);
            inputData(id,2) = inputData(id,2) + (yshift*4);
            inputData(id,3) = inputData(id,3) + (zshift*4);
        elseif iterRate < 0.75
            inputData(id,1) = inputData(id,1) - (xshift*4);
            inputData(id,2) = inputData(id,2) - (yshift*4);
            inputData(id,3) = inputData(id,3) - (zshift*4);
        end
end

function [mat] = cleanupMatrix(mat)
for i=1:size(mat,1)
    for j=1:size(mat,2)
        if isinf(mat(i,j))
            mat(i,j) = 0;
        elseif isnan(mat(i,j))
            mat(i,j) = 0;
        end
    end
end

%%% ALGORITHMIC FUNCTIONS: STeM Functionality %%%
function runStemFunction(inputData, i, maxIterations, sourceData)
global gHandles
handles  = gHandles.handles;
procTime = datestr(now, gHandles.param.dateFmt);
isStart  = i == 1;
isFinish = i == maxIterations;
if strcmpi(gHandles.param.showType, gHandles.param.dataTypes{5}); gHandles.param.strBuff = '   '; % For HVAdd Only
else                                                              gHandles.param.strBuff = '';
end
if isStart
    initVideo(gHandles.param.figNames, procTime);
    outputDataStem('________________________________');
    outputDataStem(['STeM Start: ' procTime]);
end
if ~(isStart && isFinish) && ~isStart
    outputDataStem('   --------------------------------------------------');
end
outputDataStem(['   Iteration Run/s:  ' gHandles.param.strBuff num2str(i)]);
set(handles.pb_RunAlgorithm,'String',['Iteration No:' num2str(i)]);

epsilon = str2double(get(handles.input_Epsilon, 'String'));
k_r     = str2double(get(handles.input_Kr,      'String'));
k_theta = str2double(get(handles.input_Ktheta,  'String'));
k_phi1  = str2double(get(handles.input_Kphi1,   'String'));
k_phi3  = str2double(get(handles.input_Kphi3,   'String'));
l_term  = str2double(get(handles.input_LongTerm,'String'));
[hv1, hv2, hv3, hv4] = STeM(inputData, epsilon, k_r, k_theta, k_phi1, k_phi3, l_term);
hv1 = cleanupMatrix(hv1);
hv2 = cleanupMatrix(hv2);
hv3 = cleanupMatrix(hv3);
hv4 = cleanupMatrix(hv4);

% Determine mode of analysis (No Modification or Difference of Hessians)
hessDiff = gHandles.param.runHessDiff;
if hessDiff
    if isStart && isFinish
        exportVideo();
        gHandles.proc.stopProcess = 1;
        outputDataStem('   Difference Error: Iteration must be>1');
        errordlg('Heuristic Error: To make a difference of Hessian values as a setting, there must be more than one iteration to process. Verify and try again.','Heuristic Error');
        uiwait(gcf);
        return
    elseif isStart
        gHandles.param.runHessBuff = {hv1, hv2, hv3, hv4};
        outputDataStem('   Note: Running in Hessian Diff Mode');
        return
    elseif isFinish
        hv1 = hv1 - gHandles.param.runHessBuff{1};
        hv2 = hv2 - gHandles.param.runHessBuff{2};
        hv3 = hv3 - gHandles.param.runHessBuff{3};
        hv4 = hv4 - gHandles.param.runHessBuff{4};
        gHandles.param.runHessBuff = {};
    else
        hv1 = hv1 - gHandles.param.runHessBuff{1};
        hv2 = hv2 - gHandles.param.runHessBuff{2};
        hv3 = hv3 - gHandles.param.runHessBuff{3};
        hv4 = hv4 - gHandles.param.runHessBuff{4};
        gHandles.param.runHessBuff = {hv1, hv2, hv3, hv4};
    end
end

hvAdd = hv1 + hv2 + hv3 + hv4;
gHandles.proc.hessResult = {hv1, hv2, hv3, hv4, hvAdd}; % Vital for Function: getHessianMatrixType
[detHv, detHvInv]        = detCalculation  (getHessianMatrixType(gHandles.param.showType));
[hvEigen]                = eigenCalculation(getHessianMatrixType(gHandles.param.showType));
outputDataStem(['   Det Hess ' gHandles.param.showType ':  '  num2str(detHv,'%1.4e')]); % Unnecessary to use strcat
outputDataStem(['   Det Hess Invs:  '  gHandles.param.strBuff num2str(detHvInv,'%1.4e')]);
outputDataStem(['   Largest Eigenv:  ' gHandles.param.strBuff num2str(hvEigen(1),'%1.4e')]);

if nargin == 3
    % Source Data is Input Data
    plotDataStem  (procTime, inputData, i);
    exportDataStem(procTime, inputData);
elseif nargin == 4
    % Use Source Data for Export/Plot
    plotDataStem  (procTime, sourceData, i);
    exportDataStem(procTime, sourceData);
end
if isFinish || gHandles.proc.stopProcess
    exportVideo();
    endTime = datestr(now, gHandles.param.dateFmt);
    outputDataStem(['STeM End: ' endTime]);
end

function plotDataStem(procTime, inputData, i)
% Show Figures & Tables
chartTables();
plotFigure1(procTime, i, inputData);
plotFigure2(procTime, i);
plotFigure3(procTime);
plotFigure4(procTime);
plotFigure5(procTime);
plotFigure6(procTime);

function chartTables()
% Show Tabular Data
global gHandles
handles = gHandles.handles;
if get(handles.cb_ShowAlgResults,'Value')
    set(handles.uitable_StemOutput1,'Data',getHessianMatrixType(gHandles.param.dataTypes{1}));
    set(handles.uitable_StemOutput2,'Data',getHessianMatrixType(gHandles.param.dataTypes{2}));
    set(handles.uitable_StemOutput3,'Data',getHessianMatrixType(gHandles.param.dataTypes{3}));
    set(handles.uitable_StemOutput4,'Data',getHessianMatrixType(gHandles.param.dataTypes{4}));
else
    set(handles.uitable_StemOutput1,'ColumnWidth',{383});
    set(handles.uitable_StemOutput2,'ColumnWidth',{383});
    set(handles.uitable_StemOutput3,'ColumnWidth',{383});
    set(handles.uitable_StemOutput4,'ColumnWidth',{383});
    set(handles.uitable_StemOutput1,'Data',{'HV1 Table Disabled - Set Checkbox ''Alg Result'' to Enable Feature'});
    set(handles.uitable_StemOutput2,'Data',{'HV2 Table Disabled - Set Checkbox ''Alg Result'' to Enable Feature'});
    set(handles.uitable_StemOutput3,'Data',{'HV3 Table Disabled - Set Checkbox ''Alg Result'' to Enable Feature'});
    set(handles.uitable_StemOutput4,'Data',{'HV4 Table Disabled - Set Checkbox ''Alg Result'' to Enable Feature'});
end

function plotFigure1(procTime, i, inputData)
global gHandles
% Figure 1 Output
set(0,'CurrentFigure',gHandles.fig.fig1);
if get(gHandles.handles.cb_ShowFig1,'Value')
    % UI Configuration
    gHandles.fData.fig1_1.hessIter = i;
    gHandles.fData.fig1_1.hessTime = procTime;
    colPoint     = get(gHandles.handles.popup_PointColour,'Value');
    colHess      = get(gHandles.handles.popup_HessColour,'Value');
    colTraj      = get(gHandles.handles.popup_TrajColour,'Value');
    showHess     = gHandles.param.showHess;
    showHessShade= gHandles.param.showHessShade;
    showHessGrade= gHandles.param.showHessGrade;
    showHessStr  = ['Show Hess '  gHandles.param.showType];
    gHandles.fData.fig1.plotData= inputData;
    gHandles.fData.fig1.cb_plot = uicontrol(gcf,'Style','checkbox', 'Value',0,       'Position',[0  0  115 15],'BackgroundColor',[0.8 0.8 0.8],'String','Show Line Plot');
    gHandles.fData.fig1.cb_scat = uicontrol(gcf,'Style','checkbox', 'Value',1,       'Position',[0  15 115 15],'BackgroundColor',[0.8 0.8 0.8],'String','Show Scatter Plot');
    gHandles.fData.fig1.cb_hess = uicontrol(gcf,'Style','checkbox', 'Value',showHess,'Position',[0  30 115 15],'BackgroundColor',[0.8 0.8 0.8],'String',showHessStr);
    gHandles.fData.fig1.pp_colr = uicontrol(gcf,'Style','popupmenu','Value',colPoint,'Position',[30 45  85 20],'BackgroundColor',[0.8 0.8 0.8],'String',{'Blue-BlueGray','Turquoise-Cyan','Red-Pink','Green-Lime','Orange-Yellow','Purple-Violet','Magenta-Lt.Magenta','Brown-Oak','Dk.Gray-Lt.Gray','Black-White'});
    gHandles.fData.fig1.tx_colr = uicontrol(gcf,'Style','text',     'Value',0,       'Position',[0  42  30 17],'BackgroundColor',[0.8 0.8 0.8],'String','MkCol');
    gHandles.fData.fig1.pp_hesc = uicontrol(gcf,'Style','popupmenu','Value',colHess, 'Position',[62 65  53 20],'BackgroundColor',[0.8 0.8 0.8],'String',{'Blue-BlueGray','Turquoise-Cyan','Red-Pink','Green-Lime','Orange-Yellow','Purple-Violet','Magenta-Lt.Magenta','Brown-Oak','Dk.Gray-Lt.Gray','Black-White'});
    gHandles.fData.fig1.tx_hesc = uicontrol(gcf,'Style','text',     'Value',0,       'Position',[0  62  30 17],'BackgroundColor',[0.8 0.8 0.8],'String','HvCol');
    gHandles.fData.fig1.in_hess = uicontrol(gcf,'Style','edit',     'Value',0,       'Position',[30 63  16 22],'BackgroundColor',[0.8 0.8 0.8],'String',num2str(showHessShade));
    gHandles.fData.fig1.in_hesg = uicontrol(gcf,'Style','edit',     'Value',0,       'Position',[46 63  16 22],'BackgroundColor',[0.8 0.8 0.8],'String',num2str(showHessGrade));
    gHandles.fData.fig1.tx_hess = uicontrol(gcf,'Style','text',     'Value',0,       'Position',[31 78  14  6],'BackgroundColor',[0.8 0.8 0.8],'String','Sha.','ForegroundColor',[0.4 0.4 0.4],'FontSize',5);
    gHandles.fData.fig1.tx_hesg = uicontrol(gcf,'Style','text',     'Value',0,       'Position',[47 78  14  6],'BackgroundColor',[0.8 0.8 0.8],'String','Gra.','ForegroundColor',[0.4 0.4 0.4],'FontSize',5);
    gHandles.fData.fig1.cb_pltj = 0;
    gHandles.fData.fig1.pp_cltj = 0;
    set(gHandles.fData.fig1.cb_plot,'Callback',@fig1CallbackStem);
    set(gHandles.fData.fig1.cb_scat,'Callback',@fig1CallbackStem);
    set(gHandles.fData.fig1.pp_colr,'Callback',@fig1CallbackStem);
    set(gHandles.fData.fig1.cb_hess,'Callback',@fig1CallbackStem);
    set(gHandles.fData.fig1.pp_hesc,'Callback',@fig1CallbackStem);
    set(gHandles.fData.fig1.in_hess,'Callback',@fig1CallbackStem);
    set(gHandles.fData.fig1.in_hesg,'Callback',@fig1CallbackStem);
    % UI for Trajectory Data
    if get(gHandles.handles.cb_EnablePathTraj,'Value')
        gHandles.fData.fig1.cb_pltj = uicontrol(gcf,'Style','checkbox', 'Value',1,      'Position',[0  45  115 15],'BackgroundColor',[0.8 0.8 0.8],'String','Show Trajectory');
        gHandles.fData.fig1.pp_cltj = uicontrol(gcf,'Style','popupmenu','Value',colTraj,'Position',[30 105  85 20],'BackgroundColor',[0.8 0.8 0.8],'String',{'Blue-BlueGray','Turquoise-Cyan','Red-Pink','Green-Lime','Orange-Yellow','Purple-Violet','Magenta-Lt.Magenta','Brown-Oak','Dk.Gray-Lt.Gray','Black-White'});
        gHandles.fData.fig1.tx_cltj = uicontrol(gcf,'Style','text',     'Value',0,      'Position',[0  102  30 17],'BackgroundColor',[0.8 0.8 0.8],'String','Tj.Col');
        set(gHandles.fData.fig1.cb_pltj,'Callback',@fig1CallbackStem);
        set(gHandles.fData.fig1.pp_cltj,'Callback',@fig1CallbackStem);
        set(gHandles.fData.fig1.pp_colr,'Position',[30 65 85 20]);
        set(gHandles.fData.fig1.tx_colr,'Position',[0  62 30 17]);
        set(gHandles.fData.fig1.pp_hesc,'Position',[62 85 53 20]);
        set(gHandles.fData.fig1.tx_hesc,'Position',[0  82 30 17]);
        set(gHandles.fData.fig1.in_hess,'Position',[30 83 16 22]);
        set(gHandles.fData.fig1.in_hesg,'Position',[46 83 16 22]);
        set(gHandles.fData.fig1.tx_hess,'Position',[31 98 14  6]);
        set(gHandles.fData.fig1.tx_hesg,'Position',[47 98 14  6]);
    end
    % Plot Data
    fig1CallbackStem(gHandles.fData.fig1.cb_scat,[],[]); % Subfunction for plotting
    title('Data Plot Representation (Source)');
    set(gHandles.fig.fig1,'Name','Fig #1: Data Plot (Source)');
    % Save Figure & Video
    exportFigure(gHandles.fig.fig1, procTime, gHandles.param.figNames{1});
    saveVideo(gHandles.fig.fig1, 1);
else
    clf(gHandles.fig.fig1);
    set(gHandles.fig.fig1,'Name','Fig #1 Window (Auto-Close)');
    title('Fig #1 Disabled - Set Checkbox to Enable');
    set(gca,'Color',[0.8 0.8 0.8]);
    set(gca,'XColor',[0.8 0.8 0.8]);
    set(gca,'YColor',[0.8 0.8 0.8]);
end

function fig1CallbackStem(hObject, ~, ~)
global gHandles
set(0,'CurrentFigure',gHandles.fig.fig1);
% Input Data and Colour Data
inputData  = gHandles.fData.fig1.plotData;
mColorLine = [[0 0 0.8];    [0 0.6 0.9];[1 0 0];    [0 0.6 0];[1 0.4 0];[0.6 0.3 0.6];[1 0 1];  [0.5 0 0.1];  [0.3 0.3 0.3];[0 0 0]];
mColorFill = [[0.4 0.6 0.7];[0 1 1];    [1 0.7 0.8];[0 1 0];  [1 1 0];  [0.8 0.7 0.9];[1 0.5 1];[0.7 0.5 0.3];[0.7 0.7 0.7];[1 1 1]];
% Remove previous handle for Scatter and Line plot if exists
try %#ok<ALIGN>
    set   (gHandles.fData.fig1.plotScat,'Visible','off');
    delete(gHandles.fData.fig1.plotScat);
    set   (gHandles.fData.fig1.plotMark,'Visible','off');
    delete(gHandles.fData.fig1.plotMark);
catch err; end
try %#ok<ALIGN>
    if get(gHandles.handles.cb_EnablePathTraj,'Value')
        set   (gHandles.fData.fig1.trajMark,'Visible','off');
        delete(gHandles.fData.fig1.trajMark);
        set   (gHandles.fData.fig1.trajScat,'Visible','off');
        delete(gHandles.fData.fig1.trajScat);
    end
catch err; end
% Logical test to ensure plot points or scatter display
showPlotPoint = (hObject == gHandles.fData.fig1.cb_plot) || ...
                (hObject == gHandles.fData.fig1.pp_colr && get(gHandles.fData.fig1.cb_plot,'Value')) || ...
                (hObject == gHandles.fData.fig1.cb_pltj && get(gHandles.fData.fig1.cb_plot,'Value')) || ...
                (hObject == gHandles.fData.fig1.pp_cltj && get(gHandles.fData.fig1.cb_plot,'Value')) || ...
                (hObject == gHandles.fData.fig1.cb_hess && get(gHandles.fData.fig1.cb_plot,'Value')) || ...
                (hObject == gHandles.fData.fig1.pp_hesc && get(gHandles.fData.fig1.cb_plot,'Value')) || ...
                (hObject == gHandles.fData.fig1.in_hess && get(gHandles.fData.fig1.cb_plot,'Value')) || ...
                (hObject == gHandles.fData.fig1.in_hesg && get(gHandles.fData.fig1.cb_plot,'Value'));
showScatter   = (hObject == gHandles.fData.fig1.cb_scat) || ...
                (hObject == gHandles.fData.fig1.pp_colr && get(gHandles.fData.fig1.cb_scat,'Value')) || ...
                (hObject == gHandles.fData.fig1.cb_pltj && get(gHandles.fData.fig1.cb_scat,'Value')) || ...
                (hObject == gHandles.fData.fig1.pp_cltj && get(gHandles.fData.fig1.cb_scat,'Value')) || ...
                (hObject == gHandles.fData.fig1.cb_hess && get(gHandles.fData.fig1.cb_scat,'Value')) || ...
                (hObject == gHandles.fData.fig1.pp_hesc && get(gHandles.fData.fig1.cb_scat,'Value')) || ...
                (hObject == gHandles.fData.fig1.in_hess && get(gHandles.fData.fig1.cb_scat,'Value')) || ...
                (hObject == gHandles.fData.fig1.in_hesg && get(gHandles.fData.fig1.cb_scat,'Value'));
% Plot Trajectory Map
if get(gHandles.handles.cb_EnablePathTraj,'Value')
    if get(gHandles.fData.fig1.cb_pltj,'Value')
        tjColorId= get(gHandles.fData.fig1.pp_cltj, 'Value');
        trajIndex= gHandles.param.buffTrajIndex;
        trajData = gHandles.param.buffTrajData(1:trajIndex,:);
        trajPoint= gHandles.param.buffTrajData(trajIndex,:);
        hold on
        gHandles.fData.fig1.trajScat = scatter3(trajPoint(:,1), trajPoint(:,2), trajPoint(:,3), 'o', 'LineWidth', 1, 'MarkerEdgeColor', mColorLine(tjColorId,:), 'MarkerFaceColor', mColorFill(tjColorId,:));
        gHandles.fData.fig1.trajMark = plot3(trajData(:,1), trajData(:,2), trajData(:,3), '.-', 'LineWidth', 1, 'Color', mColorLine(tjColorId,:));
        grid on
        hold off
    end
end
% Plot Points or Scatter Chart
mColorsId = get(gHandles.fData.fig1.pp_colr, 'Value');
if showPlotPoint
    hold on
    gHandles.fData.fig1.plotScat = scatter3(inputData(:,1), inputData(:,2), inputData(:,3), 'o', 'LineWidth', 1, 'MarkerEdgeColor', mColorLine(mColorsId,:), 'MarkerFaceColor', mColorFill(mColorsId,:));
    gHandles.fData.fig1.plotMark = plot3(inputData(:,1), inputData(:,2), inputData(:,3), '-o', 'LineWidth', 1, 'Color', mColorLine(mColorsId,:));
    grid on
    hold off
    set(gHandles.fData.fig1.cb_plot,'Value',1);
    set(gHandles.fData.fig1.cb_scat,'Value',0);
elseif showScatter
    hold on
    gHandles.fData.fig1.plotScat = scatter3(inputData(:,1), inputData(:,2), inputData(:,3), 'o', 'LineWidth', 1, 'MarkerEdgeColor', mColorLine(mColorsId,:), 'MarkerFaceColor', mColorFill(mColorsId,:));
    grid on
    hold off
    set(gHandles.fData.fig1.cb_scat,'Value',1);
    set(gHandles.fData.fig1.cb_plot,'Value',0);
end
% Plot Long Term Hessian Values
try
    % Remove previous figures if exists
    for i=1:size(gHandles.fData.fig1.plotArr,2)
        set     (gHandles.fData.fig1.plotArr(i),'Visible','off');
        delete  (gHandles.fData.fig1.plotArr(i));
    end
    gHandles.fData.fig1.plotArr = [];
catch err
    gHandles.fData.fig1.plotArr = [];
end
% Logical test to Ensure Hessian Map is Shown
if (hObject == gHandles.fData.fig1.pp_hesc) || ...
   (hObject == gHandles.fData.fig1.in_hess) || ...
   (hObject == gHandles.fData.fig1.in_hesg)
    hessianActive = 1;
    set(gHandles.fData.fig1.cb_hess,'Value',hessianActive);
end
% Show Plot if Hessian Map Checkbox is selected
if get(gHandles.fData.fig1.cb_hess,'Value')
    % Prevalidate User Inputs (Hessian Shade:in_hess & Hessian Grade:in_hesg)
    showHessShade = str2double(get(gHandles.fData.fig1.in_hess,'String'));
    showHessGrade = str2double(get(gHandles.fData.fig1.in_hesg,'String'));
    if (showHessShade==-2 ||showHessShade==-1 || (showHessShade>=0 && showHessShade<=gHandles.param.maxPercent))
        gHandles.param.showHessShade = showHessShade; % If Hessian Shade Value is correctly inputted
    else
        set(gHandles.fData.fig1.in_hess,'String',num2str(gHandles.param.showHessShade)); % Reverse Choice
    end
    if (showHessGrade>=1) && ~isinf(showHessGrade) && ~isnan(showHessGrade)
        gHandles.param.showHessGrade = showHessGrade; % If Hessian Grade Value is correctly inputted
    else
        set(gHandles.fData.fig1.in_hesg,'String',num2str(gHandles.param.showHessGrade)); % Reverse Choice
    end
    % Show Hessian plot if enabled by user
    hessType  = gHandles.param.showType;
    hessShade = gHandles.param.showHessShade;
    hessGrade = gHandles.param.showHessGrade; % Greater value is darker, lower value is lighter
    hvMat     = getHessianMatrixType(hessType);
    hvColorId = get(gHandles.fData.fig1.pp_hesc,'Value');
    hvColLine = mColorLine(hvColorId,:);
    hvColFill = mColorFill(hvColorId,:);
    inputDatId= 0;
    arrowId   = 0;
    maxValue  = fig1MaxDistance(inputData, hvMat);
    view(gHandles.param.dataAzim, gHandles.param.dataElev); % Set Plot View Prior to Drawing
    for i=1:size(hvMat,1)
        if (mod(i-1,3) == 0) % Data Input ID increments every nth-1 at 3rd interval of Hessian
            inputDatId = inputDatId+1;
        end
        for j=i:size(hvMat,2)
            if (mod(j,3) == 0) % Take 3rd Point and accumulate previous values
                zeroPt  = [0 0 0];
                hessPt  = [hvMat(j-2,i) hvMat(j-1,i) hvMat(j,i)];                                    % Hessian Prediction (X,Y,Z)
                inputPt = [inputData(inputDatId,1) inputData(inputDatId,2) inputData(inputDatId,3)]; % Input Source Point (X,Y,Z)
                if (sum(hessPt ~= zeroPt) > 0)                                                       % If Hessian is a non-zero point
                    distance= sqrt((hessPt(1,1) - inputPt(1,1))^2 + (hessPt(1,2) - inputPt(1,2))^2 + (hessPt(1,3) - inputPt(1,3))^2);
                    [hvColLine, hvColFill, showArrow] = fig1ColorShader(hvColLine, hvColFill, distance, maxValue, hessShade, hessGrade);
                    if showArrow                                                                     % To Fix Limits Prior to PlotArrow (Optional: PlotArrow Fixlimits)
                        arrowId = arrowId+1;
                        hessPt  = fig1HessianPtNormalize(hessPt, inputPt, distance, maxValue);        % Perform Hessian Line Arrow Normalization Mode
                        gHandles.fData.fig1.plotArr(arrowId) = PlotArrow(inputPt,hessPt,'EdgeColor',hvColLine,'FaceColor',hvColFill,'Length',10,'Width',1);
                    end
                end
            end
        end
    end
    % Statistical Measurement and Plotting
    if ~gHandles.fig.fig1_1_initialize
        gHandles.fig.fig1_1      = figure('Name','Fig #1.1: Hessian Evaluation (Statistics)','NumberTitle','off');
        gHandles.fig.fig1_1Close = get(gHandles.fig.fig1_1,'CloseRequestFcn');
        set(gHandles.fig.fig1_1,'CloseRequestFcn','');
        gHandles.fig.fig1_1_initialize   = 1;
        gHandles.fData.fig1_1.iterAdjust = 0;
        % Adjust Iteration by 1 as input iteration does not match plot iteration
        if     gHandles.fData.fig1_1.hessIter > 1; gHandles.fData.fig1_1.iterAdjust = -1;
        elseif gHandles.fData.fig1_1.hessIter < 1; gHandles.fData.fig1_1.iterAdjust = 1; 
        end
    end
    set(0,'CurrentFigure',gHandles.fig.fig1_1);
    hessMax     = size(hvMat,1) * size(hvMat,2);
    hessCount   = arrowId;
    hessPercent = (hessCount/hessMax) * gHandles.param.maxPercent;
    hessTime    = gHandles.fData.fig1_1.hessTime;
    hessIter    = gHandles.fData.fig1_1.hessIter + gHandles.fData.fig1_1.iterAdjust;
    gHandles.fData.fig1_1.hessIters(hessIter)   = hessIter;
    gHandles.fData.fig1_1.hessCount(hessIter)   = arrowId;
    gHandles.fData.fig1_1.hessPercent(hessIter) = hessPercent;
    outputDataStem(['   Hess Eval-Rate: ' gHandles.param.strBuff num2str(hessCount) ', ' num2str(hessPercent,'%01.2f') '%']);
    [plotAxes, lHandle1, lHandle2] = plotyy(gHandles.fData.fig1_1.hessIters, gHandles.fData.fig1_1.hessCount, ...
                                            gHandles.fData.fig1_1.hessIters, gHandles.fData.fig1_1.hessPercent,'plot');
    set(lHandle1,'LineStyle','-','LineWidth',1);
    set(lHandle2,'LineStyle','-.','LineWidth',1);
    set(get(plotAxes(1),'Xlabel'),'String','Iteration')
    set(get(plotAxes(1),'Ylabel'),'String','Hessian Rate Count');
    set(get(plotAxes(2),'Ylabel'),'String','% Evaluation Rate');
    title(['Hessian Evaluation Rate (' hessType ')']);
    % Save Figure & Video
    exportFigure(gHandles.fig.fig1_1, hessTime, gHandles.param.figNames{7});
    saveVideo(gHandles.fig.fig1_1, 7);
end
% If PSO is being used, fix axis for improved viewability
% http://www.mathworks.com.au/help/techdoc/visualize/f4-24991.html
set(0,'CurrentFigure',gHandles.fig.fig1);
axisLims = gHandles.PSO.axisLims;
if ~isempty(axisLims)
    axis(axisLims);
end
axis normal
view(gHandles.param.dataAzim, gHandles.param.dataElev);
title('Data Plot Representation (Source)');

function [hessPt] = fig1HessianPtNormalize(hessPt, inputPt, distance, maxValue)
% Normalization of Hessian Plot Arrows
global gHandles
runHessNormal = gHandles.param.runHessNormal(1);
disableNorm   = -3;
logNatural    = -2;
logBaseTwo    = -1;
logBaseTen    = 0;
autoScale     = 1;
switch runHessNormal
    case disableNorm
        % Disable Normalize Mode
        return
    case logNatural
        % Enable Natural Logarithm Mode
        newRadius = log(distance);
    case logBaseTwo
        % Enable Base 2 Logarithm Mode
        newRadius = log2(distance);
    case logBaseTen
        % Enable Base 10 Logarithm Mode
        newRadius = log10(distance);
    case autoScale
        % Enable Auto-Scale to Axes Mode
        xLims = xlim();
        yLims = ylim();
        zLims = zlim();
        axisRadius = sqrt(xLims(2)^2 + yLims(2)^2 + zLims(2)^2);
        if maxValue > axisRadius
            divValue  = (maxValue/axisRadius) * gHandles.param.runHessNormal(2); % Auto-Scale Division Factor
            newRadius = distance/divValue;
        else
            return % No scaling required as line is within plot
        end
    otherwise
        % Normalize by Division Mode
        newRadius = distance/runHessNormal;
end

% Normalize Hessian Value From Origin Center Point (0,0,0)
hessPt = hessPt - inputPt;
xVal   = hessPt(1);
yVal   = hessPt(2);
zVal   = hessPt(3);
radius = distance;
theta  = acos(zVal / radius); % returns value in radians
phi    = atan(yVal / xVal);   % returns value in radians
xShift = newRadius * cos(phi) * sin(theta);
yShift = newRadius * sin(phi) * sin(theta);
zShift = newRadius * cos(theta);
hessPt = [xShift yShift zShift];
hessPt = hessPt + inputPt; % Adjust Hessian Value Back to Input Point

function [maxDist] = fig1MaxDistance(inputData, hvMat)
maxDist   = 0;
inputDatId= 0;
for i=1:size(hvMat,1)
    if (mod(i-1,3) == 0)
        inputDatId = inputDatId+1;
    end
    for j=i:size(hvMat,2)
        if (mod(j,3) == 0) % Take the 3rd Point and accumulate previous values
            zeroPt = [0 0 0];
            hessPt = [hvMat(j-2,i) hvMat(j-1,i) hvMat(j,i)];                                   % Hessian Prediction
            inputPt= [inputData(inputDatId,1) inputData(inputDatId,2) inputData(inputDatId,3)];% Input Source Point
            if (sum(hessPt ~= zeroPt) > 0)
                dist = sqrt((inputPt(1,1) - hessPt(1,1))^2 + (inputPt(1,2) - hessPt(1,2))^2 + (inputPt(1,3) - hessPt(1,3))^2);
                if (dist > maxDist)
                    maxDist = dist;
                end
            end
        end
    end
end

function [hvColLine, hvColFill, showArrow, distRatio] = fig1ColorShader(hvColLine, hvColFill, distance, maxValue, hessShade, hessGrade)
global gHandles
noShade   = -2;
autoShade = -1;
showArrow = 1;
distRatio = 1-(distance/maxValue);
maxPercent= gHandles.param.maxPercent;
switch hessShade
    case noShade
        % Shader Mode off; Show All Hessian Values
        return
    case autoShade
        % Provide shading for Hessian Values based on ratio of maximum distance
        if distRatio <= 0
            return % Max Color Ratio reached, color at full saturation
        end
    otherwise
        % Provide shading based on manually set threshold value
        if (distRatio*maxPercent) > hessShade % Note: 0=Max; 100=All
            showArrow = 0;                    % Distance less than set threshold
            return
        elseif distRatio <= 0
            return % Max Color Ratio reached, color at full saturation
        end
end
colGrade  = distRatio/hessGrade;
hvColLine = hvColLine + colGrade;
hvColFill = hvColFill + colGrade;
% Test Color Gradient is valid (value <= 1)
for colId=1:size(hvColLine,2)
    if hvColLine(colId) > 1
        hvColLine(colId) = 1;
    end
    if hvColFill(colId) > 1
        hvColFill(colId) = 1;
    end
end

function plotFigure2(procTime, i)
global gHandles
% Figure 2 Output
set(0,'CurrentFigure',gHandles.fig.fig2);
if get(gHandles.handles.cb_ShowFig2,'Value')
    showDetType = gHandles.param.showDetType;
    % Show all determinant values
    if strcmpi(showDetType, 'All')
        % Generate Data
        axis 'auto y'
        [detHv1, detInv1]     = detCalculation(getHessianMatrixType(gHandles.param.dataTypes{1}));
        [detHv2, detInv2]     = detCalculation(getHessianMatrixType(gHandles.param.dataTypes{2}));
        [detHv3, detInv3]     = detCalculation(getHessianMatrixType(gHandles.param.dataTypes{3}));
        [detHv4, detInv4]     = detCalculation(getHessianMatrixType(gHandles.param.dataTypes{4}));
        [detHvAdd, detInvAdd] = detCalculation(getHessianMatrixType(gHandles.param.dataTypes{5}));
        dataEntries = [i detHv1 detInv1 detHv2 detInv2 detHv3 detInv3 detHv4 detInv4 detHvAdd detInvAdd];
        lineEntries = {'-.'; '-'; '-.'; '-'; '-.'; '-'; '-.'; '-'; '-.'};
        colorEntries= {'Red'; 'Blue'; 'Blue'; 'Green'; 'Green'; 'Magenta'; 'Magenta'; 'Black'; 'Black'};
        % Plot Data Entries
        for dataId=1:size(dataEntries,2)
            gHandles.fData.fig2.plotData(i,dataId) = dataEntries(dataId);
        end
        pHandle = plot(gHandles.fData.fig2.plotData(:,1),gHandles.fData.fig2.plotData(:,2),'-');
        set(pHandle,'Color','Red','LineWidth',1);
        hold on
        for plotId=2:size(lineEntries,1)+1
            pHandles = plot(gHandles.fData.fig2.plotData(:,1),gHandles.fData.fig2.plotData(:,plotId),lineEntries{plotId-1});
            set(pHandles,'Color',colorEntries{plotId-1},'LineWidth',1);
            if plotId == 2;                         hold on;      % Start of plot
            elseif plotId == size(lineEntries,1)+1; hold off; end % End of plot
        end
        legLab = gHandles.param.dataTypesLeg;
        hleg1  = legend(legLab{1},legLab{2},legLab{3},legLab{4},legLab{5},legLab{6},legLab{7},legLab{8},legLab{9},legLab{10});
        set(get(gca,'XLabel'),'String','Iteration');
        set(get(gca,'YLabel'),'String','Determinant Values');
        title('Determinant Values (All)');
    else
        % Show specified determinant value (showDetType)
        mat = getHessianMatrixType(showDetType);
        [detMat, detMatInv] = detCalculation(mat);
        gHandles.fData.fig2.plotData(i,1) = i;
        gHandles.fData.fig2.plotData(i,2) = detMat;
        gHandles.fData.fig2.plotData(i,3) = detMatInv;
        [plotAxes, lHandle1, lHandle2] = plotyy(gHandles.fData.fig2.plotData(:,1),gHandles.fData.fig2.plotData(:,2), ...
                                                gHandles.fData.fig2.plotData(:,1),gHandles.fData.fig2.plotData(:,3),'plot');
        set(lHandle1,'LineStyle','-','LineWidth',1);
        set(lHandle2,'LineStyle','-.','LineWidth',1);
        set(get(plotAxes(1),'Xlabel'),'String','Iteration')
        set(get(plotAxes(1),'Ylabel'),'String','Hessian Determinant Value');
        set(get(plotAxes(2),'Ylabel'),'String','Hessian Determinant Value (Inverse)');
        title(['Determinant Value (' showDetType ')']);
    end
    % Save Figure & Video
    set(gHandles.fig.fig2,'Name','Fig #2: Determinant Plot (Linear Decomposition)');
    exportFigure(gHandles.fig.fig2, procTime, gHandles.param.figNames{2});
    saveVideo(gHandles.fig.fig2, 2);
else
    clf(gHandles.fig.fig2);
    set(gHandles.fig.fig2,'Name','Fig #2 Window (Auto-Close)');
    title('Fig #2 Disabled - Set Checkbox to Enable');
    set(gca,'Color',[0.8 0.8 0.8]);
    set(gca,'XColor',[0.8 0.8 0.8]);
    set(gca,'YColor',[0.8 0.8 0.8]);
end

function plotFigure3(procTime)
global gHandles
% Figure 3 Output
set(0,'CurrentFigure',gHandles.fig.fig3);
if get(gHandles.handles.cb_ShowFig3,'Value')
    showType = gHandles.param.showType;
    mat = getHessianMatrixType(showType);
    axis fill
    pc    = NaN; pcInv    = NaN;
    score = NaN; scoreInv = NaN;
    try
        warning('off', 'stats:princomp:colRankDefX'); % Warning Suppression of msg
        matInv = 1./mat; % Same as mat.\1; Do not use inv(mat) function.
        matInv = cleanupMatrix(matInv);
        [pc, score, ~, ~]       = princomp(mat);
        [pcInv, scoreInv, ~, ~] = princomp(matInv);
    catch err %#ok<NASGU>
    end
    if ~(isnan(pc))
        if ~isnan(pcInv), subplot(1,2,1); end
        biplot(pc(:,1:2),'Scores',score(:,1:2),'VarLabels',fig3PlotDataVarLabels(size(pc,1)));
        title(['PCA Analysis Plot (' showType ')']);
    end
    if ~(isnan(pcInv))
        if ~isnan(pc), subplot(1,2,2); end
        biplot(pcInv(:,1:2),'Scores',scoreInv(:,1:2),'VarLabels',fig3PlotDataVarLabels(size(pcInv,1)));
        title(['PCA Analysis Plot (' showType ' Inverse)']);
    end
    if ~(isnan(pc)) | ~(isnan(pcInv)) %#ok<OR2>
        set(gHandles.fig.fig3,'Name','Fig #3: PCA Plot (Linear Decomposition)');
        % Save Figure & Video
        exportFigure(gHandles.fig.fig3, procTime, gHandles.param.figNames{3});
        saveVideo(gHandles.fig.fig3, 3);
    else
        title('Fig #3 Cannot be Rendered');
    end
else
    clf(gHandles.fig.fig3);
    set(gHandles.fig.fig3,'Name','Fig #3 Window (Auto-Close)');
    title('Fig #3 Disabled - Set Checkbox to Enable');
    set(gca,'Color',[0.8 0.8 0.8]);
    set(gca,'XColor',[0.8 0.8 0.8]);
    set(gca,'YColor',[0.8 0.8 0.8]);
end

function [varLabel] = fig3PlotDataVarLabels(numIds)
varLabel = cell(numIds,1);
for i=1:numIds
    varLabel{i} = [num2str(i) '.'];
end

function plotFigure4(procTime)
global gHandles
% Figure 4 Output
set(0,'CurrentFigure',gHandles.fig.fig4);
if get(gHandles.handles.cb_ShowFig4,'Value')
    showType = gHandles.param.showType;
    mat = getHessianMatrixType(showType);
    % Preliminaries: Re-arrange matrix array as n-by-3 matrix
    [X, Y] = meshgrid(1:1:size(mat,1)); % As matrix array
    hvAddTrip(:,1) = reshape(X,size(mat,1)*size(mat,1),1)'; % As Matrix Triplet Array
    hvAddTrip(:,2) = reshape(Y,size(mat,1)*size(mat,1),1)';
    hvAddTrip(:,3) = reshape(mat,size(mat,1)*size(mat,1),1)';
    % Plot Matrix and Sparsity Pattern
    axis fill
    subplot(1,2,1);
    plotmatrix(hvAddTrip,'*r');
    title(['Matrix Subplot of Hessian (' showType ')']);
    subplot(1,2,2);
    spy(hvAddTrip);
    title(['Sparsity Pattern of Hessian (' showType ')']);
    set(gHandles.fig.fig4,'Name','Fig #4: Matrix & Sparsity Plot (Non-Linear Decomposition)');
    % Save Figure & Video
    exportFigure(gHandles.fig.fig4, procTime, gHandles.param.figNames{4});
    saveVideo(gHandles.fig.fig4, 4);
else
    clf(gHandles.fig.fig4);
    set(gHandles.fig.fig4,'Name','Fig #4 Window (Auto-Close)');
    title('Fig #4 Disabled - Set Checkbox to Enable');
    set(gca,'Color',[0.8 0.8 0.8]);
    set(gca,'XColor',[0.8 0.8 0.8]);
    set(gca,'YColor',[0.8 0.8 0.8]);
end

function plotFigure5(procTime)
global gHandles
% Figure 5 Output
set(0,'CurrentFigure',gHandles.fig.fig5);
if get(gHandles.handles.cb_ShowFig5,'Value')
    % UI Configuration
    colSurf= get(gHandles.handles.popup_ColourMap,'Value');
    colMark= get(gHandles.handles.popup_MarkColour,'Value');
    plotId = gHandles.param.chartType;
    gHandles.fData.fig5.cb_mark = uicontrol(gcf,'Style','checkbox', 'Value',0,      'Position',[0  0  110 15],'BackgroundColor',[0.8 0.8 0.8],'String','Show Marker Point');
    gHandles.fData.fig5.cb_text = uicontrol(gcf,'Style','checkbox', 'Value',1,      'Position',[0  15 110 15],'BackgroundColor',[0.8 0.8 0.8],'String','Show Min/Max Txt');
    gHandles.fData.fig5.pp_type = uicontrol(gcf,'Style','popupmenu','Value',plotId, 'Position',[50 30  60 20],'BackgroundColor',[0.8 0.8 0.8],'String',{'Surface','Mesh','Contour'});
    gHandles.fData.fig5.pp_cmap = uicontrol(gcf,'Style','popupmenu','Value',colSurf,'Position',[50 50  60 20],'BackgroundColor',[0.8 0.8 0.8],'String',{'Autumn','Bone','Colorcube','Cool','Copper','Flag','Gray','Hot','HSV','Jet','Lines','Pink','Prism','Spring','Summer','White','Winter'});
    gHandles.fData.fig5.pp_mcol = uicontrol(gcf,'Style','popupmenu','Value',colMark,'Position',[50 70  60 20],'BackgroundColor',[0.8 0.8 0.8],'String',{'Blue','Blue-Gray','Turquoise','Cyan','Red','Pink','Green','Lime','Orange','Yellow','Purple','Violet','Magenta','Lt.Magenta','Brown','Oak','Dk.Gray','Lt.Gray','Black','White'});
    gHandles.fData.fig5.tx_type = uicontrol(gcf,'Style','text','Position',[0 27 50 17],'BackgroundColor',[0.8 0.8 0.8],'String','Plot Type');
    gHandles.fData.fig5.tx_cmap = uicontrol(gcf,'Style','text','Position',[0 47 50 17],'BackgroundColor',[0.8 0.8 0.8],'String','Colormap');
    gHandles.fData.fig5.tx_mcol = uicontrol(gcf,'Style','text','Position',[0 67 50 17],'BackgroundColor',[0.8 0.8 0.8],'String','Mk.Color');
    set(gHandles.fData.fig5.cb_mark,'Callback',@fig5CallbackStem);
    set(gHandles.fData.fig5.cb_text,'Callback',@fig5CallbackStem);
    set(gHandles.fData.fig5.pp_type,'Callback',@fig5CallbackStem);
    set(gHandles.fData.fig5.pp_cmap,'Callback',@fig5CallbackStem);
    set(gHandles.fData.fig5.pp_mcol,'Callback',@fig5CallbackStem);
    % Plot Surface Map
    fig5CallbackStem(gHandles.fData.fig5.pp_type,[],[]); % Subfunction for plotting
    set(gHandles.fig.fig5,'Name','Fig #5: Hessian Plot (Non-Linear Decomposition)');
    % Save Figure & Video
    exportFigure(gHandles.fig.fig5, procTime, gHandles.param.figNames{5});
    saveVideo(gHandles.fig.fig5, 5);
else
    clf(gHandles.fig.fig5);
    set(gHandles.fig.fig5,'Name','Fig #5 Window (Auto-Close)');
    title('Fig #5 Disabled - Set Checkbox to Enable');
    set(gca,'Color', [0.8 0.8 0.8]);
    set(gca,'XColor',[0.8 0.8 0.8]);
    set(gca,'YColor',[0.8 0.8 0.8]);
end

function fig5MinMaxTextValues()
global gHandles
try % Remove previous handle if exists
    set   (gHandles.fData.fig5.textMinVal,'Visible','off');
    set   (gHandles.fData.fig5.textMaxVal,'Visible','off');
    delete(gHandles.fData.fig5.textMinVal);
    delete(gHandles.fData.fig5.textMaxVal);
catch err
end
if get(gHandles.fData.fig5.cb_text,'Value') % If Text is Selected
    inputMatrix = getHessianMatrixType(gHandles.param.showType);
    minVal  = 0;
    iMinVal = 1;
    jMinVal = 1;
    kMinVal = 0;
    maxVal  = 0;
    iMaxVal = 1;
    jMaxVal = 1;
    kMaxVal = 0;
    for i=1:size(inputMatrix,1)
        for j=1:size(inputMatrix,2)
            if (inputMatrix(i,j)<kMinVal)
                iMinVal = i;
                jMinVal = j;
                kMinVal = inputMatrix(i,j);
                minVal  = inputMatrix(i,j);
            end
            if (inputMatrix(i,j)>kMaxVal)
                iMaxVal = i;
                jMaxVal = j;
                kMaxVal = inputMatrix(i,j);
                maxVal  = inputMatrix(i,j);
            end
        end
    end
    % Text Orientation Correction to make visible
    [~, elevation] = view(gca);
    if     (elevation ==  90); kMinVal = kMaxVal;
    elseif (elevation == -90); kMaxVal = kMinVal;
    end
    % Contour Map Adjustment (ID value of Dropdownbox = 3)
    if (get(gHandles.fData.fig5.pp_type,'Value') == 3)
        if (elevation >= 0)
            kMinVal = 0.05;
            kMaxVal = 0.05;
        else
            kMinVal = -0.05;
            kMaxVal = -0.05;
        end
    end
    gHandles.fData.fig5.textMinVal = text(iMinVal,jMinVal,kMinVal,['\bf\leftarrowMin.\rm(' num2str(iMinVal) ',' num2str(jMinVal) '): ' num2str(minVal)],'FontSize',9);
    gHandles.fData.fig5.textMaxVal = text(iMaxVal,jMaxVal,kMaxVal,['\bf\leftarrowMax.\rm(' num2str(iMaxVal) ',' num2str(jMaxVal) '): ' num2str(maxVal)],'FontSize',9);
end

function fig5CallbackStem(hObject, ~, ~)
global gHandles
% Preliminaries
showType = gHandles.param.showType;
mat = getHessianMatrixType(showType);
set(0,'CurrentFigure',gHandles.fig.fig5);

% Plot Mesh or Surface Map
if hObject == gHandles.fData.fig5.pp_type
    surfaceMap = 1;
    meshMap    = 2;
    contourMap = 3;
    switch get(gHandles.fData.fig5.pp_type,'Value')
        case surfaceMap
            surf(mat,'FaceColor','interp','EdgeColor','none','FaceLighting','phong');
            camlight left
        case meshMap
            mesh(mat);
        case contourMap
            contour(mat,gHandles.param.contLayers);
    end
    view(gHandles.param.showAzim, gHandles.param.showElev);
    xlabel('X','FontSize',10);
    ylabel('Y','FontSize',10);
    zlabel('Z','FontSize',10);
end
colorMaps = get(gHandles.fData.fig5.pp_cmap,'String');
colorMapId= get(gHandles.fData.fig5.pp_cmap,'Value');
colormap(colorMaps{colorMapId});
fig5MinMaxTextValues();

% Plot Mark Points
if hObject == gHandles.fData.fig5.cb_mark || hObject == gHandles.fData.fig5.pp_mcol
    if hObject == gHandles.fData.fig5.pp_mcol
        set(gHandles.fData.fig5.cb_mark, 'Value', 1);
    end
    mColors  = [[0 0 0.8];[0.4 0.6 0.7];[0 0.6 0.9];[0 1 1];[1 0 0];[1 0.7 0.8];[0 0.6 0];[0 1 0];[1 0.4 0];[1 1 0];[0.6 0.3 0.6];[0.8 0.7 0.9];[1 0 1];[1 0.5 1];[0.5 0 0.1];[0.7 0.5 0.3];[0.3 0.3 0.3];[0.7 0.7 0.7];[0 0 0];[1 1 1]];
    mColorsId= get(gHandles.fData.fig5.pp_mcol, 'Value');
    isMark   = get(gHandles.fData.fig5.cb_mark, 'Value');
    if isMark
        try % Remove previous handle if exists
            set   (gHandles.fData.fig5.plot_handle,'Visible','off');
            delete(gHandles.fData.fig5.plot_handle);
        catch err
        end
        axis tight; hold on;
        [xMatrix, yMatrix] = meshgrid(1:1:size(mat,1));     % As Matrix array
        xPts = reshape(xMatrix,size(mat,1)*size(mat,1),1)'; % As Singular array
        yPts = reshape(yMatrix,size(mat,1)*size(mat,1),1)';
        zPts = reshape(mat,    size(mat,1)*size(mat,1),1)';
        gHandles.fData.fig5.plot_handle = plot3(xPts,yPts,zPts,'.','MarkerSize',5,'Color',mColors(mColorsId,:));
    else
        axis auto; hold off;
        set(gHandles.fData.fig5.plot_handle,'Visible','off');
    end
end
title(['Hessian Plot (' showType ')']);

function plotFigure6(procTime)
global gHandles
% Figure 6 Output
set(0,'CurrentFigure',gHandles.fig.fig6);
if get(gHandles.handles.cb_ShowFig6,'Value')
    % UI Configuration
    colSurf= get(gHandles.handles.popup_ColourMap,'Value');
    colMark= get(gHandles.handles.popup_MarkColour,'Value');
    plotId = gHandles.param.chartType;
    gHandles.fData.fig6.cb_mark = uicontrol(gcf,'Style','checkbox', 'Value',0,      'Position',[0  0  110 15],'BackgroundColor',[0.8 0.8 0.8],'String','Show Marker Point');
    gHandles.fData.fig6.cb_text = uicontrol(gcf,'Style','checkbox', 'Value',1,      'Position',[0  15 110 15],'BackgroundColor',[0.8 0.8 0.8],'String','Show Min/Max Txt');
    gHandles.fData.fig6.pp_type = uicontrol(gcf,'Style','popupmenu','Value',plotId, 'Position',[50 30  60 20],'BackgroundColor',[0.8 0.8 0.8],'String',{'Surface','Mesh','Contour'});
    gHandles.fData.fig6.pp_cmap = uicontrol(gcf,'Style','popupmenu','Value',colSurf,'Position',[50 50  60 20],'BackgroundColor',[0.8 0.8 0.8],'String',{'Autumn','Bone','Colorcube','Cool','Copper','Flag','Gray','Hot','HSV','Jet','Lines','Pink','Prism','Spring','Summer','White','Winter'});
    gHandles.fData.fig6.pp_mcol = uicontrol(gcf,'Style','popupmenu','Value',colMark,'Position',[50 70  60 20],'BackgroundColor',[0.8 0.8 0.8],'String',{'Blue','Blue-Gray','Turquoise','Cyan','Red','Pink','Green','Lime','Orange','Yellow','Purple','Violet','Magenta','Lt.Magenta','Brown','Oak','Dk.Gray','Lt.Gray','Black','White'});
    gHandles.fData.fig6.tx_type = uicontrol(gcf,'Style','text','Position',[0 27 50 17],'BackgroundColor',[0.8 0.8 0.8],'String','Plot Type');
    gHandles.fData.fig6.tx_cmap = uicontrol(gcf,'Style','text','Position',[0 47 50 17],'BackgroundColor',[0.8 0.8 0.8],'String','Colormap');
    gHandles.fData.fig6.tx_mcol = uicontrol(gcf,'Style','text','Position',[0 67 50 17],'BackgroundColor',[0.8 0.8 0.8],'String','Mk.Color');
    set(gHandles.fData.fig6.cb_mark,'Callback',@fig6CallbackStem);
    set(gHandles.fData.fig6.cb_text,'Callback',@fig6CallbackStem);
    set(gHandles.fData.fig6.pp_type,'Callback',@fig6CallbackStem);
    set(gHandles.fData.fig6.pp_cmap,'Callback',@fig6CallbackStem);
    set(gHandles.fData.fig6.pp_mcol,'Callback',@fig6CallbackStem);
    % Pre-initialize variables for plot & text handles
    gHandles.fData.fig6.plot_handle_uMat   = [];
    gHandles.fData.fig6.textMinVal_uMat    = [];
    gHandles.fData.fig6.textMaxVal_uMat    = [];
    gHandles.fData.fig6.plot_handle_cholMat= [];
    gHandles.fData.fig6.textMinVal_cholMat = [];
    gHandles.fData.fig6.textMaxVal_cholMat = [];
    % Plot Surface Map
    fig6CallbackStem(gHandles.fData.fig6.pp_type,[],[]); % Subfunction for plotting
    set(gHandles.fig.fig6,'Name','Fig #6: LU & Cholesky Plot (Non-Linear Decomposition)');
    % Save Figure & Video
    exportFigure(gHandles.fig.fig6, procTime, gHandles.param.figNames{6});
    saveVideo(gHandles.fig.fig6, 6);
else
    clf(gHandles.fig.fig6);
    set(gHandles.fig.fig6,'Name','Fig #6 Window (Auto-Close)');
    title('Fig #6 Disabled - Set Checkbox to Enable');
    set(gca,'Color',[0.8 0.8 0.8]);
    set(gca,'XColor',[0.8 0.8 0.8]);
    set(gca,'YColor',[0.8 0.8 0.8]);
end

function fig6CallbackStem(hObject, ~, ~)
global gHandles
% Preliminaries
showType = gHandles.param.showType;
mat = getHessianMatrixType(showType);
[uMat, cholMat] = matrixFactorisation(mat);
set(0,'CurrentFigure',gHandles.fig.fig6);
% Plot Surface Map LU
if ~isnan(cholMat) & ~isnan(uMat) %#ok<AND2>
    subplot(1,2,1);
end
if ~isnan(uMat)
    titleString = ['Lower-Upper Decomposition (' showType ')'];
    plotHandle  = gHandles.fData.fig6.plot_handle_uMat;
    txtMinHandle= gHandles.fData.fig6.textMinVal_uMat;
    txtMaxHandle= gHandles.fData.fig6.textMaxVal_uMat;
    [plotHandle, txtMinHandle, txtMaxHandle]= fig6Plotting(hObject, uMat, titleString, plotHandle, txtMinHandle, txtMaxHandle);
    gHandles.fData.fig6.plot_handle_uMat    = plotHandle;
    gHandles.fData.fig6.textMinVal_uMat     = txtMinHandle;
    gHandles.fData.fig6.textMaxVal_uMat     = txtMaxHandle;
end
% Plot Surface Map CHOL
if ~isnan(cholMat) & ~isnan(uMat) %#ok<AND2>
    subplot(1,2,2);
end
if ~isnan(cholMat)
    titleString = ['Cholesky Factorisation (' showType ')'];
    plotHandle  = gHandles.fData.fig6.plot_handle_cholMat;
    txtMinHandle= gHandles.fData.fig6.textMinVal_cholMat;
    txtMaxHandle= gHandles.fData.fig6.textMaxVal_cholMat;
    [plotHandle, txtMinHandle, txtMaxHandle]= fig6Plotting(hObject, cholMat, titleString, plotHandle, txtMinHandle, txtMaxHandle);
    gHandles.fData.fig6.plot_handle_cholMat = plotHandle;
    gHandles.fData.fig6.textMinVal_cholMat  = txtMinHandle;
    gHandles.fData.fig6.textMaxVal_cholMat  = txtMaxHandle;
end

function [txtMinHandle, txtMaxHandle] = fig6MinMaxTextValues(inputMatrix, txtMinHandle, txtMaxHandle)
global gHandles
try % Remove previous handle if exists
    set   (txtMinHandle,'Visible','off');
    set   (txtMaxHandle,'Visible','off');
    delete(txtMinHandle);
    delete(txtMaxHandle);
catch err
end
if get(gHandles.fData.fig6.cb_text,'Value') % If Text is Selected
    minVal  = 0;
    iMinVal = 1;
    jMinVal = 1;
    kMinVal = 0;
    maxVal  = 0;
    iMaxVal = 1;
    jMaxVal = 1;
    kMaxVal = 0;
    for i=1:size(inputMatrix,1)
        for j=1:size(inputMatrix,2)
            if (inputMatrix(i,j)<kMinVal)
                iMinVal = j; % Rationalised Plot Inversion
                jMinVal = i;
                kMinVal = inputMatrix(i,j);
                minVal  = inputMatrix(i,j);
            end
            if (inputMatrix(i,j)>kMaxVal)
                iMaxVal = j; % Rationalised Plot Inversion
                jMaxVal = i;
                kMaxVal = inputMatrix(i,j);
                maxVal  = inputMatrix(i,j);
            end
        end
    end
    % Text Orientation Correction to make visible
    [~, elevation] = view(gca);
    if     (elevation ==  90); kMinVal = kMaxVal;
    elseif (elevation == -90); kMaxVal = kMinVal;
    end
    % Contour Map Adjustment (ID value of Dropdownbox = 3)
    if (get(gHandles.fData.fig6.pp_type,'Value') == 3)
        if (elevation >= 0)
            kMinVal = 0.05;
            kMaxVal = 0.05;
        else
            kMinVal = -0.05;
            kMaxVal = -0.05;
        end
    end
    txtMinHandle = text(iMinVal,jMinVal,kMinVal,['\bf\leftarrowMin.\rm(' num2str(iMinVal) ',' num2str(jMinVal) '): ' num2str(minVal)],'FontSize',9);
    txtMaxHandle = text(iMaxVal,jMaxVal,kMaxVal,['\bf\leftarrowMax.\rm(' num2str(iMaxVal) ',' num2str(jMaxVal) '): ' num2str(maxVal)],'FontSize',9);
end

function [plotHandle, txtMinHandle, txtMaxHandle] = fig6Plotting(hObject, inputMat, titleString, plotHandle, txtMinHandle, txtMaxHandle)
global gHandles
% Plot Mesh or Surface Map
if hObject == gHandles.fData.fig6.pp_type
    surfaceMap = 1;
    meshMap    = 2;
    contourMap = 3;
    switch get(gHandles.fData.fig6.pp_type,'Value')
        case surfaceMap
            surf(inputMat,'FaceColor','interp','EdgeColor','none','FaceLighting','phong');
            camlight left
        case meshMap
            mesh(inputMat);
        case contourMap
            contour(inputMat,gHandles.param.contLayers);
    end
    view(gHandles.param.showAzim, gHandles.param.showElev);
    xlabel('X','FontSize',10);
    ylabel('Y','FontSize',10);
    zlabel('Z','FontSize',10);
end
colorMaps = get(gHandles.fData.fig6.pp_cmap,'String');
colorMapId= get(gHandles.fData.fig6.pp_cmap,'Value');
colormap(colorMaps{colorMapId});
[txtMinHandle, txtMaxHandle] = fig6MinMaxTextValues(inputMat, txtMinHandle, txtMaxHandle);

% Plot Mark Points
if hObject == gHandles.fData.fig6.cb_mark || hObject == gHandles.fData.fig6.pp_mcol
    if hObject == gHandles.fData.fig6.pp_mcol
        set(gHandles.fData.fig6.cb_mark, 'Value', 1);
    end
    mColors  = [[0 0 0.8];[0.4 0.6 0.7];[0 0.6 0.9];[0 1 1];[1 0 0];[1 0.7 0.8];[0 0.6 0];[0 1 0];[1 0.4 0];[1 1 0];[0.6 0.3 0.6];[0.8 0.7 0.9];[1 0 1];[1 0.5 1];[0.5 0 0.1];[0.7 0.5 0.3];[0.3 0.3 0.3];[0.7 0.7 0.7];[0 0 0];[1 1 1]];
    mColorsId= get(gHandles.fData.fig6.pp_mcol, 'Value');
    isMark   = get(gHandles.fData.fig6.cb_mark, 'Value');
    if isMark
        try % Remove previous handle if exists
            set   (plotHandle,'Visible','off');
            delete(plotHandle);
        catch err
        end
        axis tight; hold on;
        [xMatrix, yMatrix] = meshgrid(1:1:size(inputMat,1));     % As Matrix array
        xPts = reshape(xMatrix,size(inputMat,1)*size(inputMat,1),1)'; % As Singular array
        yPts = reshape(yMatrix,size(inputMat,1)*size(inputMat,1),1)';
        zPts = reshape(inputMat,size(inputMat,1)*size(inputMat,1),1)';
        plotHandle = plot3(xPts,yPts,zPts,'.','MarkerSize',5,'Color',mColors(mColorsId,:));
    else
        axis auto; hold off;
        set(plotHandle,'Visible','off');
    end
end
title(titleString);
