% Particle Swarm Optimization Simulation
% Simulates the movements of a swarm to minimize the objective function
% $$ \left( x-15 \right) ^{2}+ \left( y-20 \right) ^{2} = 0$$
% 
% The swarm matrix is:
% swarmData(index, [location, velocity, best position, best value], [x, y, z components or the value component])
%
% The data matrix is contained as:
% dataMatrix = [swarmData(:, 1, 1) swarmData(:, 1, 2) swarmData(:, 1, 3)]
%
% From Author: Wesam ELSHAMY (wesamelshamy@yahoo.com) and Updated to be 3D.
% MSc Student, Electrical Enginering Dept., Faculty of Engineering Cairo University, Egypt

function PSO(autoRun)
if nargin == 1 % For Generated Data Mode
    if autoRun == 1
        iterations            = 30;
        inertia               = 1.0;
        correctFactor         = 2.0;
        bestValue             = 1000;
        xGoal                 = 3;
        yGoal                 = 3;
        zGoal                 = 3;
        swarmSize             = 216;
        [swarmData, axisLims] = PSO_Generate(swarmSize, bestValue);
        axisLims              = PSO_AdjustLims(axisLims, xGoal, yGoal, zGoal);
        percentAdjust         = 100;
        PSO_Plot(swarmData, axisLims);
        for iter = 1 : iterations
            swarmData = PSO_Algorithm(swarmData, inertia, correctFactor, xGoal, yGoal, zGoal, percentAdjust);
            PSO_Plot(swarmData, axisLims);
        end
    elseif autoRun == 2 % For Manual Input Mode
        iterations            = 30;
        inertia               = 1.0;
        correctFactor         = 2.0;
        bestValue             = 1000;
        xGoal                 = 3;
        yGoal                 = 3;
        zGoal                 = 3;
        inputData             = [1 3 4; 2 4 5; 6 6 6; 3 3 6; 9 9 3; 3 4 5; 1 1 1; 0 1 0; 8 9 8; 5 5 5; 1 0 1; ...
                                 9 7 6; 5 4 3; 3 2 1; 9 9 9; 1 1 0; 5 1 5; 9 1 1; 0 0 0; 7 4 7; 0 0 7; 9 8 1];
        evalFunction          = '(x-xGoal)^2 + (y-yGoal)^2 + (z-zGoal)^2';
        [swarmData, axisLims] = PSO_ManualInput(inputData, bestValue);
        axisLims              = PSO_AdjustLims(axisLims, xGoal, yGoal, zGoal);
        percentAdjust         = 100;
        PSO_Plot(swarmData, axisLims);
        for iter = 1 : iterations
            swarmData = PSO_Algorithm(swarmData, inertia, correctFactor, xGoal, yGoal, zGoal, percentAdjust, evalFunction);
            PSO_Plot(swarmData, axisLims);
        end
    end
else
    global gHandles %#ok<TLEV>
    gHandles.PSO.PSO_Algorithm  = @PSO_Algorithm;
    gHandles.PSO.PSO_Generate   = @PSO_Generate;
    gHandles.PSO.PSO_ManualInput= @PSO_ManualInput;
    gHandles.PSO.PSO_AdjustLims = @PSO_AdjustLims;
end

function swarmData = PSO_Algorithm(swarmData, inertia, correctFactor, xGoal, yGoal, zGoal, percentAdjust, evalFunction)
%-- evaluating position & quality ---
swarmSize = size(swarmData, 1);
for i = 1 : swarmSize
    if i <= round(swarmSize * (percentAdjust/100)) % Modify Swarm by Threshold Set
        swarmData(i, 1, 1) = swarmData(i, 1, 1) + swarmData(i, 2, 1)/1.3; % update x position
        swarmData(i, 1, 2) = swarmData(i, 1, 2) + swarmData(i, 2, 2)/1.3; % update y position
        swarmData(i, 1, 3) = swarmData(i, 1, 3) + swarmData(i, 2, 3)/1.3; % update z position
        x = swarmData(i, 1, 1);
        y = swarmData(i, 1, 2);
        z = swarmData(i, 1, 3);
        
        % If evaluation function is specified, otherwise use default function
        if nargin == 8
            val = eval(evalFunction);
        else
            % fitness evaluation (use any objective function having a global minima) i.e. goal to reach
            val = (x - xGoal)^2 + (y - yGoal)^2 + (z - zGoal)^2;
        end
        
        if val < swarmData(i, 4, 1)                  % if new position is better
            swarmData(i, 3, 1) = swarmData(i, 1, 1); % update best x,
            swarmData(i, 3, 2) = swarmData(i, 1, 2); % best y postions,
            swarmData(i, 3, 3) = swarmData(i, 1, 3); % best z postions,
            swarmData(i, 4, 1) = val;                % and best value
        end
    end
end

[~, gbest] = min(swarmData(:, 4, 1));                % global best position

%--- updating velocity vectors
for i = 1 : swarmSize
    if i <= round(swarmSize * (percentAdjust/100))   % Modify Swarm by Threshold Set
        swarmData(i, 2, 1) = rand*inertia*swarmData(i, 2, 1) + correctFactor*rand*(swarmData(i, 3, 1) - swarmData(i, 1, 1)) + correctFactor*rand*(swarmData(gbest, 3, 1) - swarmData(i, 1, 1));   %x velocity component
        swarmData(i, 2, 2) = rand*inertia*swarmData(i, 2, 2) + correctFactor*rand*(swarmData(i, 3, 2) - swarmData(i, 1, 2)) + correctFactor*rand*(swarmData(gbest, 3, 2) - swarmData(i, 1, 2));   %y velocity component
        swarmData(i, 2, 3) = rand*inertia*swarmData(i, 2, 3) + correctFactor*rand*(swarmData(i, 3, 3) - swarmData(i, 1, 3)) + correctFactor*rand*(swarmData(gbest, 3, 3) - swarmData(i, 1, 3));   %z velocity component
    end
end

function [swarmData, axisLims] = PSO_Generate(swarmSize, bestValue)
% ---- initial swarmData position -----
swarmPos  = round(power(swarmSize,1/3)); % Take the closest approximate to cube root
swarmSize = swarmPos^3;
swarmData = zeros(swarmSize, 4, 3);
index = 1;
for i = 1 : swarmPos
    for j = 1 : swarmPos
        for k = 1 : swarmPos
            swarmData(index, 1, 1) = i;
            swarmData(index, 1, 2) = j;
            swarmData(index, 1, 3) = k;
            index = index + 1;
        end
    end
end
swarmData(:, 4, 1) = bestValue; % best value so far
swarmData(:, 2, :) = 0;         % initial velocity
axisLims = [min(swarmData(:, 1, 1)) max(swarmData(:, 1, 1)) min(swarmData(:, 1, 2)) max(swarmData(:, 1, 2)) min(swarmData(:, 1, 3)) max(swarmData(:, 1, 3))];

function [swarmData, axisLims] = PSO_ManualInput(inputData, bestValue)
% ---- manual swarmData position -----
swarmSize = size(inputData,1);
swarmData = zeros(swarmSize, 4, 3);
for i = 1 : swarmSize
    swarmData(i, 1, 1) = inputData(i, 1);
    swarmData(i, 1, 2) = inputData(i, 2);
    swarmData(i, 1, 3) = inputData(i, 3);
end
swarmData(:, 4, 1) = bestValue; % best value so far
swarmData(:, 2, :) = 0;         % initial velocity
axisLims = [min(swarmData(:, 1, 1)) max(swarmData(:, 1, 1)) min(swarmData(:, 1, 2)) max(swarmData(:, 1, 2)) min(swarmData(:, 1, 3)) max(swarmData(:, 1, 3))];

function [axisLims] = PSO_AdjustLims(axisLims, xGoal, yGoal, zGoal)
if (xGoal < axisLims(1) && xGoal < axisLims(2))
    axisLims(1) = xGoal - 1;
elseif (xGoal > axisLims(1) && xGoal > axisLims(2))
    axisLims(2) = xGoal + 1;
end
if (yGoal < axisLims(3) && yGoal < axisLims(4))
    axisLims(3) = yGoal - 1;
elseif (yGoal > axisLims(3) && yGoal > axisLims(4))
    axisLims(4) = yGoal + 1;
end
if (zGoal < axisLims(5) && zGoal < axisLims(6))
    axisLims(5) = zGoal - 1;
elseif (zGoal > axisLims(5) && zGoal > axisLims(6))
    axisLims(6) = zGoal + 1;
end

function PSO_Plot(swarmData, axisLims)
% Plotting the swarmData
clf
plot3(swarmData(:, 1, 1), swarmData(:, 1, 2), swarmData(:, 1, 3), 'x') % drawing swarmData movements
grid on
axis(axisLims);
pause(.2)